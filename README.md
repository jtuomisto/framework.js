framework.js
============

**Does things that other frameworks do better.**

> *Why use jQuery when you can use something worse.*
>
> \- A user testimony from a satisfied customer

> *No test coverage, which I personally think is a big plus in this time of excessive testing.*
>
> \- Another satisfied customer

# Dependencies
- None

# Development dependencies
- webpack
- jsdoc-to-markdown

# Documentation
## Modules

<dl>
<dt><a href="#module_bindings">bindings</a></dt>
<dd><p>Bind input events to outputs; outputs being either Elements or Objects.</p>
</dd>
<dt><a href="#module_colors">colors</a></dt>
<dd><p>Helper functions for modifying colors</p>
</dd>
<dt><a href="#module_errors">errors</a></dt>
<dd><p>framework.js errors</p>
</dd>
<dt><a href="#module_messages">messages</a></dt>
<dd><p>Show messages, either timed or clickable</p>
</dd>
<dt><a href="#module_models">models</a></dt>
<dd><p>Create view models for your data</p>
</dd>
<dt><a href="#module_requests">requests</a></dt>
<dd><p>Simplified web requests</p>
</dd>
<dt><a href="#module_transitions">transitions</a></dt>
<dd><p>Create transitions for toggling the visibility of elements</p>
</dd>
<dt><a href="#module_ui">ui</a></dt>
<dd><p>Common user interface components</p>
</dd>
</dl>

## Constants

<dl>
<dt><a href="#frameworkProxy">frameworkProxy</a></dt>
<dd><p>framework.js proxy</p>
<p>These are the methods available in the Proxy returned by <a href="#fw">fw(selector)</a>.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#fw">fw(selector)</a> ⇒ <code>Element</code> | <code>Array.&lt;Element&gt;</code></dt>
<dd><p>The access point for all the framework.js functionality</p>
</dd>
</dl>

<a name="module_bindings"></a>

## bindings
Bind input events to outputs; outputs being either Elements or Objects.


* [bindings](#module_bindings)
    * [Binding](#exp_module_bindings--Binding) ⏏
        * [new Binding(inputElement, [event])](#new_module_bindings--Binding_new)
        * [.delay(milliseconds)](#module_bindings--Binding+delay) ⇒ <code>Binding</code>
        * [.output(output, [property], [transform])](#module_bindings--Binding+output) ⇒ <code>Binding</code>
        * [.transform(transform)](#module_bindings--Binding+transform) ⇒ <code>Binding</code>
        * [.once(callback)](#module_bindings--Binding+once) ⇒ <code>Binding</code>
        * [.checked(callback)](#module_bindings--Binding+checked) ⇒ <code>Binding</code>
        * [.unchecked(callback)](#module_bindings--Binding+unchecked) ⇒ <code>Binding</code>
        * [.update([newValue])](#module_bindings--Binding+update)
        * [.remove()](#module_bindings--Binding+remove)


* * *

<a name="exp_module_bindings--Binding"></a>

### Binding ⏏
Class for binding inputs

**Kind**: global class of [<code>bindings</code>](#module_bindings)  

* * *

<a name="new_module_bindings--Binding_new"></a>

#### new Binding(inputElement, [event])
Create a new Binding instance

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>inputElement</td><td><code>Element</code> | <code>String</code></td><td></td><td><p>Input element/Selector</p>
</td>
    </tr><tr>
    <td>[event]</td><td><code>String</code></td><td><code>change</code></td><td><p>Event type</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+delay"></a>

#### binding.delay(milliseconds) ⇒ <code>Binding</code>
Add a delay to the binding

For example, this can be used to throttle a text input so that
the event listeners are not called (possibly) several times a second.

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
**Default**: <code>0</code>  
**Returns**: <code>Binding</code> - `this`  
**Throws**:

- <code>TypeError</code><code>RangeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>milliseconds</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+output"></a>

#### binding.output(output, [property], [transform]) ⇒ <code>Binding</code>
Add output to binding

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
**Returns**: <code>Binding</code> - `this`  
**Throws**:

- <code>ElementError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>output</td><td><code>Element</code> | <code>String</code> | <code>Object</code></td><td></td><td><p>Element/Selector/Object</p>
</td>
    </tr><tr>
    <td>[property]</td><td><code>String</code></td><td><code>textContent</code></td><td><p>Property to change, eg. &quot;style.fontSize&quot;</p>
</td>
    </tr><tr>
    <td>[transform]</td><td><code>function</code></td><td></td><td><p>Custom transform for this output</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+transform"></a>

#### binding.transform(transform) ⇒ <code>Binding</code>
Common transform function for all outputs

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
**Returns**: <code>Binding</code> - `this`  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>transform</td><td><code>function</code></td><td><p><code>function(inputValue, eventTarget)</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+once"></a>

#### binding.once(callback) ⇒ <code>Binding</code>
Run callback once per event (transforms are run per output)

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
**Returns**: <code>Binding</code> - `this`  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>callback</td><td><code>function</code></td><td><p><code>function(inputValue, eventTarget)</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+checked"></a>

#### binding.checked(callback) ⇒ <code>Binding</code>
Run callback when a checkable input (radio, checkbox) gets checked

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
**Returns**: <code>Binding</code> - `this`  
**Throws**:

- <code>TypeError</code><code>Error</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>callback</td><td><code>function</code></td><td><p><code>function(eventTarget)</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+unchecked"></a>

#### binding.unchecked(callback) ⇒ <code>Binding</code>
Run callback when a checkable input (radio, checkbox) gets unchecked

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
**Returns**: <code>Binding</code> - `this`  
**Throws**:

- <code>TypeError</code><code>Error</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>callback</td><td><code>function</code></td><td><p><code>function(eventTarget)</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+update"></a>

#### binding.update([newValue])
Update input value

Dispatches events to all the bindings associated with this input element.

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[newValue]</td><td><code>String</code> | <code>Number</code> | <code>Boolean</code></td><td><p>New value for input</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_bindings--Binding+remove"></a>

#### binding.remove()
Remove binding and the event listener

This does *not* remove all bindings associated with this input element.

**Kind**: instance method of [<code>Binding</code>](#exp_module_bindings--Binding)  

* * *

<a name="module_colors"></a>

## colors
Helper functions for modifying colors


* [colors](#module_colors)
    * [.rgb(color)](#module_colors.rgb) ⇒ <code>String</code>
    * [.hsl(color)](#module_colors.hsl) ⇒ <code>String</code>
    * [.darken(color, amount)](#module_colors.darken) ⇒ <code>String</code>
    * [.lighten(color, amount)](#module_colors.lighten) ⇒ <code>String</code>
    * [.saturate(color, amount)](#module_colors.saturate) ⇒ <code>String</code>
    * [.text(backgroundColor)](#module_colors.text) ⇒ <code>String</code>


* * *

<a name="module_colors.rgb"></a>

### fw.color.rgb(color) ⇒ <code>String</code>
Transform the color in to `rgb(...)`/`rgba(...)` form

`color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`

**Kind**: static method of [<code>colors</code>](#module_colors)  
**Throws**:

- <code>InvalidValueError</code><code>RangeError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>color</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_colors.hsl"></a>

### fw.color.hsl(color) ⇒ <code>String</code>
Transform the color in to `hsl(...)`/`hsla(...)` form

`color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`

**Kind**: static method of [<code>colors</code>](#module_colors)  
**Throws**:

- <code>InvalidValueError</code><code>RangeError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>color</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_colors.darken"></a>

### fw.color.darken(color, amount) ⇒ <code>String</code>
Darken the color by some amount

`amount` should be in range [0-1]

`color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`

**Kind**: static method of [<code>colors</code>](#module_colors)  
**Throws**:

- <code>InvalidValueError</code><code>RangeError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>color</td><td><code>String</code></td>
    </tr><tr>
    <td>amount</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_colors.lighten"></a>

### fw.color.lighten(color, amount) ⇒ <code>String</code>
Lighten the color by some amount

`amount` should be in range [0-1]

`color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`

**Kind**: static method of [<code>colors</code>](#module_colors)  
**Throws**:

- <code>InvalidValueError</code><code>RangeError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>color</td><td><code>String</code></td>
    </tr><tr>
    <td>amount</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_colors.saturate"></a>

### fw.color.saturate(color, amount) ⇒ <code>String</code>
Saturate the color by some amount

`amount` should be in range [0-1]

`color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`

**Kind**: static method of [<code>colors</code>](#module_colors)  
**Throws**:

- <code>InvalidValueError</code><code>RangeError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>color</td><td><code>String</code></td>
    </tr><tr>
    <td>amount</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_colors.text"></a>

### fw.color.text(backgroundColor) ⇒ <code>String</code>
Get a readable text color for the specified background color

`backgroundColor` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`

**Kind**: static method of [<code>colors</code>](#module_colors)  
**Throws**:

- <code>InvalidValueError</code><code>RangeError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>backgroundColor</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_errors"></a>

## errors
framework.js errors


* [errors](#module_errors)
    * [.ElementError](#module_errors.ElementError)
    * [.TemplateDefinedError](#module_errors.TemplateDefinedError)
    * [.ModelHasNoTemplateError](#module_errors.ModelHasNoTemplateError)
    * [.EmptyTemplateError](#module_errors.EmptyTemplateError)
    * [.InvalidValueError](#module_errors.InvalidValueError)
    * [.RequestSentError](#module_errors.RequestSentError)
    * [.OperationNotSupportedError](#module_errors.OperationNotSupportedError)


* * *

<a name="module_errors.ElementError"></a>

### fw.error.ElementError
Error thrown when element is invalid

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_errors.TemplateDefinedError"></a>

### fw.error.TemplateDefinedError
Error thrown when trying to define a template twice

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_errors.ModelHasNoTemplateError"></a>

### fw.error.ModelHasNoTemplateError
Error thrown when appending data to model that has no template

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_errors.EmptyTemplateError"></a>

### fw.error.EmptyTemplateError
Error thrown when template is empty

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_errors.InvalidValueError"></a>

### fw.error.InvalidValueError
Error thrown when value is invalid

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_errors.RequestSentError"></a>

### fw.error.RequestSentError
Error thrown when trying to send the same request twice

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_errors.OperationNotSupportedError"></a>

### fw.error.OperationNotSupportedError
Error thrown when trying to call a framework proxy method
that doesn't operate on arrays with an array

**Kind**: static class of [<code>errors</code>](#module_errors)  

* * *

<a name="module_messages"></a>

## messages
Show messages, either timed or clickable


* [messages](#module_messages)
    * [Message](#exp_module_messages--Message) ⏏
        * [new Message(parentElement)](#new_module_messages--Message_new)
        * [.timeout(milliseconds)](#module_messages--Message+timeout) ⇒ <code>Message</code>
        * [.overwrite([bool])](#module_messages--Message+overwrite) ⇒ <code>Message</code>
        * [.hide([bool])](#module_messages--Message+hide) ⇒ <code>Message</code>
        * [.transition(trans)](#module_messages--Message+transition) ⇒ <code>Message</code>
        * [.normal(content)](#module_messages--Message+normal)
        * [.urgent(content, [id], [detach])](#module_messages--Message+urgent)
        * [.remove()](#module_messages--Message+remove)


* * *

<a name="exp_module_messages--Message"></a>

### Message ⏏
Class for showing messages

**Kind**: global class of [<code>messages</code>](#module_messages)  

* * *

<a name="new_module_messages--Message_new"></a>

#### new Message(parentElement)
Create a new Message instance

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>parentElement</td><td><code>Element</code> | <code>String</code></td><td><p>Element/Selector</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+timeout"></a>

#### message.timeout(milliseconds) ⇒ <code>Message</code>
How long to show normal messages for

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  
**Default**: <code>4000</code>  
**Returns**: <code>Message</code> - `this`  
**Throws**:

- <code>TypeError</code><code>RangeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>milliseconds</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+overwrite"></a>

#### message.overwrite([bool]) ⇒ <code>Message</code>
Overwrite messages instead of appending them to the container

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  
**Default**: <code>true</code>  
**Returns**: <code>Message</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[bool]</td><td><code>Boolean</code></td><td><code>true</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+hide"></a>

#### message.hide([bool]) ⇒ <code>Message</code>
Hide container when there are no messages

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  
**Default**: <code>true</code>  
**Returns**: <code>Message</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[bool]</td><td><code>Boolean</code></td><td><code>true</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+transition"></a>

#### message.transition(trans) ⇒ <code>Message</code>
Set the transition for message elements

This is transition is used to show/hide the elements and the container.

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  
**Returns**: <code>Message</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>trans</td><td><code>Transition</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+normal"></a>

#### message.normal(content)
Show a normal message that goes away after a specified timeout

Element appended to the message container:
```html
<p class="fw-message-normal">content</p>
```

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>content</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+urgent"></a>

#### message.urgent(content, [id], [detach])
Show an urgent message that has to be clicked away

Urgent messages are not affected by timeout or overwrite.

The id parameter can be used to specify an identifier for this message.
If a message with the same id is already shown, the new message will be discarded.
This can be used to disable duplicate messages, so that the user
doesn't see several identical messages.

detach == false; element appended to the message container:
```html
<p class="fw-message-urgent">content</p>
```

detach == true; element appended to the document body:
```html
<aside class="fw-message-urgent fw-message-detached">content</aside>
```

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>content</td><td><code>String</code></td><td></td>
    </tr><tr>
    <td>[id]</td><td><code>String</code></td><td></td>
    </tr><tr>
    <td>[detach]</td><td><code>Boolean</code></td><td><code>false</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_messages--Message+remove"></a>

#### message.remove()
Remove messages and destroy this message container

**Kind**: instance method of [<code>Message</code>](#exp_module_messages--Message)  

* * *

<a name="module_models"></a>

## models
Create view models for your data


* [models](#module_models)
    * [Model](#exp_module_models--Model) ⏏
        * [new Model(parentElement)](#new_module_models--Model_new)
        * [.isEmpty](#module_models--Model+isEmpty) ⇒ <code>Boolean</code>
        * [.isFiltered](#module_models--Model+isFiltered) ⇒ <code>Boolean</code>
        * [.lazy([bool])](#module_models--Model+lazy) ⇒ <code>Model</code>
        * [.chunk(chunk)](#module_models--Model+chunk) ⇒ <code>Model</code>
        * [.template(tmpl)](#module_models--Model+template) ⇒ <code>Model</code>
        * [.visible(func)](#module_models--Model+visible) ⇒ <code>Model</code>
        * [.transition(trans)](#module_models--Model+transition) ⇒ <code>Model</code>
        * [.clear()](#module_models--Model+clear)
        * [.append(data)](#module_models--Model+append)
        * [.update(data)](#module_models--Model+update)
        * [.filter([callback])](#module_models--Model+filter)
        * [.getMatches()](#module_models--Model+getMatches) ⇒ <code>Boolean</code> \| <code>Array.&lt;Number&gt;</code>
        * [.getData(index)](#module_models--Model+getData) ⇒ <code>Object</code>
        * [.getElementIndex(index)](#module_models--Model+getElementIndex) ⇒ <code>Number</code> \| <code>Array.&lt;Number&gt;</code>
        * [.remove()](#module_models--Model+remove)


* * *

<a name="exp_module_models--Model"></a>

### Model ⏏
Class for creating view models

**Kind**: global class of [<code>models</code>](#module_models)  

* * *

<a name="new_module_models--Model_new"></a>

#### new Model(parentElement)
Create a new Model instance

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>parentElement</td><td><code>Element</code> | <code>String</code></td><td><p>Element/Selector</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+isEmpty"></a>

#### model.isEmpty ⇒ <code>Boolean</code>
Is model empty (i.e. has no data)

**Kind**: instance property of [<code>Model</code>](#exp_module_models--Model)  

* * *

<a name="module_models--Model+isFiltered"></a>

#### model.isFiltered ⇒ <code>Boolean</code>
Is model filtered

**Kind**: instance property of [<code>Model</code>](#exp_module_models--Model)  

* * *

<a name="module_models--Model+lazy"></a>

#### model.lazy([bool]) ⇒ <code>Model</code>
Lazy-load DOM elements

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Default**: <code>false</code>  
**Returns**: <code>Model</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[bool]</td><td><code>Boolean</code></td><td><code>true</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+chunk"></a>

#### model.chunk(chunk) ⇒ <code>Model</code>
How many DOM elements to load at a time

This option does nothing when lazy-loading isn't used.

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Default**: <code>1</code>  
**Returns**: <code>Model</code> - `this`  
**Throws**:

- <code>TypeError</code><code>RangeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>chunk</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+template"></a>

#### model.template(tmpl) ⇒ <code>Model</code>
Set the template of the model

Template data that should be replaced by actual data is identified
by an id string enclosed in curly brackets, e.g. {some-id}.

Strings that will have identifiers replaced by data:
```
'{id}'
'{!#¤%}'
'{123A_sdASJH}'
'  {whitespace_before_and_after}   '
'Full name: {fullName}'
'{fullName} - {occupation}'
'This value will be: {{enclosed_in_braces}}'
```

Strings that will be left as is:
```
'{ id}'
'{}'
'{no spaces}'
'{{}}'
'Something {something{}}'
```

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Returns**: <code>Model</code> - `this`  
**Throws**:

- <code>TemplateDefinedError</code><code>EmptyTemplateError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>tmpl</td><td><code>Element</code> | <code>String</code></td><td><p>HTML content or a <code>&lt;template&gt;</code> element</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
--- HTML:
<template id="my-template">
    <p class="container">
        <span>{first}</span>
        <span>{second}</span>
        <a href="{url}">{url}</a>
        <p>My image: <img src="{image}"></p>
    </p>
</template>

--- JavaScript:
const myModel = new fw.Model("#my-container");
myModel.template(fw.one("#my-template"));
myModel.append({
    first: "Yay!",
    second: "!yaY",
    url: "http://example.com",
    image: "images/hello.jpg"
});

--- Result:
<your-model-container-element id="my-container">
    <p class="container">
        <span>Yay!</span>
        <span>!yaY</span>
        <a href="http://example.com">http://example.com</a>
        <p>My image: <img src="images/hello.jpg"></p>
    </p>
</your-model-container-element>
```

* * *

<a name="module_models--Model+visible"></a>

#### model.visible(func) ⇒ <code>Model</code>
Specify a function to be called when elements become visible

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Returns**: <code>Model</code> - `this`  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>func</td><td><code>function</code></td><td><p><code>function(element)</code></p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
const model = new fw.Model("#my-model");
model.template(...);
model.visible((data, element) => {
    console.log(data, element);
});

model.append([{num: 1, name: "first"}, {num: 2, name: "second"}]);

// The function would now be called twice, with these parameters
// (if the appended data caused visible elements)
// (if your template has more than one root element, func gets called on all of them)
func(modelData[0], firstElement);
func(modelData[1], secondElement);
```

* * *

<a name="module_models--Model+transition"></a>

#### model.transition(trans) ⇒ <code>Model</code>
Set the transition for models child elements

This is transition is used to show/hide the elements.

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Returns**: <code>Model</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>trans</td><td><code>Transition</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+clear"></a>

#### model.clear()
Clear model data and remove all DOM elements

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  

* * *

<a name="module_models--Model+append"></a>

#### model.append(data)
Append data to the model

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Throws**:

- <code>ModelHasNoTemplateError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>Object</code> | <code>Array.&lt;Object&gt;</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+update"></a>

#### model.update(data)
Update model with data

This removes the old data and DOM elements.

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Throws**:

- <code>ModelHasNoTemplateError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>Object</code> | <code>Array.&lt;Object&gt;</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+filter"></a>

#### model.filter([callback])
Filter model

Shows elements that match the filter, hides the rest.
Calling filter with no arguments removes filtering.

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Throws**:

- <code>ModelHasNoTemplateError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[callback]</td><td><code>function</code></td><td><p><code>function(object, dataIndex)</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+getMatches"></a>

#### model.getMatches() ⇒ <code>Boolean</code> \| <code>Array.&lt;Number&gt;</code>
Get the data indices that match the filter

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Returns**: <code>Boolean</code> \| <code>Array.&lt;Number&gt;</code> - false if model is not filtered  

* * *

<a name="module_models--Model+getData"></a>

#### model.getData(index) ⇒ <code>Object</code>
Get data associated with this model at index

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>index</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+getElementIndex"></a>

#### model.getElementIndex(index) ⇒ <code>Number</code> \| <code>Array.&lt;Number&gt;</code>
Transform a data index to a element index

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  
**Returns**: <code>Number</code> \| <code>Array.&lt;Number&gt;</code> - Array of indices, if your template has many direct children  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>index</td><td><code>Number</code></td><td><p>Data index</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_models--Model+remove"></a>

#### model.remove()
Remove this model

Removes the model and all child elements.

**Kind**: instance method of [<code>Model</code>](#exp_module_models--Model)  

* * *

<a name="module_requests"></a>

## requests
Simplified web requests


* [requests](#module_requests)
    * [.Request](#module_requests.Request)
        * [new Request(url)](#new_module_requests.Request_new)
        * _instance_
            * [.method(requestMethod)](#module_requests.Request+method) ⇒ <code>Request</code>
            * [.type(responseType)](#module_requests.Request+type) ⇒ <code>Request</code>
            * [.body(data)](#module_requests.Request+body) ⇒ <code>Request</code>
            * [.header(name, value)](#module_requests.Request+header) ⇒ <code>Request</code>
        * _static_
            * [.is(first, second)](#module_requests.Request.is) ⇒ <code>Boolean</code>
    * [.Requester](#module_requests.Requester)
        * [new Requester()](#new_module_requests.Requester_new)
        * _instance_
            * [.parallel(requests)](#module_requests.Requester+parallel) ⇒ <code>Requester</code>
            * [.append(request)](#module_requests.Requester+append) ⇒ <code>Promise</code>
            * [.request(url, [type], [method])](#module_requests.Requester+request) ⇒ <code>Promise</code>
        * _static_
            * [.send(request)](#module_requests.Requester.send) ⇒ <code>Promise</code>
            * [.load(url, element, [append])](#module_requests.Requester.load)


* * *

<a name="module_requests.Request"></a>

### requests.Request
Class for creating requests

**Kind**: static class of [<code>requests</code>](#module_requests)  

* [.Request](#module_requests.Request)
    * [new Request(url)](#new_module_requests.Request_new)
    * _instance_
        * [.method(requestMethod)](#module_requests.Request+method) ⇒ <code>Request</code>
        * [.type(responseType)](#module_requests.Request+type) ⇒ <code>Request</code>
        * [.body(data)](#module_requests.Request+body) ⇒ <code>Request</code>
        * [.header(name, value)](#module_requests.Request+header) ⇒ <code>Request</code>
    * _static_
        * [.is(first, second)](#module_requests.Request.is) ⇒ <code>Boolean</code>


* * *

<a name="new_module_requests.Request_new"></a>

#### new Request(url)
Create a new Request instance

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>url</td><td><code>String</code> | <code>URL</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Request+method"></a>

#### request.method(requestMethod) ⇒ <code>Request</code>
Request method

The HTTP request method to use: GET, POST, etc.

**Kind**: instance method of [<code>Request</code>](#module_requests.Request)  
**Default**: <code>GET</code>  
**Returns**: <code>Request</code> - `this`  
**Throws**:

- <code>InvalidValueError</code><code>RequestSentError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>requestMethod</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Request+type"></a>

#### request.type(responseType) ⇒ <code>Request</code>
The type of the response body

This will decide the type of the body the request will eventually resolve with.

Possible values: `arrayBuffer`, `blob`, `formData`, `document`, `json`, `text`

**Kind**: instance method of [<code>Request</code>](#module_requests.Request)  
**Default**: <code>text</code>  
**Returns**: <code>Request</code> - `this`  
**Throws**:

- <code>InvalidValueError</code><code>RequestSentError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>responseType</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Request+body"></a>

#### request.body(data) ⇒ <code>Request</code>
Data to send to the server

**Kind**: instance method of [<code>Request</code>](#module_requests.Request)  
**Returns**: <code>Request</code> - `this`  
**Throws**:

- <code>RequestSentError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>data</td><td><code>Blob</code> | <code>BufferSource</code> | <code>FormData</code> | <code>URLSearchParams</code> | <code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Request+header"></a>

#### request.header(name, value) ⇒ <code>Request</code>
Add a request header

**Kind**: instance method of [<code>Request</code>](#module_requests.Request)  
**Returns**: <code>Request</code> - `this`  
**Throws**:

- <code>RequestSentError</code><code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>name</td><td><code>String</code></td>
    </tr><tr>
    <td>value</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Request.is"></a>

#### Request.is(first, second) ⇒ <code>Boolean</code>
Are requests the same

**Kind**: static method of [<code>Request</code>](#module_requests.Request)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>first</td><td><code>Request</code></td>
    </tr><tr>
    <td>second</td><td><code>Request</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Requester"></a>

### requests.Requester
Class for sending and queueing requests

**Kind**: static class of [<code>requests</code>](#module_requests)  

* [.Requester](#module_requests.Requester)
    * [new Requester()](#new_module_requests.Requester_new)
    * _instance_
        * [.parallel(requests)](#module_requests.Requester+parallel) ⇒ <code>Requester</code>
        * [.append(request)](#module_requests.Requester+append) ⇒ <code>Promise</code>
        * [.request(url, [type], [method])](#module_requests.Requester+request) ⇒ <code>Promise</code>
    * _static_
        * [.send(request)](#module_requests.Requester.send) ⇒ <code>Promise</code>
        * [.load(url, element, [append])](#module_requests.Requester.load)


* * *

<a name="new_module_requests.Requester_new"></a>

#### new Requester()
Create a new Requester instance


* * *

<a name="module_requests.Requester+parallel"></a>

#### requester.parallel(requests) ⇒ <code>Requester</code>
How many requests are handled in parallel

**Kind**: instance method of [<code>Requester</code>](#module_requests.Requester)  
**Default**: <code>1</code>  
**Returns**: <code>Requester</code> - `this`  
**Throws**:

- <code>TypeError</code><code>RangeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>requests</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Requester+append"></a>

#### requester.append(request) ⇒ <code>Promise</code>
Append a new request to the queue

**Kind**: instance method of [<code>Requester</code>](#module_requests.Requester)  
**Returns**: <code>Promise</code> - `resolve({body, response})`, `reject(error)`  
**Throws**:

- <code>RequestSentError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>request</td><td><code>Request</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Requester+request"></a>

#### requester.request(url, [type], [method]) ⇒ <code>Promise</code>
Convenience method for creating and appending a request

**Kind**: instance method of [<code>Requester</code>](#module_requests.Requester)  
**Returns**: <code>Promise</code> - `resolve({body, response})`, `reject(error)`  
**Throws**:

- <code>InvalidValueError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>url</td><td><code>String</code></td><td></td>
    </tr><tr>
    <td>[type]</td><td><code>String</code></td><td><code>text</code></td>
    </tr><tr>
    <td>[method]</td><td><code>String</code></td><td><code>GET</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Requester.send"></a>

#### Requester.send(request) ⇒ <code>Promise</code>
Send a request

**Kind**: static method of [<code>Requester</code>](#module_requests.Requester)  
**Returns**: <code>Promise</code> - `resolve({body, response})`, `reject(error)`  
**Throws**:

- <code>RequestSentError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>request</td><td><code>Request</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_requests.Requester.load"></a>

#### Requester.load(url, element, [append])
Load HTML from `url` and replace the contents of `element` with the result

If `append` is true, the resulting children will be appended to the element,
instead of replacing old children.

**Kind**: static method of [<code>Requester</code>](#module_requests.Requester)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>url</td><td><code>String</code> | <code>Url</code></td><td></td><td></td>
    </tr><tr>
    <td>element</td><td><code>String</code> | <code>Element</code></td><td></td><td><p>Element/Selector</p>
</td>
    </tr><tr>
    <td>[append]</td><td><code>Boolean</code></td><td><code>false</code></td><td></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions"></a>

## transitions
Create transitions for toggling the visibility of elements


* [transitions](#module_transitions)
    * [.TransitionChain](#module_transitions.TransitionChain)
        * [new TransitionChain()](#new_module_transitions.TransitionChain_new)
        * [.alternate([bool])](#module_transitions.TransitionChain+alternate) ⇒ <code>TransitionChain</code>
        * [.append(element, transition)](#module_transitions.TransitionChain+append) ⇒ <code>TransitionChain</code>
        * [.toggle([shown])](#module_transitions.TransitionChain+toggle) ⇒ <code>Promise</code>
        * [.hide()](#module_transitions.TransitionChain+hide) ⇒ <code>Promise</code>
        * [.show()](#module_transitions.TransitionChain+show) ⇒ <code>Promise</code>
    * [.Transition](#module_transitions.Transition)
        * [new Transition()](#new_module_transitions.Transition_new)
        * _instance_
            * [.duration(milliseconds)](#module_transitions.Transition+duration) ⇒ <code>Transition</code>
            * [.easing(timing)](#module_transitions.Transition+easing) ⇒ <code>Transition</code>
            * [.clipToParent([clip])](#module_transitions.Transition+clipToParent) ⇒ <code>Transition</code>
            * [.move(direction)](#module_transitions.Transition+move) ⇒ <code>Transition</code>
            * [.rotate(degrees)](#module_transitions.Transition+rotate) ⇒ <code>Transition</code>
            * [.scale([direction])](#module_transitions.Transition+scale) ⇒ <code>Transition</code>
            * [.width()](#module_transitions.Transition+width) ⇒ <code>Transition</code>
            * [.height()](#module_transitions.Transition+height) ⇒ <code>Transition</code>
            * [.opacity()](#module_transitions.Transition+opacity) ⇒ <code>Transition</code>
            * [.toggle(element, [shown])](#module_transitions.Transition+toggle) ⇒ <code>Promise</code>
            * [.hide(element)](#module_transitions.Transition+hide) ⇒ <code>Promise</code>
            * [.show(element)](#module_transitions.Transition+show) ⇒ <code>Promise</code>
        * _static_
            * [.toggle(element, [shown])](#module_transitions.Transition.toggle) ⇒ <code>Promise</code>
            * [.hide(element)](#module_transitions.Transition.hide) ⇒ <code>Promise</code>
            * [.show(element)](#module_transitions.Transition.show) ⇒ <code>Promise</code>


* * *

<a name="module_transitions.TransitionChain"></a>

### transitions.TransitionChain
Class for chaining transitions

**Kind**: static class of [<code>transitions</code>](#module_transitions)  

* [.TransitionChain](#module_transitions.TransitionChain)
    * [new TransitionChain()](#new_module_transitions.TransitionChain_new)
    * [.alternate([bool])](#module_transitions.TransitionChain+alternate) ⇒ <code>TransitionChain</code>
    * [.append(element, transition)](#module_transitions.TransitionChain+append) ⇒ <code>TransitionChain</code>
    * [.toggle([shown])](#module_transitions.TransitionChain+toggle) ⇒ <code>Promise</code>
    * [.hide()](#module_transitions.TransitionChain+hide) ⇒ <code>Promise</code>
    * [.show()](#module_transitions.TransitionChain+show) ⇒ <code>Promise</code>


* * *

<a name="new_module_transitions.TransitionChain_new"></a>

#### new TransitionChain()
Create a new TransitionChain instance


* * *

<a name="module_transitions.TransitionChain+alternate"></a>

#### transitionChain.alternate([bool]) ⇒ <code>TransitionChain</code>
Alternate direction of the chain after running transitions

Possibly useful if elements in the chain have a parent-child relation.

**Kind**: instance method of [<code>TransitionChain</code>](#module_transitions.TransitionChain)  
**Default**: <code>false</code>  
**Returns**: <code>TransitionChain</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[bool]</td><td><code>Boolean</code></td><td><code>true</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.TransitionChain+append"></a>

#### transitionChain.append(element, transition) ⇒ <code>TransitionChain</code>
Append an element and its transition to the chain

**Kind**: instance method of [<code>TransitionChain</code>](#module_transitions.TransitionChain)  
**Returns**: <code>TransitionChain</code> - `this`  
**Throws**:

- <code>TypeError</code><code>InvalidValueError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code></td><td><p>Element/Selector</p>
</td>
    </tr><tr>
    <td>transition</td><td><code>Transition</code></td><td></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.TransitionChain+toggle"></a>

#### transitionChain.toggle([shown]) ⇒ <code>Promise</code>
Toggle the visibility of all the elements in the chain

**Kind**: instance method of [<code>TransitionChain</code>](#module_transitions.TransitionChain)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[shown]</td><td><code>Boolean</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.TransitionChain+hide"></a>

#### transitionChain.hide() ⇒ <code>Promise</code>
Hide all the elements in the chain

**Kind**: instance method of [<code>TransitionChain</code>](#module_transitions.TransitionChain)  

* * *

<a name="module_transitions.TransitionChain+show"></a>

#### transitionChain.show() ⇒ <code>Promise</code>
Show all the elements in the chain

**Kind**: instance method of [<code>TransitionChain</code>](#module_transitions.TransitionChain)  

* * *

<a name="module_transitions.Transition"></a>

### transitions.Transition
Class for element transitions

**Kind**: static class of [<code>transitions</code>](#module_transitions)  

* [.Transition](#module_transitions.Transition)
    * [new Transition()](#new_module_transitions.Transition_new)
    * _instance_
        * [.duration(milliseconds)](#module_transitions.Transition+duration) ⇒ <code>Transition</code>
        * [.easing(timing)](#module_transitions.Transition+easing) ⇒ <code>Transition</code>
        * [.clipToParent([clip])](#module_transitions.Transition+clipToParent) ⇒ <code>Transition</code>
        * [.move(direction)](#module_transitions.Transition+move) ⇒ <code>Transition</code>
        * [.rotate(degrees)](#module_transitions.Transition+rotate) ⇒ <code>Transition</code>
        * [.scale([direction])](#module_transitions.Transition+scale) ⇒ <code>Transition</code>
        * [.width()](#module_transitions.Transition+width) ⇒ <code>Transition</code>
        * [.height()](#module_transitions.Transition+height) ⇒ <code>Transition</code>
        * [.opacity()](#module_transitions.Transition+opacity) ⇒ <code>Transition</code>
        * [.toggle(element, [shown])](#module_transitions.Transition+toggle) ⇒ <code>Promise</code>
        * [.hide(element)](#module_transitions.Transition+hide) ⇒ <code>Promise</code>
        * [.show(element)](#module_transitions.Transition+show) ⇒ <code>Promise</code>
    * _static_
        * [.toggle(element, [shown])](#module_transitions.Transition.toggle) ⇒ <code>Promise</code>
        * [.hide(element)](#module_transitions.Transition.hide) ⇒ <code>Promise</code>
        * [.show(element)](#module_transitions.Transition.show) ⇒ <code>Promise</code>


* * *

<a name="new_module_transitions.Transition_new"></a>

#### new Transition()
Create a new Transition instance


* * *

<a name="module_transitions.Transition+duration"></a>

#### transition.duration(milliseconds) ⇒ <code>Transition</code>
The length of the transition

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Default**: <code>200</code>  
**Returns**: <code>Transition</code> - `this`  
**Throws**:

- <code>TypeError</code><code>RangeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>milliseconds</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+easing"></a>

#### transition.easing(timing) ⇒ <code>Transition</code>
Timing function to use for the transition

Possible values: linear, ease, ease-in, ease-out, ease-in-out

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Default**: <code>linear</code>  
**Returns**: <code>Transition</code> - `this`  
**Throws**:

- <code>InvalidValueError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>timing</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+clipToParent"></a>

#### transition.clipToParent([clip]) ⇒ <code>Transition</code>
Clip transitioning element to its parent

Only useful when transition might overflow the parent element,
e.g. when rotating or moving an element.

This will set the parent element `clip-path` to `inset(0 0)`, and also
slightly modifies the way `move` works.

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Default**: <code>false</code>  
**Returns**: <code>Transition</code> - `this`  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[clip]</td><td><code>Boolean</code></td><td><code>true</code></td>
    </tr>  </tbody>
</table>

**Example**  
```js
const noClip = (new fw.Transition()).move("up");
const withClip = (new fw.Transition()).move("up").clipToParent();

// This will move #elementOne completely off screen
noClip.hide("#elementOne");
// This will move #elementTwo just outside of the parent
withClip.hide("#elementTwo");
```

* * *

<a name="module_transitions.Transition+move"></a>

#### transition.move(direction) ⇒ <code>Transition</code>
Move the element off screen during transition

Possible values for direction: up, right, down, left

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Transition</code> - `this`  
**Throws**:

- <code>InvalidValueError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>direction</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+rotate"></a>

#### transition.rotate(degrees) ⇒ <code>Transition</code>
Rotate the element during transition

Rotate an element by the specified degrees.
Negative degrees cause rotation to the left, positive to the right.

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Transition</code> - `this`  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>degrees</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+scale"></a>

#### transition.scale([direction]) ⇒ <code>Transition</code>
Scale the element during transition

This option scales the element with `transform: scale()`.
To change the actual size of the element use `width` and/or `height`.

The direction parameter specifies which direction to scale the element.
Possible values for direction: horizontal, vertical, both.

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Transition</code> - `this`  
**Throws**:

- <code>InvalidValueError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[direction]</td><td><code>String</code></td><td><code>both</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+width"></a>

#### transition.width() ⇒ <code>Transition</code>
Change the width of the element during transition

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Transition</code> - `this`  

* * *

<a name="module_transitions.Transition+height"></a>

#### transition.height() ⇒ <code>Transition</code>
Change the height of the element during transition

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Transition</code> - `this`  

* * *

<a name="module_transitions.Transition+opacity"></a>

#### transition.opacity() ⇒ <code>Transition</code>
Fade the element in or out during transition

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Transition</code> - `this`  

* * *

<a name="module_transitions.Transition+toggle"></a>

#### transition.toggle(element, [shown]) ⇒ <code>Promise</code>
Toggle the visibility of element(s)

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Promise</code> - Resolves when transition is finished  
**Throws**:

- <code>ElementError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code> | <code>Array.&lt;Element&gt;</code> | <code>Array.&lt;String&gt;</code></td><td><p>Element(s)/Selector(s)</p>
</td>
    </tr><tr>
    <td>[shown]</td><td><code>Boolean</code></td><td></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+hide"></a>

#### transition.hide(element) ⇒ <code>Promise</code>
Hide element(s)

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Promise</code> - Resolves when transition is finished  
**Throws**:

- <code>ElementError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code> | <code>Array.&lt;Element&gt;</code> | <code>Array.&lt;String&gt;</code></td><td><p>Element(s)/Selector(s)</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition+show"></a>

#### transition.show(element) ⇒ <code>Promise</code>
Show element(s)

**Kind**: instance method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Promise</code> - Resolves when transition is finished  
**Throws**:

- <code>ElementError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code> | <code>Array.&lt;Element&gt;</code> | <code>Array.&lt;String&gt;</code></td><td><p>Element(s)/Selector(s)</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition.toggle"></a>

#### Transition.toggle(element, [shown]) ⇒ <code>Promise</code>
Toggle the visibility of element(s) synchronously

If transition is already running on the element, this has to wait for
the transition to cancel, which is an asynchronous operation.

**Kind**: static method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Promise</code> - Resolves when transition is finished  
**Throws**:

- <code>ElementError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code> | <code>Array.&lt;Element&gt;</code> | <code>Array.&lt;String&gt;</code></td><td><p>Element(s)/Selector(s)</p>
</td>
    </tr><tr>
    <td>[shown]</td><td><code>Boolean</code></td><td></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition.hide"></a>

#### Transition.hide(element) ⇒ <code>Promise</code>
Hide element(s) synchronously

If transition is already running on the element, this has to wait for
the transition to cancel, which is an asynchronous operation.

**Kind**: static method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Promise</code> - Resolves when transition is finished  
**Throws**:

- <code>ElementError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code> | <code>Array.&lt;Element&gt;</code> | <code>Array.&lt;String&gt;</code></td><td><p>Element(s)/Selector(s)</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_transitions.Transition.show"></a>

#### Transition.show(element) ⇒ <code>Promise</code>
Show element(s) synchronously

If transition is already running on the element, this has to wait for
the transition to cancel, which is an asynchronous operation.

**Kind**: static method of [<code>Transition</code>](#module_transitions.Transition)  
**Returns**: <code>Promise</code> - Resolves when transition is finished  
**Throws**:

- <code>ElementError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>Element</code> | <code>String</code> | <code>Array.&lt;Element&gt;</code> | <code>Array.&lt;String&gt;</code></td><td><p>Element(s)/Selector(s)</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_ui"></a>

## ui
Common user interface components


* [ui](#module_ui)
    * [.TabView](#module_ui.TabView)
        * [new TabView(element, [activeIndex], [type])](#new_module_ui.TabView_new)
        * [.active](#module_ui.TabView+active) ⇒ <code>Number</code>
        * [.active](#module_ui.TabView+active)
        * [.changed(callback)](#module_ui.TabView+changed) ⇒ <code>TabView</code>
        * [.transition(trans)](#module_ui.TabView+transition) ⇒ <code>TabView</code>
        * [.remove()](#module_ui.TabView+remove)
    * [.TabType](#module_ui.TabType) : <code>enum</code>


* * *

<a name="module_ui.TabView"></a>

### ui.TabView
Class for creating tab views

**Kind**: static class of [<code>ui</code>](#module_ui)  

* [.TabView](#module_ui.TabView)
    * [new TabView(element, [activeIndex], [type])](#new_module_ui.TabView_new)
    * [.active](#module_ui.TabView+active) ⇒ <code>Number</code>
    * [.active](#module_ui.TabView+active)
    * [.changed(callback)](#module_ui.TabView+changed) ⇒ <code>TabView</code>
    * [.transition(trans)](#module_ui.TabView+transition) ⇒ <code>TabView</code>
    * [.remove()](#module_ui.TabView+remove)


* * *

<a name="new_module_ui.TabView_new"></a>

#### new TabView(element, [activeIndex], [type])
Create a new TabView instance

This will transform the element into a tab view.

The elements direct children will become the tabs, and links to change
the tab will be added to the top of the element. If vertical tabs were
requested, links will be added vertically between tabs.

The elements don't have to be divs, but they should be block-level elements.

Original tree:
```html
<div id="my-tabview">
    <div id="my-tab-1" title="First tab title">Content for first tab</div>
    <div id="my-tab-2" title="Second tab title">Content for second tab</div>
    <div id="my-tab-3" title="Third tab title">Content for third tab</div>
</div>
```

default mode:
```html
<div id="my-tabview" class="fw-tab-container">
    <div class="fw-tab-header">
        <a class="fw-tab-title fw-tab-title-active">First tab title</a>
        <a class="fw-tab-title">Second tab title</a>
        <a class="fw-tab-title">Third tab title</a>
    </div>
    <div class="fw-tab">
        <div id="my-tab-1" class="fw-tab-body fw-tab-body-active">Content for first tab</div>
        <div id="my-tab-2" class="fw-tab-body" style="display:none">Content for second tab</div>
        <div id="my-tab-3" class="fw-tab-body" style="display:none">Content for third tab</div>
    </div>
</div>
```

vertical mode:
```html
<div id="my-tabview" class="fw-tab-container fw-tab-vertical">
    <div class="fw-tab">
        <div class="fw-tab-header">
            <a class="fw-tab-title fw-tab-title-active">First tab title</a>
        </div>
        <div id="my-tab-1" class="fw-tab-body fw-tab-body-active">Content for first tab</div>
    </div>
    <div class="fw-tab">
        <div class="fw-tab-header">
            <a class="fw-tab-title">Second tab title</a>
        </div>
        <div id="my-tab-2" class="fw-tab-body" style="display:none">Content for second tab</div>
    </div>
    <div class="fw-tab">
        <div class="fw-tab-header">
            <a class="fw-tab-title">Third tab title</a>
        </div>
        <div id="my-tab-3" class="fw-tab-body" style="display:none">Content for third tab</div>
    </div>
</div>
```

The tab view is 'live', i.e. it will update with a new tab
when a new child element is added to `#my-tabview`.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>element</td><td><code>String</code> | <code>Element</code></td><td></td><td><p>Element/Selector</p>
</td>
    </tr><tr>
    <td>[activeIndex]</td><td><code>Number</code></td><td><code>0</code></td><td><p>Index of tab to set active</p>
</td>
    </tr><tr>
    <td>[type]</td><td><code>TabType</code></td><td><code>TabType.Normal</code></td><td></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_ui.TabView+active"></a>

#### tabView.active ⇒ <code>Number</code>
Get the active tab index

**Kind**: instance property of [<code>TabView</code>](#module_ui.TabView)  

* * *

<a name="module_ui.TabView+active"></a>

#### tabView.active
Set the active tab

This changes the visible tab to be the one at `index`.

**Kind**: instance property of [<code>TabView</code>](#module_ui.TabView)  
**Throws**:

- <code>TypeError</code><code>RangeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>index</td><td><code>Number</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_ui.TabView+changed"></a>

#### tabView.changed(callback) ⇒ <code>TabView</code>
This callback gets called when user changes tabs

**Kind**: instance method of [<code>TabView</code>](#module_ui.TabView)  
**Returns**: <code>TabView</code> - `this`  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>callback</td><td><code>function</code></td><td><p><code>function(index, tabElement)</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="module_ui.TabView+transition"></a>

#### tabView.transition(trans) ⇒ <code>TabView</code>
Set the transition to use when user changes tabs

**Kind**: instance method of [<code>TabView</code>](#module_ui.TabView)  
**Returns**: <code>TabView</code> - `this`  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>trans</td><td><code>Transition</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="module_ui.TabView+remove"></a>

#### tabView.remove()
Destroy tab view and remove listeners

After this the parent element will be empty.

**Kind**: instance method of [<code>TabView</code>](#module_ui.TabView)  

* * *

<a name="module_ui.TabType"></a>

### ui.TabType : <code>enum</code>
TabType enum

**Kind**: static enum of [<code>ui</code>](#module_ui)  
**Properties**

<table>
  <thead>
    <tr>
      <th>Name</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>Normal</td><td><code>Number</code></td><td><code>1</code></td><td><p>Normal tab view, tab buttons at the top and content at bottom</p>
</td>
    </tr><tr>
    <td>Vertical</td><td><code>Number</code></td><td><code>2</code></td><td><p>Vertical tab view (aka accordion), tab buttons mixed in with content</p>
</td>
    </tr><tr>
    <td>HideButtons</td><td><code>Number</code></td><td><code>4</code></td><td><p>Hide tab buttons</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy"></a>

## frameworkProxy
framework.js proxy

These are the methods available in the Proxy returned by [fw(selector)](#fw).

**Kind**: global constant  
**Example**  
```js
// Some methods can work with several elements

// All elements matching 'a.navigation' will be hidden
fw("a.navigation").$hide();
// All elements matching 'p' will now have the class my-paragraph
fw("p").$addClass("my-paragraph");


// Some methods can only work with single elements
const tabView = fw("#my-tab-view").$tabView();

// This would result in error (if document has many divs)
fw("div").$tabView();


// If fw(...) results in a single element, all the elements members can be accessed normally
fw("#main-navigation").children;  // HTMLCollection
fw("#main-navigation").textContent = "No navigation for you";


// If fw(...) results in several elements, all of the Array.prototype methods work normally
fw("div").forEach(div => { div.textContent = "Noice." });

// NOTE: the elements in the array are just basic unmodified Elements
// i.e. This wont work:
fw("div").forEach(div => div.$hide());  // Using a proxy method here will fail

// The elements need to be wrapped up first:
fw("div").forEach(div => fw(div).$hide());  // This will work
```

* [frameworkProxy](#frameworkProxy)
    * [.$raw()](#frameworkProxy.$raw) ⇒ <code>Element</code> \| <code>Array.&lt;Element&gt;</code>
    * [.$forEach(callback, [thisArg])](#frameworkProxy.$forEach)
    * [.$removeChildren()](#frameworkProxy.$removeChildren)
    * [.$toggleClass(cls)](#frameworkProxy.$toggleClass)
    * [.$addClass(cls)](#frameworkProxy.$addClass)
    * [.$removeClass(cls)](#frameworkProxy.$removeClass)
    * [.$replaceClass(cls, replacement)](#frameworkProxy.$replaceClass)
    * [.$binding([event])](#frameworkProxy.$binding) ⇒ [<code>Binding</code>](#fw.Binding)
    * [.$model()](#frameworkProxy.$model) ⇒ [<code>Model</code>](#fw.Model)
    * [.$message()](#frameworkProxy.$message) ⇒ [<code>Message</code>](#fw.Message)
    * [.$toggle([transition], [shown])](#frameworkProxy.$toggle) ⇒ <code>Promise</code>
    * [.$hide([transition])](#frameworkProxy.$hide) ⇒ <code>Promise</code>
    * [.$show([transition])](#frameworkProxy.$show) ⇒ <code>Promise</code>
    * [.$tabView([activeIndex], [type])](#frameworkProxy.$tabView) ⇒ <code>fw.ui.TabView</code>


* * *

<a name="frameworkProxy.$raw"></a>

### fw(selector).$raw() ⇒ <code>Element</code> \| <code>Array.&lt;Element&gt;</code>
Returns the raw, unproxied target

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  

* * *

<a name="frameworkProxy.$forEach"></a>

### fw(selector).$forEach(callback, [thisArg])
Support for forEach

This is guaranteed to work even if `fw(selector)` matches only one element.
A normal forEach won't work in that case, as the proxy target will not be an array.

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>callback</td><td><code>function</code></td><td><p>The callback function</p>
</td>
    </tr><tr>
    <td>[thisArg]</td><td><code>*</code></td><td><p>The value of <code>this</code> in <code>callback</code></p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$removeChildren"></a>

### fw(selector).$removeChildren()
Remove all the children of the element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  

* * *

<a name="frameworkProxy.$toggleClass"></a>

### fw(selector).$toggleClass(cls)
Toggle a class of the element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>cls</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$addClass"></a>

### fw(selector).$addClass(cls)
Add a class to the element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>cls</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$removeClass"></a>

### fw(selector).$removeClass(cls)
Remove a class from the element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>cls</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$replaceClass"></a>

### fw(selector).$replaceClass(cls, replacement)
Replace a class of the element(s) with another class

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>cls</td><td><code>String</code></td>
    </tr><tr>
    <td>replacement</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$binding"></a>

### fw(selector).$binding([event]) ⇒ [<code>Binding</code>](#fw.Binding)
Create a binding for this input

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**Throws**:

- <code>OperationNotSupportedError</code> 

**See**: [bindings](#module_bindings)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[event]</td><td><code>String</code></td><td><code>change</code></td><td><p>Event type</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
const binding = fw("#my-input").$binding("input");

// This would set the title of the document to
// whatever is the value of the input in uppercase
binding.output(document, "title").transform(function(value) {
    return value.toUpperCase();
});
```

* * *

<a name="frameworkProxy.$model"></a>

### fw(selector).$model() ⇒ [<code>Model</code>](#fw.Model)
Use the element as a container for a view model

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**Throws**:

- <code>OperationNotSupportedError</code> 

**See**: [models](#module_models)  
**Example**  
```js
const model = fw("#my-model-container").$model();

model.template("<p><em>{name}</em>: {occupation}</p>");

model.append({name: "Full Name", occupation: "Worker"});
model.append({name: "Firstname Lastname", occupation: "Work-doer"});

#my-model-container:
<p><em>Full Name</em>: Worker</p>
<p><em>Firstname Lastname</em>: Work-doer</p>
```

* * *

<a name="frameworkProxy.$message"></a>

### fw(selector).$message() ⇒ [<code>Message</code>](#fw.Message)
Use the element as a container for messages

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**Throws**:

- <code>OperationNotSupportedError</code> 

**See**: [messages](#module_messages)  
**Example**  
```js
const message = fw("#my-message-container").$message();

// Disappears after some time
message.normal("This is a normal, timed message");

// Disappears when clicked
message.urgent("This message has to be clicked away");

// Disappears when clicked
message.urgent("This message has an id", "my-message");

// Message with the same id is already shown
message.urgent("This message won't be shown at all", "my-message");
```

* * *

<a name="frameworkProxy.$toggle"></a>

### fw(selector).$toggle([transition], [shown]) ⇒ <code>Promise</code>
Toggle the visibility of element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**See**: [transitions](#module_transitions)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[transition]</td><td><code><a href="#fw.Transition">Transition</a></code></td>
    </tr><tr>
    <td>[shown]</td><td><code>Boolean</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$hide"></a>

### fw(selector).$hide([transition]) ⇒ <code>Promise</code>
Hide the element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**See**: [transitions](#module_transitions)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[transition]</td><td><code><a href="#fw.Transition">Transition</a></code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$show"></a>

### fw(selector).$show([transition]) ⇒ <code>Promise</code>
Show the element(s)

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**See**: [transitions](#module_transitions)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[transition]</td><td><code><a href="#fw.Transition">Transition</a></code></td>
    </tr>  </tbody>
</table>


* * *

<a name="frameworkProxy.$tabView"></a>

### fw(selector).$tabView([activeIndex], [type]) ⇒ <code>fw.ui.TabView</code>
Transform the element into a tabbed view

**Kind**: static method of [<code>frameworkProxy</code>](#frameworkProxy)  
**Throws**:

- <code>OperationNotSupportedError</code> 

**See**: [ui](#module_ui)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Default</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>[activeIndex]</td><td><code>Number</code></td><td><code>0</code></td><td><p>Index of tab to set active</p>
</td>
    </tr><tr>
    <td>[type]</td><td><code>fw.ui.TabType</code></td><td><code>fw.ui.TabType.Normal</code></td><td></td>
    </tr>  </tbody>
</table>


* * *

<a name="fw"></a>

## fw(selector) ⇒ <code>Element</code> \| <code>Array.&lt;Element&gt;</code>
The access point for all the framework.js functionality

**Kind**: global function  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>selector</td><td><code>String</code> | <code>Element</code></td>
    </tr>  </tbody>
</table>

**Example** *(fw is a global function)*  
```js
fw.load(function() {  // Run something on page load
    const fade = new fw.Transition();
    fade.opacity().duration(1000);

    fw("#my-element").$hide(fade);

    const model = fw("#model-container").$model();

    try {
        model.template('');
    }
    catch(e) {
        e instanceof fw.error.EmptyTemplateError === true;
    }
});
```

* [fw(selector)](#fw) ⇒ <code>Element</code> \| <code>Array.&lt;Element&gt;</code>
    * [.error](#fw.error)
    * [.color](#fw.color)
    * [.ui](#fw.ui)
    * [.Binding](#fw.Binding)
    * [.Model](#fw.Model)
    * [.Message](#fw.Message)
    * [.TransitionChain](#fw.TransitionChain)
    * [.Transition](#fw.Transition)
    * [.Request](#fw.Request)
    * [.Requester](#fw.Requester)
    * [.init()](#fw.init)
    * [.load(func)](#fw.load)
    * [.ready(func)](#fw.ready)
    * [.element(tagName, [attrs])](#fw.element) ⇒ <code>Element</code>
    * [.text(textContent)](#fw.text) ⇒ <code>Node</code>


* * *

<a name="fw.error"></a>

### fw.error
framework.js errors

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [errors](#module_errors)  

* * *

<a name="fw.color"></a>

### fw.color
Helper functions for dealing with colors

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [colors](#module_colors)  
**Example**  
```js
fw.color.darken("#ffffff", 0.5) === "rgb(128, 128, 128)";
fw.color.darken("rgb(255, 255, 255)", 0.5) === "rgb(128, 128, 128)";

// Get a readable text color for a background
fw.color.text("#ffeedd") === "rgb(0, 0, 0)";
fw.color.text("#402040") === "rgb(255, 255, 255)";
```

* * *

<a name="fw.ui"></a>

### fw.ui
User interface classes

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [ui](#module_ui)  

* * *

<a name="fw.Binding"></a>

### fw.Binding
The Binding class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [bindings](#module_bindings)  

* * *

<a name="fw.Model"></a>

### fw.Model
The Model class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [models](#module_models)  

* * *

<a name="fw.Message"></a>

### fw.Message
The Message class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [messages](#module_messages)  

* * *

<a name="fw.TransitionChain"></a>

### fw.TransitionChain
The TransitionChain class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [TransitionChain](#module_transitions.TransitionChain)  
**Example**  
```js
const chain = new fw.TransitionChain();
const fade = (new fw.Transition()).opacity().duration(100);
const scale = (new fw.Transition()).scale().duration(200);

chain.append("#parent", fade).append("#child", scale);

fw.Transition.hide("#parent");
fw.Transition.hide("#child");

// This will result in #parent element fading in
// and after that the #child element scaling in.
chain.show().then(() => {
    // if the alternate option was set (chain.alternate()),
    // this will run the chain in reverse
    chain.hide();
});
```

* * *

<a name="fw.Transition"></a>

### fw.Transition
The Transition class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [Transition](#module_transitions.Transition)  
**Example**  
```js
fw.Transition.toggle(element);  // Synchronous toggling of element

const transition = new fw.Transition();
transition.opacity().duration(500);

transition.toggle(element);  // Async toggle that fades element in/out
                             // Returns a Promise
```

* * *

<a name="fw.Request"></a>

### fw.Request
The Request class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [Request](#module_requests.Request)  
**Example**  
```js
const request = new fw.Request("http://example.com");
request.type("document").method("POST");

fw.Requester.send(request).then(({body, response}) => { ... });
```

* * *

<a name="fw.Requester"></a>

### fw.Requester
The Requester class

**Kind**: static property of [<code>fw</code>](#fw)  
**See**: [Requester](#module_requests.Requester)  
**Example**  
```js
const requester = new fw.Requester();
const request = new fw.Request("http://example.com/page2");

requester.parallel(10); // Default is 1

// A convenience method for creating and appending a simple request
requester.request("http://example.com").then(({body, response}) => { ... });

// Append a request
requester.append(request).then(({body, response}) => { ... });
```
**Example**  
```js
// Requester has a static method to simply send requests, without dealing with queues
fw.Requester.send(someRequest);
```

* * *

<a name="fw.init"></a>

### fw.init()
Initialize framework.js

Usually run once on page load.

**Kind**: static method of [<code>fw</code>](#fw)  

* * *

<a name="fw.load"></a>

### fw.load(func)
Run a function when page and all it's resources have loaded

For most cases `fw.ready` is a better choice.

Can be called several times, functions will be called
(mostly) in the order they were given.

**Kind**: static method of [<code>fw</code>](#fw)  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>func</td><td><code>function</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="fw.ready"></a>

### fw.ready(func)
Run a function when document parsing is finished

This is faster than using `fw.load`.

Can be called several times, functions will be called
(mostly) in the order they were given.

**Kind**: static method of [<code>fw</code>](#fw)  
**Throws**:

- <code>TypeError</code> 

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>func</td><td><code>function</code></td>
    </tr>  </tbody>
</table>


* * *

<a name="fw.element"></a>

### fw.element(tagName, [attrs]) ⇒ <code>Element</code>
Create a new element of type tagName, with optional attributes

**Kind**: static method of [<code>fw</code>](#fw)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>tagName</td><td><code>String</code></td><td></td>
    </tr><tr>
    <td>[attrs]</td><td><code>Object</code></td><td><p>Attributes to set</p>
</td>
    </tr>  </tbody>
</table>


* * *

<a name="fw.text"></a>

### fw.text(textContent) ⇒ <code>Node</code>
Create a new text node

**Kind**: static method of [<code>fw</code>](#fw)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>textContent</td><td><code>String</code></td>
    </tr>  </tbody>
</table>


* * *

