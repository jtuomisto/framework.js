var fw =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/fw.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/arrays.js":
/*!***********************!*\
  !*** ./src/arrays.js ***!
  \***********************/
/*! exports provided: filter, map, find, findIndex, forEach, reduce, reduceRight, every, some, flatMap, concat, createArray, isArrayLike */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filter", function() { return filter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "map", function() { return map; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "find", function() { return find; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findIndex", function() { return findIndex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "forEach", function() { return forEach; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reduce", function() { return reduce; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reduceRight", function() { return reduceRight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "every", function() { return every; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "some", function() { return some; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "flatMap", function() { return flatMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "concat", function() { return concat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createArray", function() { return createArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isArrayLike", function() { return isArrayLike; });
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./misc */ "./src/misc.js");



function base(proto) {
    return function(list, callback, ...args) {
        if(!isArrayLike(list))
            throw new TypeError("Parameter wasn't iterable");

        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_0__["isFunction"])(callback))
            throw new TypeError("Parameter wasn't a function");

        // Getting this far means list is some kind of iterable
        //
        // If list is not an array and doesn't have a length property,
        // it's propably an iterator, which means it should be made in to an array
        //
        // Array.prototypes can deal with some non-array lists directly eg.
        // HTMLCollection, NodeList
        // They all have the length property and will fail this test
        if(!Array.isArray(list) && Object(_misc__WEBPACK_IMPORTED_MODULE_0__["isUndefined"])(list.length))
            list = [...list];

        return proto.call(list, callback, ...args);
    };
}


/**
 * filter(list, callback, [thisArg])
 * @private
 */
const filter = base(Array.prototype.filter);

/**
 * map(list, callback, [thisArg])
 * @private
 */
const map = base(Array.prototype.map);

/**
 * find(list, callback, [thisArg])
 * @private
 */
const find = base(Array.prototype.find);

/**
 * findIndex(list, callback, [thisArg])
 * @private
 */
const findIndex = base(Array.prototype.findIndex);

/**
 * forEach(list, callback, [thisArg])
 * @private
 */
const forEach = base(Array.prototype.forEach);

/**
 * reduce(list, callback, [initialValue])
 * @private
 */
const reduce = base(Array.prototype.reduce);

/**
 * reduceRight(list, callback, [initialValue])
 * @private
 */
const reduceRight = base(Array.prototype.reduceRight);

/**
 * every(list, callback, [thisArg])
 * @private
 */
const every = base(Array.prototype.every);

/**
 * some(list, callback, [thisArg])
 * @private
 */
const some = base(Array.prototype.some);

/**
 * flatMap(list, callback, [thisArg])
 * @private
 */
function flatMap(list, callback, ...args) {
    return map(list, callback, ...args).reduce((acc, a) => acc.concat(a), []);
}

/**
 * concat(...)
 * @private
 */
function concat(...args) {
    return args.map(a => !Array.isArray(a) && isArrayLike(a) ? [...a] : a)
               .reduce((acc, a) => acc.concat(a), []);
}


function createArray(length) {
    return [...(new Array(length)).keys()];
}


function isArrayLike(value) {
    return !Object(_misc__WEBPACK_IMPORTED_MODULE_0__["isNone"])(value) && !Object(_misc__WEBPACK_IMPORTED_MODULE_0__["isString"])(value) && !Object(_misc__WEBPACK_IMPORTED_MODULE_0__["isUndefined"])(value[Symbol.iterator]);
}


/**
 * @module arrays
 * @private
 */



/***/ }),

/***/ "./src/bindings.js":
/*!*************************!*\
  !*** ./src/bindings.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./misc */ "./src/misc.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./errors */ "./src/errors.js");





const DEFAULT_PROP = "textContent";

const bindings = new Map();


/**
 * Class for binding inputs
 *
 * @alias module:bindings
 */
class Binding {
    /**
     * @private
     */
    static _initialize() {
        _arrays__WEBPACK_IMPORTED_MODULE_0__["filter"](bindings.keys(), input => input.type !== "file")
              .forEach(input => Binding._dispatch(input));
    }

    /**
     * @private
     */
    static _dispatch(input) {
        const bind = bindings.get(input);
        const events = _arrays__WEBPACK_IMPORTED_MODULE_0__["map"](bind.instances.values(), instance => instance._event);
        const dispatched = {};

        events.forEach(event => {
            if(!dispatched[event]) {
                input.dispatchEvent(new Event(event));
                dispatched[event] = true;
            }
        });
    }

    /**
     * Create a new Binding instance
     *
     * @param {(Element|String)} inputElement Input element/Selector
     * @param {String} [event=change] Event type
     * @throws {ElementError}
     */
    constructor(inputElement, event) {
        this._input = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["realElement"])(inputElement);
        this._event = event || "change";
        this._delay = 0;
        this._timeout = null;
        // transform is an object, so that output callbacks have a reference to it
        // i.e. outputs are transformed correctly even if transform
        // is given *after* output callbacks have been created
        // e.g. fw(...).$binding().output(...).transform(...)
        this._transform = {func: (x) => x};
        this._output = [];
        this._once = [];
        this._checked = [];
        this._unchecked = [];

        this._eventListener = this._listener.bind(this);
        this._input.addEventListener(this._event, this._eventListener);

        let bind = bindings.get(this._input);

        if(!bind) {
            bind = {id: Object(_misc__WEBPACK_IMPORTED_MODULE_1__["identifier"])(), instances: new Map()};
            bindings.set(this._input, bind);
        }

        this._id = bind.id();
        bind.instances.set(this._id, this);
    }

    _checkable(target) {
        target = target || this._input;
        return ["radio", "checkbox"].includes(target.type);
    }

    _handler(event) {
        const target = event.target;
        let value = target.value;

        if(this._checkable(target)) {
            value = target.checked;

            if(value)
                this._checked.forEach(check => check(target));
            else
                this._unchecked.forEach(uncheck => uncheck(target));
        }

        this._timeout = null;
        this._output.forEach(output => output(value, target));
        this._once.forEach(once => once(value, target));
    }

    _listener(event) {
        if(this._delay > 0) {
            if(this._timeout !== null)
                clearTimeout(this._timeout);

            this._timeout = setTimeout(this._handler.bind(this, event), this._delay);
        }
        else {
            this._handler(event);
        }
    }

    /**
     * Add a delay to the binding
     *
     * For example, this can be used to throttle a text input so that
     * the event listeners are not called (possibly) several times a second.
     *
     * @default 0
     * @param {Number} milliseconds
     * @throws {(TypeError|RangeError)}
     * @returns {Binding} `this`
     */
    delay(milliseconds) {
        this._delay = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].between(milliseconds, 0, _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].PlusInfinity);
        return this;
    }

    /**
     * Add output to binding
     *
     * @param {(Element|String|Object)} output Element/Selector/Object
     * @param {String} [property=textContent] Property to change, eg. "style.fontSize"
     * @param {Function} [transform] Custom transform for this output
     * @throws {(ElementError|TypeError)}
     * @returns {Binding} `this`
     */
    output(output, property, transform) {
        let obj = _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](output);
        let outputs = obj.length;
        const properties = !!property ? property.split('.') : [DEFAULT_PROP];
        const propsLength = properties.length;
        const prop = properties[propsLength - 1];
        transform = !!transform ? {func: transform} : this._transform;

        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(transform.func))
            throw new TypeError("Parameter wasn't a function");

        if(Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isString"])(output)) {
            const elements = document.querySelectorAll(output);

            if(elements.length === 0)
                throw new _errors__WEBPACK_IMPORTED_MODULE_2__["ElementError"](`No elements found for: ${output}`);

            obj = Array.from(elements);
            outputs = obj.length;
        }

        for(let i = 0; i < outputs; i++) {
            for(let j = 0; j < propsLength - 1; j++) {
                obj[i] = obj[i][properties[j]];
            }
        }

        const setter = function(value, target) {
            const [o, l, p, t] = this;
            const transformed = t.func(value, target);

            for(let i = 0; i < l; i++) {
                // CSS styles work in slightly weird way:
                // Property access through brackets wont work for custom properties (css variables)
                // Property access through setProperty wont work for camelCase properties
                // So use setProperty on all properties with dash
                if(o[i] instanceof CSSStyleDeclaration && p.includes('-'))
                    o[i].setProperty(p, transformed);
                else if(o[i] instanceof Element && Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isUndefined"])(o[i][p]))
                    o[i].setAttribute(p, transformed);
                else
                    o[i][p] = transformed;
            }
        };

        this._output.push(setter.bind([obj, outputs, prop, transform]));
        return this;
    }

    /**
     * Common transform function for all outputs
     *
     * @param {Function} transform `function(inputValue, eventTarget)`
     * @throws {TypeError}
     * @returns {Binding} `this`
     */
    transform(transform) {
        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(transform))
            throw new TypeError("Transform wasn't a function");

        this._transform.func = transform;
        return this;
    }

    /**
     * Run callback once per event (transforms are run per output)
     *
     * @param {Function} callback `function(inputValue, eventTarget)`
     * @throws {TypeError}
     * @returns {Binding} `this`
     */
    once(callback) {
        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(callback))
            throw new TypeError("Callback wasn't a function");

        this._once.push(callback);
        return this;
    }

    /**
     * Run callback when a checkable input (radio, checkbox) gets checked
     *
     * @param {Function} callback `function(eventTarget)`
     * @throws {(TypeError|Error)}
     * @returns {Binding} `this`
     */
    checked(callback) {
        if(!this._checkable())
            throw new Error("Input isn't a checkable (radio, checkbox)");

        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(callback))
            throw new TypeError("Callback wasn't a function");

        this._checked.push(callback);
        return this;
    }

    /**
     * Run callback when a checkable input (radio, checkbox) gets unchecked
     *
     * @param {Function} callback `function(eventTarget)`
     * @throws {(TypeError|Error)}
     * @returns {Binding} `this`
     */
    unchecked(callback) {
        if(!this._checkable())
            throw new Error("Input isn't a checkable (radio, checkbox)");

        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isFunction"])(callback))
            throw new TypeError("Callback wasn't a function");

        this._unchecked.push(callback);
        return this;
    }

    /**
     * Update input value
     *
     * Dispatches events to all the bindings associated with this input element.
     *
     * @param {(String|Number|Boolean)} [newValue] New value for input
     */
    update(newValue) {
        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isUndefined"])(newValue) && this._input.type !== "file") {
            if(this._checkable())
                this._input.checked = !!newValue;
            else
                this._input.value = newValue;
        }

        Binding._dispatch(this._input);
    }

    /**
     * Remove binding and the event listener
     *
     * This does *not* remove all bindings associated with this input element.
     */
    remove() {
        this._input.removeEventListener(this._event, this._eventListener);

        const bind = bindings.get(this._input);
        bind.instances.delete(this._id);

        if(bind.instances.size === 0)
            bindings.delete(this._input);
    }
}


/**
 * Bind input events to outputs; outputs being either Elements or Objects.
 *
 * @module bindings
 */
/* harmony default export */ __webpack_exports__["default"] = (Binding);


/***/ }),

/***/ "./src/colors.js":
/*!***********************!*\
  !*** ./src/colors.js ***!
  \***********************/
/*! exports provided: rgb, hsl, darken, lighten, saturate, text */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rgb", function() { return rgb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hsl", function() { return hsl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "darken", function() { return darken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lighten", function() { return lighten; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saturate", function() { return saturate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "text", function() { return text; });
/* harmony import */ var _named_colors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./named_colors */ "./src/named_colors.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./misc */ "./src/misc.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./errors */ "./src/errors.js");





/**
 * @private
 */
class RGBAColor {
    static from(color) {
        if(color instanceof RGBAColor)
            return new RGBAColor(color.r, color.g, color.b, color.a);
        else if(color instanceof HSLAColor)
            return RGBAColor.fromHSLA(color);

        throw new _errors__WEBPACK_IMPORTED_MODULE_2__["InvalidValueError"]("Unknown color type");
    }

    static fromHSLA(hslaColor) {
        const toByte = frac => {
            // Is it better to use floor(... * 256) than round(... * 255)?
            return Math.floor(frac * 256);
        };

        const [h, s, l] = [hslaColor.h, hslaColor.s, hslaColor.l];
        const hh = h / 60;
        const c = (1 - Math.abs(2 * l - 1)) * s;
        const x = c * (1 - Math.abs(hh % 2 - 1));
        const m = l - c / 2;
        let r, g, b, a = hslaColor.a;
        let rgb;

        if(hh <= 1)      rgb = [c, x, 0];
        else if(hh <= 2) rgb = [x, c, 0];
        else if(hh <= 3) rgb = [0, c, x];
        else if(hh <= 4) rgb = [0, x, c];
        else if(hh <= 5) rgb = [x, 0, c];
        else if(hh <= 6) rgb = [c, 0, x];

        [r, g, b] = rgb.map(x => toByte(x + m));

        return new RGBAColor(r, g, b, a);
    }

    /**
     * @param {Number} r [0-255]
     * @param {Number} g [0-255]
     * @param {Number} b [0-255]
     * @param {Number} a [0-1]
     */
    constructor(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isUndefined"])(a) ? 1 : a;
    }

    get r() { return this._r; }
    get g() { return this._g; }
    get b() { return this._b; }
    get a() { return this._a; }
    set r(v) { this._r = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(Math.round(v), 0, 255); }
    set g(v) { this._g = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(Math.round(v), 0, 255); }
    set b(v) { this._b = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(Math.round(v), 0, 255); }
    set a(v) { this._a = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(v, 0, 1); }

    toString() {
        const a = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].round(this._a, 2);

        // rgb and rgba are the same as of CSS Colors Level 4,
        // but that specification is still a working draft
        if(this._a === 1)
            return `rgb(${this._r}, ${this._g}, ${this._b})`;
        else
            return `rgba(${this._r}, ${this._g}, ${this._b}, ${a})`;
    }
}


/**
 * @private
 */
class HSLAColor {
    static from(color) {
        if(color instanceof HSLAColor)
            return new HSLAColor(color.h, color.s, color.l, color.a);
        else if(color instanceof RGBAColor)
            return HSLAColor.fromRGBA(color);

        throw new _errors__WEBPACK_IMPORTED_MODULE_2__["InvalidValueError"]("Unknown color type");
    }

    static fromRGBA(rgbaColor) {
        const [r, g, b] = [rgbaColor.r, rgbaColor.g, rgbaColor.b].map(x => x / 255);
        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);
        const c = max - min;
        let h, s, l = (max + min) / 2, a = rgbaColor.a;

        if(c === 0) {
            h = s = 0;
        }
        else {
            switch(max) {
                case r:
                    h = (g - b) / c % 6;
                    break;
                case g:
                    h = (b - r) / c + 2;
                    break;
                case b:
                    h = (r - g) / c + 4;
                    break;
            }

            h *= 60;
            s = l === 1 ? 0 : c / (1 - Math.abs(2 * l - 1));
        }

        return new HSLAColor(h, s, l, a);
    }

    /**
     * @param {Number} h [0-360]
     * @param {Number} s [0-1]
     * @param {Number} l [0-1]
     * @param {Number} a [0-1]
     */
    constructor(h, s, l, a) {
        this.h = h;
        this.s = s;
        this.l = l;
        this.a = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["isUndefined"])(a) ? 1 : a;
    }

    get h() { return this._h; }
    get s() { return this._s; }
    get l() { return this._l; }
    get a() { return this._a; }
    set h(v) { this._h = (v % 360) + (v < 0 ? 360 : 0); }
    set s(v) { this._s = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(v, 0, 1); }
    set l(v) { this._l = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(v, 0, 1); }
    set a(v) { this._a = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].clamp(v, 0, 1); }

    toString() {
        const h = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].round(this._h, 2);
        const s = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].round(this._s * 100, 2);
        const l = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].round(this._l * 100, 2);
        const a = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].round(this._a, 2);

        // hsl and hsla are the same as of CSS Colors Level 4,
        // but that specification is still a working draft
        if(this._a === 1)
            return `hsl(${h}deg, ${s}%, ${l}%)`;
        else
            return `hsla(${h}deg, ${s}%, ${l}%, ${a})`;
    }
};


/**
 * @private
 */
class CSSColorNumber {
    constructor(str) {
        this._value = NaN;
        this._isPercentage = false;
        this._parse(str);
    }

    _parse(str) {
        const [parsed, unit] = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].parseWithUnits(str, ['%']);
        let number = parsed;

        if(unit === '%') {
            number /= 100;

            if(number < 0 || number > 1)
                throw new RangeError(`Percentage out of bounds [0-100]: ${str}`);

            this._isPercentage = true;
        }

        if(number < 0 || number > 255)
            throw new RangeError(`Number out of bounds [0-255]: ${str}`);

        this._value = number;
    }

    get value() {
        return this._value;
    }

    get isPercentage() {
        return this._isPercentage;
    }
}


/**
 * @private
 */
class CSSColorAngle {
    constructor(str) {
        this._value = NaN;
        this._parse(str);
    }

    _parse(str) {
        const [parsed, unit] = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].parseWithUnits(str, ["deg", "rad", "grad", "turn"]);
        let degrees = parsed;

        switch(unit) {
            case "rad":
                degrees *= 180 / Math.PI;
                break;
            case "grad":
                degrees *= 0.9;
                break;
            case "turn":
                degrees *= 360;
                break;
        }

        degrees = degrees % 360 + (degrees < 0 ? 360 : 0);
        this._value = degrees;
    }

    get degrees() {
        return this._value;
    }
}


/**
 * @private
 */
class CSSColorParser {
    static parseHex(color) {
        const bytes = color.substring(1);
        const length = bytes.length;
        const rgba = [NaN, NaN, NaN, 255];
        let colorCount = 3, colorLength = 1, hex = null;

        switch(length) {
            case 3: // #rgb
                break;
            case 4: // #rgba
                colorCount = 4;
                break;
            case 6: // #rrggbb
                colorLength = 2;
                break;
            case 8: // #rrggbbaa
                colorCount = 4;
                colorLength = 2;
                break;
            default:
                throw new Error("Wrong amount of bytes");
        }

        for(let i = 0; i < colorCount; i++) {
            hex = bytes.substring(colorLength * i, colorLength * (i + 1));
            // If color was given in short form (#rgb(a)), repeat hex
            rgba[i] = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].parseHexadecimal(hex.repeat(3 - colorLength));
        }

        return new RGBAColor(rgba[0], rgba[1], rgba[2], rgba[3] / 255);
    }

    static parseRGBA(color) {
        const parser = new CSSColorParser(["rgb", "rgba"], [NaN, NaN, NaN, 1], ["number", "number", "number", "alpha"]);
        const result = parser.parseString(color);
        return new RGBAColor(result[0], result[1], result[2], result[3]);
    }

    static parseHSLA(color) {
        const parser = new CSSColorParser(["hsl", "hsla"], [NaN, NaN, NaN, 1], ["angle", "percent", "percent", "alpha"]);
        const result = parser.parseString(color);
        return new HSLAColor(result[0], result[1], result[2], result[3]);
    }

    static parse(str, colorClass) {
        const fixed = str.trim().toLowerCase();
        let color = null;

        try {
            if(_named_colors__WEBPACK_IMPORTED_MODULE_0__["default"].hasOwnProperty(fixed))
                color = CSSColorParser.parseHex(_named_colors__WEBPACK_IMPORTED_MODULE_0__["default"][fixed]);
            else if(fixed.startsWith('#'))
                color = CSSColorParser.parseHex(fixed);
            else if(fixed.startsWith("rgb"))
                color = CSSColorParser.parseRGBA(fixed);
            else if(fixed.startsWith("hsl"))
                color = CSSColorParser.parseHSLA(fixed);
        }
        catch(e) {
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["InvalidValueError"](`Invalid color '${str}': ${e.message}`);
        }

        if(!!color)
            return colorClass.from(color);

        throw new _errors__WEBPACK_IMPORTED_MODULE_2__["InvalidValueError"](`Unknown color type: ${str}`);
    }

    constructor(types, defaults, parts) {
        this._types = types;
        this._defaults = defaults;
        this._parts = parts;
    }

    alpha(str) {
        const number = new CSSColorNumber(str);

        if(!number.isPercentage && number.value > 1)
            throw new RangeError(`Alpha out of range [0-1]: ${str}`);

        return number.value;
    }

    angle(str) {
        const angle = new CSSColorAngle(str);
        return angle.degrees;
    }

    number(str) {
        const number = new CSSColorNumber(str);
        return number.value * (number.isPercentage ? 255 : 1);
    }

    percent(str) {
        const number = new CSSColorNumber(str);

        if(!number.isPercentage)
            throw new TypeError(`Number wasn't a percentage: ${str}`);

        return number.value;
    }

    parseString(str) {
        // This is not actually correct as it accepts
        // colors that a browser would not, e.g.:
        // rgb(r / g / b / a)
        // hsl(h, s / l a)
        const separators = [' ', ',', '/'];
        const length = str.length;
        const count = this._parts.length;
        const result = Array.from(this._defaults);
        let c = 0, l = '', acc = [], end = false;

        const parseAccumulated = () => {
            if(c >= count)
                throw new Error("Too many values");

            if(acc.length > 0) {
                result[c] = this[this._parts[c]].call(this, acc.join(''));
                acc = [];
                c++;
            }
        };

        for(let i = 0; i < length; i++) {
            l = str[i];

            if(l === '(') {
                const type = acc.join('');
                acc = [];

                if(!this._types.includes(type))
                    throw new Error(`Unknown type: ${type}`);
            }
            else if(l === ')') {
                end = true;
                parseAccumulated();
                break;
            }
            else if(separators.includes(l)) {
                parseAccumulated();
            }
            else {
                acc.push(l);
            }
        }

        if(!end)
            throw new Error("Parsing stopped before reaching end");

        if(result.some(x => Number.isNaN(x)))
            throw new Error("Missing values");

        return result;
    }
}


/**
 * Transform the color in to `rgb(...)`/`rgba(...)` form
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.rgb
 * @param {String} color
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function rgb(color) {
    return CSSColorParser.parse(color, RGBAColor).toString();
}


/**
 * Transform the color in to `hsl(...)`/`hsla(...)` form
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.hsl
 * @param {String} color
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function hsl(color) {
    return CSSColorParser.parse(color, HSLAColor).toString();
}


/**
 * Darken the color by some amount
 *
 * `amount` should be in range [0-1]
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.darken
 * @param {String} color
 * @param {Number} amount
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function darken(color, amount) {
    const rgba = CSSColorParser.parse(color, RGBAColor);
    const factor = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].between(amount, 0, 1);

    rgba.r -= rgba.r * factor;
    rgba.g -= rgba.g * factor;
    rgba.b -= rgba.b * factor;

    return rgba.toString();
}


/**
 * Lighten the color by some amount
 *
 * `amount` should be in range [0-1]
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.lighten
 * @param {String} color
 * @param {Number} amount
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function lighten(color, amount) {
    const rgba = CSSColorParser.parse(color, RGBAColor);
    const factor = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].between(amount, 0, 1);

    rgba.r += (255 - rgba.r) * factor;
    rgba.g += (255 - rgba.g) * factor;
    rgba.b += (255 - rgba.b) * factor;

    return rgba.toString();
}


/**
 * Saturate the color by some amount
 *
 * `amount` should be in range [0-1]
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.saturate
 * @param {String} color
 * @param {Number} amount
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function saturate(color, amount) {
    const hsla = CSSColorParser.parse(color, HSLAColor);
    const factor = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].between(amount, 0, 1);

    if(hsla.s > 0) // Saturating gray doesn't make a lot of sense
        hsla.s += (1 - hsla.s) * factor;

    return RGBAColor.fromHSLA(hsla).toString();
}


/**
 * Get a readable text color for the specified background color
 *
 * `backgroundColor` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.text
 * @param {String} backgroundColor
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function text(backgroundColor) {
    const rgba = CSSColorParser.parse(backgroundColor, RGBAColor);
    // Rec. 709
    const luma = 0.2126 * rgba.r + 0.7152 * rgba.g + 0.0722 * rgba.b;
    // 115 seemed to be slightly better than the more logical 127 (= max_luma / 2)
    const c = luma > 115 ? 0 : 255;
    return (new RGBAColor(c, c, c)).toString();
}


/**
 * Helper functions for modifying colors
 *
 * @module colors
 * @typicalname fw.color
 */



/***/ }),

/***/ "./src/css.js":
/*!********************!*\
  !*** ./src/css.js ***!
  \********************/
/*! exports provided: computedStyle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "computedStyle", function() { return computedStyle; });
/* harmony import */ var _dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom */ "./src/dom.js");
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./misc */ "./src/misc.js");





const cache = {};


function cloneStyle(computed) {
    const values = {};

    // Firefox doesn't support Object.keys on CSSStyleDeclaration,
    // and only returns the property name indexes
    _arrays__WEBPACK_IMPORTED_MODULE_1__["createArray"](computed.length).forEach(i => {
        const p = computed[i], v = computed[p];

        if(p.indexOf('-') > 0) {
            const prop = p.replace(/\-([a-z])/g, (_, l) => l.toUpperCase());
            values[prop] = v;
        }

        values[p] = v;
    });

    return values;
}


function computeElement(element) {
    if(element.isConnected) return window.getComputedStyle(element);

    const container = _dom__WEBPACK_IMPORTED_MODULE_0__["element"]("div");
    const cloned = element.cloneNode(false);

    container.style.display = "none";
    container.appendChild(cloned);
    document.body.appendChild(container);
    // The style must be copied as it is a 'live' reference to the style
    // i.e. it will disappear (at least in Chromium) when the element is removed
    const computed = cloneStyle(window.getComputedStyle(cloned));
    document.body.removeChild(container);

    return computed;
}


/**
 * @private
 * @param {(Element|String)} element
 * @returns {(Object|CSSStyleDeclaration)}
 */
function computedStyle(element) {
    if(element instanceof Element)
        return computeElement(Object(_misc__WEBPACK_IMPORTED_MODULE_2__["realElement"])(element));

    element = element.toLowerCase();

    if(!cache[element])
        cache[element] = computeElement(_dom__WEBPACK_IMPORTED_MODULE_0__["element"](element));

    return cache[element];
}


/**
 * @module css
 * @private
 */



/***/ }),

/***/ "./src/data.js":
/*!*********************!*\
  !*** ./src/data.js ***!
  \*********************/
/*! exports provided: has, get, set, remove, purge */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "has", function() { return has; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get", function() { return get; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "set", function() { return set; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "remove", function() { return remove; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "purge", function() { return purge; });
const data = new Map();


function has(element, key) {
    const obj = data.get(element);
    return !!obj && obj.has(key);
}

function get(element, key) {
    const obj = data.get(element);
    return !!obj ? obj.get(key) : undefined;
}

function set(element, key, value) {
    const obj = data.get(element);

    if(!!obj)
        obj.set(key, value);
    else
        data.set(element, new Map([[key, value]]));
}

function remove(element, key) {
    const obj = data.get(element);

    if(!!obj) {
        obj.delete(key);

        if(obj.size === 0)
            data.delete(element);
    }
}

function purge(element, ) {
    data.delete(element);
}


/**
 * @module data
 * @private
 */



/***/ }),

/***/ "./src/dom.js":
/*!********************!*\
  !*** ./src/dom.js ***!
  \********************/
/*! exports provided: element, text */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "element", function() { return element; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "text", function() { return text; });
function element(tagName, attrs) {
    const newElement = document.createElement(tagName);

    if(!!attrs) {
        Object.entries(attrs).forEach(([key, value]) => {
            if(!key || !value) return;

            if(Array.isArray(value))
                value = value.join(' ');

            if(key.toLowerCase() === "class")
                String(value).split(' ').forEach((cls) => newElement.classList.add(cls));
            else
                newElement.setAttribute(key, value);
        });
    }

    return newElement;
}

function text(textContent) {
    return document.createTextNode(textContent);
}


/**
 * @module dom
 * @private
 */



/***/ }),

/***/ "./src/errors.js":
/*!***********************!*\
  !*** ./src/errors.js ***!
  \***********************/
/*! exports provided: ElementError, TemplateDefinedError, ModelHasNoTemplateError, EmptyTemplateError, InvalidValueError, RequestSentError, OperationNotSupportedError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElementError", function() { return ElementError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemplateDefinedError", function() { return TemplateDefinedError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelHasNoTemplateError", function() { return ModelHasNoTemplateError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmptyTemplateError", function() { return EmptyTemplateError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvalidValueError", function() { return InvalidValueError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestSentError", function() { return RequestSentError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OperationNotSupportedError", function() { return OperationNotSupportedError; });
class FrameworkError extends Error {
    constructor(...args) {
        super(...args);
    }
}


/**
 * Error thrown when element is invalid
 *
 * @alias module:errors.ElementError
 */
class ElementError extends FrameworkError {}

/**
 * Error thrown when trying to define a template twice
 *
 * @alias module:errors.TemplateDefinedError
 */
class TemplateDefinedError extends FrameworkError {}

/**
 * Error thrown when appending data to model that has no template
 *
 * @alias module:errors.ModelHasNoTemplateError
 */
class ModelHasNoTemplateError extends FrameworkError {}

/**
 * Error thrown when template is empty
 *
 * @alias module:errors.EmptyTemplateError
 */
class EmptyTemplateError extends FrameworkError {}

/**
 * Error thrown when value is invalid
 *
 * @alias module:errors.InvalidValueError
 */
class InvalidValueError extends FrameworkError {}

/**
 * Error thrown when trying to send the same request twice
 *
 * @alias module:errors.RequestSentError
 */
class RequestSentError extends FrameworkError {}

/**
 * Error thrown when trying to call a framework proxy method
 * that doesn't operate on arrays with an array
 *
 * @alias module:errors.OperationNotSupportedError
 */
class OperationNotSupportedError extends FrameworkError {}


/**
 * framework.js errors
 *
 * @module errors
 * @typicalname fw.error
 */



/***/ }),

/***/ "./src/fw.js":
/*!*******************!*\
  !*** ./src/fw.js ***!
  \*******************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom */ "./src/dom.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./errors */ "./src/errors.js");
/* harmony import */ var _colors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./colors */ "./src/colors.js");
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ui */ "./src/ui.js");
/* harmony import */ var _bindings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./bindings */ "./src/bindings.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./models */ "./src/models.js");
/* harmony import */ var _messages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./messages */ "./src/messages.js");
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./transitions */ "./src/transitions.js");
/* harmony import */ var _requests__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./requests */ "./src/requests.js");
/* harmony import */ var _proxy__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./proxy */ "./src/proxy.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./misc */ "./src/misc.js");













/**
 * The access point for all the framework.js functionality
 *
 * @example <caption>fw is a global function</caption>
 * fw.load(function() {  // Run something on page load
 *     const fade = new fw.Transition();
 *     fade.opacity().duration(1000);
 *
 *     fw("#my-element").$hide(fade);
 *
 *     const model = fw("#model-container").$model();
 *
 *     try {
 *         model.template('');
 *     }
 *     catch(e) {
 *         e instanceof fw.error.EmptyTemplateError === true;
 *     }
 * });
 *
 * @param {(String|Element)} selector
 * @throws {TypeError}
 * @returns {(Element|Element[])}
 * @function
 * @global
 */
const fw = (function(selector) {
    if(selector instanceof Element) {
        if(selector.$__frameworkProxy)
            return selector;
        else
            return Object(_proxy__WEBPACK_IMPORTED_MODULE_9__["createFrameworkProxy"])(selector);
    }
    else if(!Object(_misc__WEBPACK_IMPORTED_MODULE_10__["isString"])(selector)) {
        throw new TypeError("Paramater wasn't a string nor an Element");
    }

    const query = document.querySelectorAll(selector);

    if(query.length === 0)
        return undefined;
    else if(query.length > 1)
        return Object(_proxy__WEBPACK_IMPORTED_MODULE_9__["createFrameworkProxy"])(Array.from(query));
    else
        return Object(_proxy__WEBPACK_IMPORTED_MODULE_9__["createFrameworkProxy"])(query[0]);
}).bind({});


/**
 * Initialize framework.js
 *
 * Usually run once on page load.
 *
 * @alias fw.init
 */
fw.init = () => {
    _bindings__WEBPACK_IMPORTED_MODULE_4__["default"]._initialize();
};


/**
 * Run a function when page and all it's resources have loaded
 *
 * For most cases `fw.ready` is a better choice.
 *
 * Can be called several times, functions will be called
 * (mostly) in the order they were given.
 *
 * @alias fw.load
 * @param {Function} func
 * @throws {TypeError}
 */
fw.load = func => {
    if(!Object(_misc__WEBPACK_IMPORTED_MODULE_10__["isFunction"])(func))
        throw new TypeError("Parameter wasn't a function");

    // The load event *might* have not fired when readyState is complete
    // If this is called when readyState has changed, but load has not fired
    // func may be called before load
    // Unlikely(?) to cause anything serious
    if(document.readyState !== "complete")
        window.addEventListener("load", func);
    else
        func();
};


/**
 * Run a function when document parsing is finished
 *
 * This is faster than using `fw.load`.
 *
 * Can be called several times, functions will be called
 * (mostly) in the order they were given.
 *
 * @alias fw.ready
 * @param {Function} func
 * @throws {TypeError}
 */
fw.ready = func => {
    if(!Object(_misc__WEBPACK_IMPORTED_MODULE_10__["isFunction"])(func))
        throw new TypeError("Parameter wasn't a function");

    if(document.readyState === "loading")
        document.addEventListener("DOMContentLoaded", func);
    else
        func();
};


/**
 * framework.js errors
 *
 * @alias fw.error
 * @see {@link module:errors}
 */
fw.error = _errors__WEBPACK_IMPORTED_MODULE_1__;


/**
 * Helper functions for dealing with colors
 *
 * @example
 * fw.color.darken("#ffffff", 0.5) === "rgb(128, 128, 128)";
 * fw.color.darken("rgb(255, 255, 255)", 0.5) === "rgb(128, 128, 128)";
 *
 * // Get a readable text color for a background
 * fw.color.text("#ffeedd") === "rgb(0, 0, 0)";
 * fw.color.text("#402040") === "rgb(255, 255, 255)";
 *
 * @alias fw.color
 * @see {@link module:colors}
 */
fw.color = _colors__WEBPACK_IMPORTED_MODULE_2__;


/**
 * User interface classes
 *
 * @alias fw.ui
 * @see {@link module:ui}
 */
fw.ui = _ui__WEBPACK_IMPORTED_MODULE_3__;


/**
 * The Binding class
 *
 * @alias fw.Binding
 * @see {@link module:bindings}
 */
fw.Binding = _bindings__WEBPACK_IMPORTED_MODULE_4__["default"];


/**
 * The Model class
 *
 * @alias fw.Model
 * @see {@link module:models}
 */
fw.Model = _models__WEBPACK_IMPORTED_MODULE_5__["default"];


/**
 * The Message class
 *
 * @alias fw.Message
 * @see {@link module:messages}
 */
fw.Message = _messages__WEBPACK_IMPORTED_MODULE_6__["default"];


/**
 * The TransitionChain class
 *
 * @example
 * const chain = new fw.TransitionChain();
 * const fade = (new fw.Transition()).opacity().duration(100);
 * const scale = (new fw.Transition()).scale().duration(200);
 *
 * chain.append("#parent", fade).append("#child", scale);
 *
 * fw.Transition.hide("#parent");
 * fw.Transition.hide("#child");
 *
 * // This will result in #parent element fading in
 * // and after that the #child element scaling in.
 * chain.show().then(() => {
 *     // if the alternate option was set (chain.alternate()),
 *     // this will run the chain in reverse
 *     chain.hide();
 * });
 *
 * @alias fw.TransitionChain
 * @see {@link module:transitions.TransitionChain}
 */
fw.TransitionChain = _transitions__WEBPACK_IMPORTED_MODULE_7__["TransitionChain"];


/**
 * The Transition class
 *
 * @example
 * fw.Transition.toggle(element);  // Synchronous toggling of element
 *
 * const transition = new fw.Transition();
 * transition.opacity().duration(500);
 *
 * transition.toggle(element);  // Async toggle that fades element in/out
 *                              // Returns a Promise
 *
 * @alias fw.Transition
 * @see {@link module:transitions.Transition}
 */
fw.Transition = _transitions__WEBPACK_IMPORTED_MODULE_7__["Transition"];


/**
 * The Request class
 *
 * @example
 * const request = new fw.Request("http://example.com");
 * request.type("document").method("POST");
 *
 * fw.Requester.send(request).then(({body, response}) => { ... });
 *
 * @alias fw.Request
 * @see {@link module:requests.Request}
 */
fw.Request = _requests__WEBPACK_IMPORTED_MODULE_8__["Request"];


/**
 * The Requester class
 *
 * @example
 * const requester = new fw.Requester();
 * const request = new fw.Request("http://example.com/page2");
 *
 * requester.parallel(10); // Default is 1
 *
 * // A convenience method for creating and appending a simple request
 * requester.request("http://example.com").then(({body, response}) => { ... });
 *
 * // Append a request
 * requester.append(request).then(({body, response}) => { ... });
 *
 * @example
 * // Requester has a static method to simply send requests, without dealing with queues
 * fw.Requester.send(someRequest);
 *
 * @alias fw.Requester
 * @see {@link module:requests.Requester}
 */
fw.Requester = _requests__WEBPACK_IMPORTED_MODULE_8__["Requester"]


/**
 * Create a new element of type tagName, with optional attributes
 *
 * @alias fw.element
 * @param {String} tagName
 * @param {Object} [attrs] Attributes to set
 * @returns {Element}
 */
fw.element = (tagName, attrs) => _dom__WEBPACK_IMPORTED_MODULE_0__["element"](tagName, attrs);


/**
 * Create a new text node
 *
 * @alias fw.text
 * @param {String} textContent
 * @returns {Node}
 */
fw.text = (textContent) => _dom__WEBPACK_IMPORTED_MODULE_0__["text"](textContent);


/**
 * @module fw
 * @private
 */
/* harmony default export */ __webpack_exports__["default"] = (fw);


/***/ }),

/***/ "./src/messages.js":
/*!*************************!*\
  !*** ./src/messages.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom */ "./src/dom.js");
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transitions */ "./src/transitions.js");
/* harmony import */ var _proxy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./proxy */ "./src/proxy.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./misc */ "./src/misc.js");







const ELEMENT_TYPE = "p";
const DETACHED_TYPE = "aside";
const NORMAL_CLASS = "fw-message-normal";
const URGENT_CLASS = "fw-message-urgent";
const DETACHED_CLASS = "fw-message-detached";


/**
 * Class for showing messages
 *
 * @alias module:messages
 */
class Message {
    /**
     * Create a new Message instance
     *
     * @param {(Element|String)} parentElement Element/Selector
     * @throws {(ElementInUseError|ElementError)}
     */
    constructor(parentElement) {
        this._element = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["realElement"])(parentElement);
        this._timeout = 4000;
        this._overwrite = true;
        this._hide = true;
        this._messages = new Map();
        this._id = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["identifier"])();
        this._lastNormal = null;
        this._transition = _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"];
    }

    _createMessage(content, type, detached, id) {
        const msg = {
            element: this._createElement(type === "urgent", detached),
            id: id || null, timeout: null, messageID: this._id(), type, detached
        };

        msg.element.textContent = content;

        return msg;
    }

    _createElement(urgent, detached) {
        const cls = urgent ? detached ? [URGENT_CLASS, DETACHED_CLASS] : URGENT_CLASS : NORMAL_CLASS;
        const elem = urgent && detached ? DETACHED_TYPE : ELEMENT_TYPE;
        return _dom__WEBPACK_IMPORTED_MODULE_0__["element"](elem, {class: cls});
    }

    _setMessageTimeout(msg) {
        const messageTimeout = () => {
            if(this._lastNormal === msg.messageID)
                this._lastNormal = null;

            this._removeMessage(msg);
        };

        if(msg.timeout !== null)
            clearTimeout(msg.timeout);

        msg.timeout = setTimeout(messageTimeout, this._timeout);
    }

    _setMessageClickHandler(msg) {
        msg.element.addEventListener("click", this._removeMessage.bind(this, msg));
    }

    async _removeMessage(msg) {
        const parent = msg.detached ? document.body : this._element;

        this._messages.delete(msg.messageID);

        if(this._hide && !msg.detached && this._messages.size === 0)
            await this._transition.hide(this._element);
        else if(msg.detached)
            await this._transition.hide(msg.element);

        parent.removeChild(msg.element);
    }

    /**
     * How long to show normal messages for
     *
     * @default 4000
     * @param {Number} milliseconds
     * @throws {(TypeError|RangeError)}
     * @returns {Message} `this`
     */
    timeout(milliseconds) {
        this._timeout = _misc__WEBPACK_IMPORTED_MODULE_4__["Numbers"].between(milliseconds, 0, _misc__WEBPACK_IMPORTED_MODULE_4__["Numbers"].PlusInfinity);
        return this;
    }

    /**
     * Overwrite messages instead of appending them to the container
     *
     * @default true
     * @param {Boolean} [bool=true]
     * @returns {Message} `this`
     */
    overwrite(bool) {
        this._overwrite = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["boolDefault"])(bool, true);
        return this;
    }

    /**
     * Hide container when there are no messages
     *
     * @default true
     * @param {Boolean} [bool=true]
     * @returns {Message} `this`
     */
    hide(bool) {
        this._hide = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["boolDefault"])(bool, true);
        return this;
    }

    /**
     * Set the transition for message elements
     *
     * This is transition is used to show/hide the elements and the container.
     *
     * @param {Transition} trans
     * @return {Message} `this`
     */
    transition(trans) {
        if(!_transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"]._valid(trans))
            throw new TypeError("Parameter was not a Transition");

        this._transition = trans;
        return this;
    }

    /**
     * Show a normal message that goes away after a specified timeout
     *
     * Element appended to the message container:
     * ```html
     * <p class="fw-message-normal">content</p>
     * ```
     *
     * @param {String} content
     */
    normal(content) {
        if(this._overwrite && this._lastNormal !== null) {
            const msg = this._messages.get(this._lastNormal);
            msg.element.textContent = content;
            return this._setMessageTimeout(msg);
        }

        const msg = this._createMessage(content, "normal", false);
        this._lastNormal = msg.messageID;
        this._messages.set(msg.messageID, msg);

        this._element.appendChild(msg.element);
        this._transition.show(this._element);

        this._setMessageTimeout(msg);
    }

    /**
     * Show an urgent message that has to be clicked away
     *
     * Urgent messages are not affected by timeout or overwrite.
     *
     * The id parameter can be used to specify an identifier for this message.
     * If a message with the same id is already shown, the new message will be discarded.
     * This can be used to disable duplicate messages, so that the user
     * doesn't see several identical messages.
     *
     * detach == false; element appended to the message container:
     * ```html
     * <p class="fw-message-urgent">content</p>
     * ```
     *
     * detach == true; element appended to the document body:
     * ```html
     * <aside class="fw-message-urgent fw-message-detached">content</aside>
     * ```
     *
     * @param {String} content
     * @param {String} [id]
     * @param {Boolean} [detach=false]
     */
    urgent(content, id, detach) {
        const haveID = !Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isNone"])(id);
        const matcher = msg => msg.id === id;

        if(haveID && _arrays__WEBPACK_IMPORTED_MODULE_1__["some"](this._messages.values(), matcher))
            return;

        const msg = this._createMessage(content, "urgent", !!detach, id);
        this._messages.set(msg.messageID, msg);

        if(!msg.detached) {
            const reference = this._element.firstElementChild || null;
            this._element.insertBefore(msg.element, reference);
            this._transition.show(this._element);
        }
        else {
            _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"].hide(msg.element);
            document.body.appendChild(msg.element);
            this._transition.show(msg.element);
        }

        this._setMessageClickHandler(msg);
    }

    /**
     * Remove messages and destroy this message container
     */
    remove() {
        _arrays__WEBPACK_IMPORTED_MODULE_1__["forEach"](this._messages.values(), msg => {
            if(msg.type === "normal") {
                this._element.removeChild(msg.element);
                clearTimeout(msg.timeout);
            }
            else {
                document.body.removeChild(msg.element);
            }
        });

        this._messages = new Map();
        this._lastNormal = null;

        Object(_proxy__WEBPACK_IMPORTED_MODULE_3__["invalidateCache"])(this._element, "message");
    }
}


/**
 * Show messages, either timed or clickable
 *
 * @module messages
 */
/* harmony default export */ __webpack_exports__["default"] = (Message);


/***/ }),

/***/ "./src/misc.js":
/*!*********************!*\
  !*** ./src/misc.js ***!
  \*********************/
/*! exports provided: Numbers, isString, isFunction, isUndefined, isNone, identifier, realElement, boolDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Numbers", function() { return Numbers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return isString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isFunction", function() { return isFunction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isUndefined", function() { return isUndefined; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNone", function() { return isNone; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "identifier", function() { return identifier; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "realElement", function() { return realElement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "boolDefault", function() { return boolDefault; });
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./errors */ "./src/errors.js");



const Numbers = new class {
    _normalizeNumber(str) {
        return String(str).toLowerCase().replace(/\s/g, '');
    }

    get PlusInfinity() {
        return Number.POSITIVE_INFINITY;
    }

    get MinusInfinity() {
        return Number.NEGATIVE_INFINITY;
    }

    parse(value) {
        const parsed = Number.parseFloat(value);

        if(Number.isNaN(parsed))
            throw new TypeError(`Not a number: ${value}`);

        return parsed;
    }

    strictParse(value) {
        const str = this._normalizeNumber(value);

        if(/^(\-|\+)?\d*\.?\d+(e(\-|\+)?\d+)?$/.test(str))
            return this.parse(str);

        throw new TypeError(`Not a number: ${value}`);
    }

    parseHexadecimal(value) {
        const str = this._normalizeNumber(value);

        if(/^(0x)?[0-9a-f]+$/.test(str))
            return Number.parseInt(str, 16);

        throw new TypeError(`Not a hexadecimal number: ${value}`);
    }

    parseWithUnits(value, allowedUnits) {
        allowedUnits = allowedUnits || [];
        let str = this._normalizeNumber(value);
        const unit = allowedUnits.filter(x => str.endsWith(x))
                                 .reduce((p, c) => c.length > p.length ? c : p, '');

        if(!!unit)
            str = str.substring(0, str.length - unit.length);

        return [this.strictParse(str), unit];
    }

    between(value, min, max) {
        const parsed = this.parse(value);

        if(value > max || value < min)
            throw new RangeError(`Value out of bounds [${min}-${max}]: ${parsed}`);

        return parsed;
    }

    round(value, decimals) {
        const factor = Math.pow(10, decimals || 0);
        return Math.round(this.parse(value) * factor) / factor;
    }

    clamp(value, min, max) {
        return Math.max(min || 0, Math.min(this.parse(value), max || 1));
    }
};


function isString(value) {
    return typeof value === "string";
}


function isFunction(value) {
    return typeof value === "function";
}


function isUndefined(value) {
    return typeof value === "undefined";
}


function isNone(value) {
    return isUndefined(value) || value === null;
}


function identifier() {
    // Unlikely that anyone would need more than 9 007 199 254 740 991 ids
    const generator = (function*() {
        let id = 0;
        for(;;) yield id++;
    })();

    return () => generator.next().value;
}


function realElement(element) {
    let real = null;

    if(element instanceof Element) {
        if(element.$__frameworkProxy)
            real = element.$raw();
        else
            real = element;
    }
    else if(isString(element) && element.length > 0) {
        real = document.querySelector(element);
    }

    if(!real) throw new _errors__WEBPACK_IMPORTED_MODULE_0__["ElementError"](`Invalid element: ${element}`);

    return real;
}


function boolDefault(bool, def) {
    return !isNone(bool) ? !!bool : def;
}


/**
 * @module common
 * @private
 */



/***/ }),

/***/ "./src/models.js":
/*!***********************!*\
  !*** ./src/models.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom */ "./src/dom.js");
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transitions */ "./src/transitions.js");
/* harmony import */ var _proxy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./proxy */ "./src/proxy.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./misc */ "./src/misc.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./errors */ "./src/errors.js");








/**
 * @private
 */
class Template {
    constructor() {
        this._template = null;
        this._model = {};
        this._original = [];
    }

    get _hasTemplate() {
        return this._template !== null;
    }

    get rootNodesLength() {
        if(!this._hasTemplate)
            throw new _errors__WEBPACK_IMPORTED_MODULE_5__["ModelHasNoTemplateError"]("Template not defined");

        return this._template.content.children.length;
    }

    fragment(datum) {
        if(!this._hasTemplate)
            throw new _errors__WEBPACK_IMPORTED_MODULE_5__["ModelHasNoTemplateError"]("Template not defined");

        Object.entries(datum).forEach(([key, value]) => {
            if(this._model.hasOwnProperty(key))
                this._model[key].forEach(f => f(value));
        });

        const imported = document.importNode(this._template.content, true);
        this._original.forEach(f => f());
        return imported;
    }

    setTemplate(tmpl) {
        if(this._template instanceof HTMLTemplateElement)
            throw new _errors__WEBPACK_IMPORTED_MODULE_5__["TemplateDefinedError"]("Template already defined");

        const createSetters = (id, obj, prop) => {
            const setter = ((i, o, p, value) => {
                o[p] = String(o[p]).replace(new RegExp(`{${i}}`, 'g'), value);
            }).bind(undefined, id, obj, prop);

            const originalSetter = ((o, p, v) => {
                o[p] = v;
            }).bind(undefined, obj, prop, obj[prop]);

            this._original.push(originalSetter);

            if(this._model.hasOwnProperty(id))
                this._model[id].push(setter);
            else
                this._model[id] = [setter];
        };

        const getIDMatches = value => {
            const regex = /{([^{} ]+)}/g;
            const matches = [];
            let match = null;

            while((match = regex.exec(value)) !== null)
                matches.push(match[1]);

            return matches;
        };

        const setupIdentifiers = element => {
            const attributes = element.attributes;
            const textNodes = _arrays__WEBPACK_IMPORTED_MODULE_1__["filter"](element.childNodes, c => c.nodeName === "#text");

            _arrays__WEBPACK_IMPORTED_MODULE_1__["concat"](attributes, textNodes).forEach(node => {
                getIDMatches(node.nodeValue).forEach(id => {
                    createSetters(id, node, "nodeValue");
                });
            });
        };

        if(tmpl instanceof HTMLTemplateElement) {
            this._template = tmpl.cloneNode(true);
        }
        else {
            this._template = _dom__WEBPACK_IMPORTED_MODULE_0__["element"]("template");
            this._template.innerHTML = tmpl;
        }

        const elements = [...this._template.content.children];
        let current = undefined;

        if(elements.length === 0)
            throw new _errors__WEBPACK_IMPORTED_MODULE_5__["EmptyTemplateError"]("Empty template");

        try {
            while(!Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isUndefined"])(current = elements.shift())) {
                setupIdentifiers(current);

                if(current.children.length > 0)
                    elements.unshift(...current.children);
            }
        }
        catch(err) {
            this._template = null;
            this._model = {};
            throw err;
        }
    }
}


/**
 * Class for creating view models
 *
 * @alias module:models
 */
class Model {
    /**
     * Create a new Model instance
     *
     * @param {(Element|String)} parentElement Element/Selector
     * @throws {(ElementInUseError|ElementError)}
     */
    constructor(parentElement) {
        this._element = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["realElement"])(parentElement);
        this._data = [];
        this._lazy = false;
        this._chunk = 1;
        this._dataIndex = 0;
        this._visibleDOM = 0;
        this._filterMatch = undefined;
        this._filterCallback = undefined;
        this._transition = _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"];
        this._template = new Template();
        this._elementDataMap = new Map();
        this._visible = () => undefined;
        this._observer = this._setupObserver();
    }

    /**
     * @private
     */
    get _visibleElementsLength() {
        return this._template.rootNodesLength * this._dataIndex;
    }

    /**
     * @private
     */
    get _actualElementsLength() {
        return this._element.children.length;
    }

    /**
     * @private
     */
    _setupObserver() {
        const options = {/*root: this._element, */rootMargin: "100px"/*, threshold: 0*/};

        return new IntersectionObserver(entries => {
            entries.filter(entry => entry.isIntersecting).forEach(entry => {
                this._observer.unobserve(entry.target);
                this._visible(this._elementDataMap.get(entry.target), entry.target);
                this._elementDataMap.delete(entry.target);
                this._visibleDOM++;

                if(this._lazy) this._lazyDOM();
            });
        }, options);
    }

    /**
     * @private
     */
    _getIndexes(index) {
        const roots = this._template.rootNodesLength;
        return _arrays__WEBPACK_IMPORTED_MODULE_1__["createArray"](roots).map(i => i + index * roots);
    }

    /**
     * @private
     */
    _matchesFilter(datum, index) {
        index = index || -1;

        if(!this.isFiltered)
            return true;
        else
            return this._filterCallback(datum, index);
    }

    /**
     * @private
     */
    _createDOM(datum, index) {
        const fragment = this._template.fragment(datum);

        if(this.isFiltered && !this._filterMatch.has(index))
            _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"].hide(fragment.children);

        this._element.appendChild(fragment);

        this._getIndexes(index).forEach(i => {
            this._elementDataMap.set(this._element.children[i], datum);
            this._observer.observe(this._element.children[i]);
        });
    }

    /**
     * @private
     */
    _lazyDOM(largestFilterMatchIndex) {
        const dataLength = this._data.length;

        if(this._dataIndex === dataLength) return;

        const diff = this._visibleElementsLength - this._visibleDOM;
        largestFilterMatchIndex = largestFilterMatchIndex || -1;

        const update = count => {
            for(; count > 0 && this._dataIndex < dataLength; count--, this._dataIndex++)
                this._createDOM(this._data[this._dataIndex], this._dataIndex);
        };

        if(this._dataIndex === 0 || diff <= 0)
            update(this._chunk);

        if(largestFilterMatchIndex >= this._dataIndex) {
            const needed = largestFilterMatchIndex - this._dataIndex + 1;
            const chunks = this._chunk * Math.ceil(needed / this._chunk);
            update(chunks);
        }
    }

    /**
     * Is model empty (i.e. has no data)
     *
     * @returns {Boolean}
     */
    get isEmpty() {
        return this._data.length === 0;
    }

    /**
     * Is model filtered
     *
     * @returns {Boolean}
     */
    get isFiltered() {
        return Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isFunction"])(this._filterCallback);
    }

    /**
     * Lazy-load DOM elements
     *
     * @default false
     * @param {Boolean} [bool=true]
     * @returns {Model} `this`
     */
    lazy(bool) {
        this._lazy = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["boolDefault"])(bool, true);
        return this;
    }

    /**
     * How many DOM elements to load at a time
     *
     * This option does nothing when lazy-loading isn't used.
     *
     * @default 1
     * @param {Number} chunk
     * @throws {(TypeError|RangeError)}
     * @returns {Model} `this`
     */
    chunk(chunk) {
        this._chunk = _misc__WEBPACK_IMPORTED_MODULE_4__["Numbers"].between(chunk, 1, _misc__WEBPACK_IMPORTED_MODULE_4__["Numbers"].PlusInfinity);
        return this;
    }

    /**
     * Set the template of the model
     *
     * Template data that should be replaced by actual data is identified
     * by an id string enclosed in curly brackets, e.g. {some-id}.
     *
     * Strings that will have identifiers replaced by data:
     * ```
     * '{id}'
     * '{!#¤%}'
     * '{123A_sdASJH}'
     * '  {whitespace_before_and_after}   '
     * 'Full name: {fullName}'
     * '{fullName} - {occupation}'
     * 'This value will be: {{enclosed_in_braces}}'
     * ```
     *
     * Strings that will be left as is:
     * ```
     * '{ id}'
     * '{}'
     * '{no spaces}'
     * '{{}}'
     * 'Something {something{}}'
     * ```
     *
     * @example
     * --- HTML:
     * <template id="my-template">
     *     <p class="container">
     *         <span>{first}</span>
     *         <span>{second}</span>
     *         <a href="{url}">{url}</a>
     *         <p>My image: <img src="{image}"></p>
     *     </p>
     * </template>
     *
     * --- JavaScript:
     * const myModel = new fw.Model("#my-container");
     * myModel.template(fw.one("#my-template"));
     * myModel.append({
     *     first: "Yay!",
     *     second: "!yaY",
     *     url: "http://example.com",
     *     image: "images/hello.jpg"
     * });
     *
     * --- Result:
     * <your-model-container-element id="my-container">
     *     <p class="container">
     *         <span>Yay!</span>
     *         <span>!yaY</span>
     *         <a href="http://example.com">http://example.com</a>
     *         <p>My image: <img src="images/hello.jpg"></p>
     *     </p>
     * </your-model-container-element>
     *
     * @param {(Element|String)} tmpl HTML content or a `<template>` element
     * @throws {(TemplateDefinedError|EmptyTemplateError)}
     * @returns {Model} `this`
     */
    template(tmpl) {
        this._template.setTemplate(tmpl);
        return this;
    }

    /**
     * Specify a function to be called when elements become visible
     *
     * @example
     * const model = new fw.Model("#my-model");
     * model.template(...);
     * model.visible((data, element) => {
     *     console.log(data, element);
     * });
     *
     * model.append([{num: 1, name: "first"}, {num: 2, name: "second"}]);
     *
     * // The function would now be called twice, with these parameters
     * // (if the appended data caused visible elements)
     * // (if your template has more than one root element, func gets called on all of them)
     * func(modelData[0], firstElement);
     * func(modelData[1], secondElement);
     *
     * @param {Function} func `function(element)`
     * @throws {TypeError}
     * @returns {Model} `this`
     */
    visible(func) {
        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isFunction"])(func))
            throw new TypeError("Parameter wasn't a function");

        this._visible = func;
        return this;
    }

    /**
     * Set the transition for models child elements
     *
     * This is transition is used to show/hide the elements.
     *
     * @param {Transition} trans
     * @return {Model} `this`
     */
    transition(trans) {
        if(!_transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"]._valid(trans))
            throw new TypeError("Parameter was not a Transition");

        this._transition = trans;
        return this;
    }

    /**
     * Clear model data and remove all DOM elements
     */
    clear() {
        this._data = [];
        this._elementDataMap = new Map();
        this._dataIndex = 0;
        this._visibleDOM = 0;
        this._filterMatch = undefined;

        while(this._element.lastChild) {
            if(this._element.lastChild instanceof Element)
                this._observer.unobserve(this._element.lastChild);

            this._element.removeChild(this._element.lastChild);
        }
    }

    /**
     * Append data to the model
     *
     * @param {(Object|Object[])} data
     * @throws {ModelHasNoTemplateError}
     */
    async append(data) {
        let dataIndex = this._data.length;
        let largestIndex = undefined;

        if(this.isFiltered)
            this._filterMatch = this._filterMatch || new Map();

        _arrays__WEBPACK_IMPORTED_MODULE_1__["concat"](data).forEach(datum => {
            const thisIndex = dataIndex++;

            this._data.push(datum);

            if(this.isFiltered && this._matchesFilter(datum, thisIndex)) {
                this._filterMatch.set(thisIndex, true);
                largestIndex = thisIndex;
            }

            if(!this._lazy) this._createDOM(datum, thisIndex);
        });

        if(this._lazy) this._lazyDOM(largestIndex);
    }

    /**
     * Update model with data
     *
     * This removes the old data and DOM elements.
     *
     * @param {(Object|Object[])} data
     * @throws {ModelHasNoTemplateError}
     */
    async update(data) {
        this.clear();
        await this.append(data);
    }

    /**
     * Filter model
     *
     * Shows elements that match the filter, hides the rest.
     * Calling filter with no arguments removes filtering.
     *
     * @param {Function} [callback] `function(object, dataIndex)`
     * @throws {ModelHasNoTemplateError}
     */
    async filter(callback) {
        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isFunction"])(callback) && !Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isNone"])(callback))
            throw new TypeError("Callback wasn't a function");

        this._filterCallback = callback;

        const elements = indexes => {
            const c = this._element.children;
            return _arrays__WEBPACK_IMPORTED_MODULE_1__["flatMap"](indexes, di => this._getIndexes(di).map(i => c[i]));
        };

        const invert = (indexes, length) => {
            const inverted = [];
            let indexesIndex = 0;

            for(let i = 0; i < length; i++) {
                if(i === indexes[indexesIndex])
                    indexesIndex++;
                else
                    inverted.push(i);
            }

            return inverted;
        };

        const currentDataLength = () => this._lazy ? this._dataIndex : this._data.length;

        if(!this.isFiltered) {
            if(!!this._filterMatch) {
                const matches = [...this._filterMatch.keys()];
                const shown = elements(invert(matches, currentDataLength()));
                await this._transition.show(shown);
            }

            return this._filterMatch = undefined;
        }

        let matched = [];
        let unmatched = [];
        let largestIndex = undefined;
        this._filterMatch = new Map();

        this._data.forEach((datum, index) => {
            if(this._matchesFilter(datum, index)) {
                this._filterMatch.set(index, true);
                matched.push(index);
            }
        });

        if(matched.length > 0)
            largestIndex = matched[matched.length - 1];

        if(this._lazy) this._lazyDOM(largestIndex);

        if(matched.length !== currentDataLength())
            unmatched = invert(matched, currentDataLength());

        await this._transition.hide(elements(unmatched));
        await this._transition.show(elements(matched));
    }

    /**
     * Get the data indices that match the filter
     *
     * @returns {(Boolean|Number[])} false if model is not filtered
     */
    getMatches() {
        if(Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isUndefined"])(this._filterMatch))
            return false;
        else
            return [...this._filterMatch.keys()];
    }

    /**
     * Get data associated with this model at index
     *
     * @param {Number} index
     * @returns {Object}
     */
    getData(index) {
        return this._data[index];
    }

    /**
     * Transform a data index to a element index
     *
     * @param {Number} index Data index
     * @returns {(Number|Number[])} Array of indices, if your template has many direct children
     */
    getElementIndex(index) {
        const indexes = this._getIndexes(index);

        if(indexes.length > 1)
            return indexes;
        else
            return indexes[0];
    }

    /**
     * Remove this model
     *
     * Removes the model and all child elements.
     */
    remove() {
        this.clear();
        Object(_proxy__WEBPACK_IMPORTED_MODULE_3__["invalidateCache"])(this._element, "model");
    }
}


/**
 * Create view models for your data
 *
 * @module models
 */
/* harmony default export */ __webpack_exports__["default"] = (Model);


/***/ }),

/***/ "./src/named_colors.js":
/*!*****************************!*\
  !*** ./src/named_colors.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// https://developer.mozilla.org/en-US/docs/Web/CSS/color_value
const namedColors = {
    black: "#000000",
    silver: "#c0c0c0",
    gray: "#808080",
    white: "#ffffff",
    maroon: "#800000",
    red: "#ff0000",
    purple: "#800080",
    fuchsia: "#ff00ff",
    green: "#008000",
    lime: "#00ff00",
    olive: "#808000",
    yellow: "#ffff00",
    navy: "#000080",
    blue: "#0000ff",
    teal: "#008080",
    aqua: "#00ffff",
    orange: "#ffa500",
    aliceblue: "#f0f8ff",
    antiquewhite: "#faebd7",
    aquamarine: "#7fffd4",
    azure: "#f0ffff",
    beige: "#f5f5dc",
    bisque: "#ffe4c4",
    blanchedalmond: "#ffebcd",
    blueviolet: "#8a2be2",
    brown: "#a52a2a",
    burlywood: "#deb887",
    cadetblue: "#5f9ea0",
    chartreuse: "#7fff00",
    chocolate: "#d2691e",
    coral: "#ff7f50",
    cornflowerblue: "#6495ed",
    cornsilk: "#fff8dc",
    crimson: "#dc143c",
    cyan: "#00ffff",
    darkblue: "#00008b",
    darkcyan: "#008b8b",
    darkgoldenrod: "#b8860b",
    darkgray: "#a9a9a9",
    darkgreen: "#006400",
    darkgrey: "#a9a9a9",
    darkkhaki: "#bdb76b",
    darkmagenta: "#8b008b",
    darkolivegreen: "#556b2f",
    darkorange: "#ff8c00",
    darkorchid: "#9932cc",
    darkred: "#8b0000",
    darksalmon: "#e9967a",
    darkseagreen: "#8fbc8f",
    darkslateblue: "#483d8b",
    darkslategray: "#2f4f4f",
    darkslategrey: "#2f4f4f",
    darkturquoise: "#00ced1",
    darkviolet: "#9400d3",
    deeppink: "#ff1493",
    deepskyblue: "#00bfff",
    dimgray: "#696969",
    dimgrey: "#696969",
    dodgerblue: "#1e90ff",
    firebrick: "#b22222",
    floralwhite: "#fffaf0",
    forestgreen: "#228b22",
    gainsboro: "#dcdcdc",
    ghostwhite: "#f8f8ff",
    gold: "#ffd700",
    goldenrod: "#daa520",
    greenyellow: "#adff2f",
    grey: "#808080",
    honeydew: "#f0fff0",
    hotpink: "#ff69b4",
    indianred: "#cd5c5c",
    indigo: "#4b0082",
    ivory: "#fffff0",
    khaki: "#f0e68c",
    lavender: "#e6e6fa",
    lavenderblush: "#fff0f5",
    lawngreen: "#7cfc00",
    lemonchiffon: "#fffacd",
    lightblue: "#add8e6",
    lightcoral: "#f08080",
    lightcyan: "#e0ffff",
    lightgoldenrodyellow: "#fafad2",
    lightgray: "#d3d3d3",
    lightgreen: "#90ee90",
    lightgrey: "#d3d3d3",
    lightpink: "#ffb6c1",
    lightsalmon: "#ffa07a",
    lightseagreen: "#20b2aa",
    lightskyblue: "#87cefa",
    lightslategray: "#778899",
    lightslategrey: "#778899",
    lightsteelblue: "#b0c4de",
    lightyellow: "#ffffe0",
    limegreen: "#32cd32",
    linen: "#faf0e6",
    magenta: "#ff00ff",
    mediumaquamarine: "#66cdaa",
    mediumblue: "#0000cd",
    mediumorchid: "#ba55d3",
    mediumpurple: "#9370db",
    mediumseagreen: "#3cb371",
    mediumslateblue: "#7b68ee",
    mediumspringgreen: "#00fa9a",
    mediumturquoise: "#48d1cc",
    mediumvioletred: "#c71585",
    midnightblue: "#191970",
    mintcream: "#f5fffa",
    mistyrose: "#ffe4e1",
    moccasin: "#ffe4b5",
    navajowhite: "#ffdead",
    oldlace: "#fdf5e6",
    olivedrab: "#6b8e23",
    orangered: "#ff4500",
    orchid: "#da70d6",
    palegoldenrod: "#eee8aa",
    palegreen: "#98fb98",
    paleturquoise: "#afeeee",
    palevioletred: "#db7093",
    papayawhip: "#ffefd5",
    peachpuff: "#ffdab9",
    peru: "#cd853f",
    pink: "#ffc0cb",
    plum: "#dda0dd",
    powderblue: "#b0e0e6",
    rosybrown: "#bc8f8f",
    royalblue: "#4169e1",
    saddlebrown: "#8b4513",
    salmon: "#fa8072",
    sandybrown: "#f4a460",
    seagreen: "#2e8b57",
    seashell: "#fff5ee",
    sienna: "#a0522d",
    skyblue: "#87ceeb",
    slateblue: "#6a5acd",
    slategray: "#708090",
    slategrey: "#708090",
    snow: "#fffafa",
    springgreen: "#00ff7f",
    steelblue: "#4682b4",
    tan: "#d2b48c",
    thistle: "#d8bfd8",
    tomato: "#ff6347",
    turquoise: "#40e0d0",
    violet: "#ee82ee",
    wheat: "#f5deb3",
    whitesmoke: "#f5f5f5",
    yellowgreen: "#9acd32",
    rebeccapurple: "#663399",
    transparent: "#ffffff00"
};


/**
 * @module named_colors
 * @private
 */
/* harmony default export */ __webpack_exports__["default"] = (namedColors);


/***/ }),

/***/ "./src/proxy.js":
/*!**********************!*\
  !*** ./src/proxy.js ***!
  \**********************/
/*! exports provided: createFrameworkProxy, invalidateCache */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createFrameworkProxy", function() { return createFrameworkProxy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "invalidateCache", function() { return invalidateCache; });
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./data */ "./src/data.js");
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ui */ "./src/ui.js");
/* harmony import */ var _bindings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bindings */ "./src/bindings.js");
/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./models */ "./src/models.js");
/* harmony import */ var _messages__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./messages */ "./src/messages.js");
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./transitions */ "./src/transitions.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./misc */ "./src/misc.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./errors */ "./src/errors.js");











/**
 * framework.js proxy
 *
 * These are the methods available in the Proxy returned by {@link fw fw(selector)}.
 *
 * @example
 * // Some methods can work with several elements
 *
 * // All elements matching 'a.navigation' will be hidden
 * fw("a.navigation").$hide();
 * // All elements matching 'p' will now have the class my-paragraph
 * fw("p").$addClass("my-paragraph");
 *
 *
 * // Some methods can only work with single elements
 * const tabView = fw("#my-tab-view").$tabView();
 *
 * // This would result in error (if document has many divs)
 * fw("div").$tabView();
 *
 *
 * // If fw(...) results in a single element, all the elements members can be accessed normally
 * fw("#main-navigation").children;  // HTMLCollection
 * fw("#main-navigation").textContent = "No navigation for you";
 *
 *
 * // If fw(...) results in several elements, all of the Array.prototype methods work normally
 * fw("div").forEach(div => { div.textContent = "Noice." });
 *
 * // NOTE: the elements in the array are just basic unmodified Elements
 * // i.e. This wont work:
 * fw("div").forEach(div => div.$hide());  // Using a proxy method here will fail
 *
 * // The elements need to be wrapped up first:
 * fw("div").forEach(div => fw(div).$hide());  // This will work
 *
 * @typicalname fw(selector)
 * @global
 */
const frameworkProxy = {};


/**
 * @private
 */
const frameworkProxyHandler = {
    get(target, property, proxy) {
        if(property === "$__frameworkProxy")
            return true;

        if(Object(_misc__WEBPACK_IMPORTED_MODULE_7__["isFunction"])(target[property]))
            return target[property].bind(target);
        else if(!Object(_misc__WEBPACK_IMPORTED_MODULE_7__["isUndefined"])(target[property]))
            return target[property];

        const many = Array.isArray(target);

        if(frameworkProxy.hasOwnProperty(property))
            return frameworkProxy[property].bind({target, proxy, many});

        return undefined;
    },
    set(target, property, value, proxy) {
        if(property === "$__frameworkProxy")
            return false;

        target[property] = value;
        return true;
    }
};


/**
 * @private
 */
function createFrameworkProxy(target) {
    return new Proxy(target, frameworkProxyHandler);
}


/**
 * @private
 */
function invalidateCache(target, name) {
    const methodName = '$' + name;
    _data__WEBPACK_IMPORTED_MODULE_1__["remove"](target, methodName);
}


/**
 * func will have it's this binded to an object with these keys:
 * target: target element, or an array of elements
 * proxy:  the proxy object
 * many:   if true, target is an array of elements
 *
 * @private
 * @param {String} name
 * @param {Boolean} remember If true, remember the return value for this target
 * @param {Function} func
 */
function addProxyMethod(name, remember, func) {
    const methodName = '$' + name;

    if(remember) {
        frameworkProxy[methodName] = function(...args) {
            const funcArgs = func.bind(this, ...args);

            if(this.many)
                return funcArgs();

            if(_data__WEBPACK_IMPORTED_MODULE_1__["has"](this.target, methodName)) {
                const value = _data__WEBPACK_IMPORTED_MODULE_1__["get"](this.target, methodName);

                if(!Object(_misc__WEBPACK_IMPORTED_MODULE_7__["isUndefined"])(value))
                    return value;
            }

            const ret = funcArgs();

            if(!Object(_misc__WEBPACK_IMPORTED_MODULE_7__["isUndefined"])(ret)) {
                _data__WEBPACK_IMPORTED_MODULE_1__["set"](this.target, methodName, ret);
                return ret;
            }
            else {
                return this.proxy;
            }
        };
    }
    else {
        frameworkProxy[methodName] = function(...args) {
            const ret = func.call(this, ...args);
            return !Object(_misc__WEBPACK_IMPORTED_MODULE_7__["isUndefined"])(ret) ? ret : this.proxy;
        };
    }
}


/**
 * Returns the raw, unproxied target
 *
 * @returns {(Element|Element[])}
 * @name $raw
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("raw", false, function() {
    return this.target;
});


/**
 * Support for forEach
 *
 * This is guaranteed to work even if `fw(selector)` matches only one element.
 * A normal forEach won't work in that case, as the proxy target will not be an array.
 *
 * @param {Function} callback The callback function
 * @param {*} [thisArg] The value of `this` in `callback`
 * @name $forEach
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("forEach", false, function(callback, thisArg) {
    _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](this.target).forEach(callback, thisArg);
});


/**
 * Remove all the children of the element(s)
 *
 * @name $removeChildren
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("removeChildren", false, function() {
    _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](this.target).forEach(e => {
        while(!!e.lastChild) e.removeChild(e.lastChild);
    });
});


/**
 * Toggle a class of the element(s)
 *
 * @param {String} cls
 * @name $toggleClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("toggleClass", false, function(cls) {
    _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](this.target).forEach(x => x.classList.toggle(cls));
});


/**
 * Add a class to the element(s)
 *
 * @param {String} cls
 * @name $addClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("addClass", false, function(cls) {
    _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](this.target).forEach(x => x.classList.add(cls));
});


/**
 * Remove a class from the element(s)
 *
 * @param {String} cls
 * @name $removeClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("removeClass", false, function(cls) {
    _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](this.target).forEach(x => x.classList.remove(cls));
});


/**
 * Replace a class of the element(s) with another class
 *
 * @param {String} cls
 * @param {String} replacement
 * @name $replaceClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("replaceClass", false, function(cls, replacement) {
    _arrays__WEBPACK_IMPORTED_MODULE_0__["concat"](this.target).forEach(x => x.classList.replace(cls, replacement));
});


/**
 * Create a binding for this input
 *
 * @example
 * const binding = fw("#my-input").$binding("input");
 *
 * // This would set the title of the document to
 * // whatever is the value of the input in uppercase
 * binding.output(document, "title").transform(function(value) {
 *     return value.toUpperCase();
 * });
 *
 * @param {String} [event=change] Event type
 * @throws {OperationNotSupportedError}
 * @returns {fw.Binding}
 * @see {@link module:bindings}
 * @name $binding
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("binding", false, function(event) {
    if(this.many)
        throw new _errors__WEBPACK_IMPORTED_MODULE_8__["OperationNotSupportedError"]("$binding doesn't operate on lists");

    return new _bindings__WEBPACK_IMPORTED_MODULE_3__["default"](this.target, event);
});


/**
 * Use the element as a container for a view model
 *
 * @example
 * const model = fw("#my-model-container").$model();
 *
 * model.template("<p><em>{name}</em>: {occupation}</p>");
 *
 * model.append({name: "Full Name", occupation: "Worker"});
 * model.append({name: "Firstname Lastname", occupation: "Work-doer"});
 *
 * #my-model-container:
 * <p><em>Full Name</em>: Worker</p>
 * <p><em>Firstname Lastname</em>: Work-doer</p>
 *
 * @throws {OperationNotSupportedError}
 * @returns {fw.Model}
 * @see {@link module:models}
 * @name $model
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("model", true, function() {
    if(this.many)
        throw new _errors__WEBPACK_IMPORTED_MODULE_8__["OperationNotSupportedError"]("$model doesn't operate on lists");

    return new _models__WEBPACK_IMPORTED_MODULE_4__["default"](this.target);
});


/**
 * Use the element as a container for messages
 *
 * @example
 * const message = fw("#my-message-container").$message();
 *
 * // Disappears after some time
 * message.normal("This is a normal, timed message");
 *
 * // Disappears when clicked
 * message.urgent("This message has to be clicked away");
 *
 * // Disappears when clicked
 * message.urgent("This message has an id", "my-message");
 *
 * // Message with the same id is already shown
 * message.urgent("This message won't be shown at all", "my-message");
 *
 * @throws {OperationNotSupportedError}
 * @returns {fw.Message}
 * @see {@link module:messages}
 * @name $message
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("message", true, function() {
    if(this.many)
        throw new _errors__WEBPACK_IMPORTED_MODULE_8__["OperationNotSupportedError"]("$message doesn't operate on lists");

    return new _messages__WEBPACK_IMPORTED_MODULE_5__["default"](this.target);
});


/**
 * Toggle the visibility of element(s)
 *
 * @param {fw.Transition} [transition]
 * @param {Boolean} [shown]
 * @returns {Promise}
 * @see {@link module:transitions}
 * @name $toggle
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("toggle", false, function(transition, shown) {
    return (_transitions__WEBPACK_IMPORTED_MODULE_6__["Transition"]._valid(transition) ? transition : _transitions__WEBPACK_IMPORTED_MODULE_6__["Transition"]).toggle(this.target, shown);
});


/**
 * Hide the element(s)
 *
 * @param {fw.Transition} [transition]
 * @returns {Promise}
 * @see {@link module:transitions}
 * @name $hide
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("hide", false, function(transition) {
    return (_transitions__WEBPACK_IMPORTED_MODULE_6__["Transition"]._valid(transition) ? transition : _transitions__WEBPACK_IMPORTED_MODULE_6__["Transition"]).hide(this.target);
});


/**
 * Show the element(s)
 *
 * @param {fw.Transition} [transition]
 * @returns {Promise}
 * @see {@link module:transitions}
 * @name $show
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("show", false, function(transition) {
    return (_transitions__WEBPACK_IMPORTED_MODULE_6__["Transition"]._valid(transition) ? transition : _transitions__WEBPACK_IMPORTED_MODULE_6__["Transition"]).show(this.target);
});


/**
 * Transform the element into a tabbed view
 *
 * @param {Number} [activeIndex=0] Index of tab to set active
 * @param {fw.ui.TabType} [type=fw.ui.TabType.Normal]
 * @throws {OperationNotSupportedError}
 * @returns {fw.ui.TabView}
 * @see {@link module:ui}
 * @name $tabView
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("tabView", true, function(activeIndex, type) {
    if(this.many)
        throw new _errors__WEBPACK_IMPORTED_MODULE_8__["OperationNotSupportedError"]("$tabView doesn't operate on lists");

    return new _ui__WEBPACK_IMPORTED_MODULE_2__["TabView"](this.target, activeIndex, type);
});


/**
 * @module proxy
 * @private
 */



/***/ }),

/***/ "./src/requests.js":
/*!*************************!*\
  !*** ./src/requests.js ***!
  \*************************/
/*! exports provided: Request, Requester */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Request", function() { return Request; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Requester", function() { return Requester; });
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./misc */ "./src/misc.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./errors */ "./src/errors.js");





const DOCUMENT_MIMES = ["application/xml", "image/svg+xml", "text/html", "text/xml"];
const TYPES = ["arrayBuffer", "blob", "formData", "document", "json", "text"];
const METHODS = ["GET", "HEAD", "POST", "PUT", "DELETE",
                 "CONNECT", "OPTIONS", "TRACE", "PATCH"];


function parseDocument(response, body) {
    const contentType = response.headers.get("content-type");
    let mime = "text/html";

    if(!!contentType) {
        mime = contentType.split(';')[0].toLowerCase().trim();
        mime = !DOCUMENT_MIMES.includes(mime) ? "text/html" : mime;
    }

    return (new DOMParser()).parseFromString(body, mime);
}


/**
 * @private
 */
class RequestQueue {
    constructor(maxRequests) {
        this.maxRequests = maxRequests || 1;
        this.requests = {};
        this.activeRequests = {};
        this.slot = 0;
        this.id = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["identifier"])();
    }

    nextSlot() {
        if(this.slot >= this.maxRequests) this.slot = 0;
        return this.slot++;
    }

    /**
     * @param {Request} request
     * @returns {Promise} resolve({body, response}), reject(error)
     */
    append(request) {
        const match = Object.values(this.requests).find(x => Request.is(x.request, request));

        if(!!match) return match.promise;

        const internal = {id: this.id(), promise: null, request};
        this.requests[internal.id] = internal;

        internal.promise = new Promise((resolve, reject) => {
            const index = this.nextSlot();
            let active = this.activeRequests[index];

            if(!active) {
                active = {index, promise: Promise.resolve(), length: 0};
                this.activeRequests[index] = active;
            }

            active.length++;

            active.promise = active.promise.finally(() => {
                Requester.send(internal.request).then(resolve, reject).finally(() => {
                    if(--active.length === 0)
                        delete this.activeRequests[active.index];

                    delete this.requests[internal.id];
                });
            });
        });

        return internal.promise;
    }
}


/**
 * Class for creating requests
 *
 * @alias module:requests.Request
 */
class Request {
    /**
     * Are requests the same
     *
     * @param {Request} first
     * @param {Request} second
     * @returns {Boolean}
     */
    static is(first, second) {
        const f = first, s = second;

        const basicEquals = f._url === s._url &&
                            f._method === s._method &&
                            f._type === s._type &&
                            f._headersLength === s._headersLength &&
                            f._body == s._body;

        const headerEquals = () => {
            return [...f._headers.entries()].every(([n, v]) => s._headers.get(n) === v);
        };

        return f === s || (basicEquals && headerEquals());
    }

    /**
     * Create a new Request instance
     *
     * @param {(String|URL)} url
     */
    constructor(url) {
        this._url = url instanceof URL ? url.href : url;
        this._method = "GET";
        this._type = "text";
        this._headers = new Headers();
        this._headersLength = 0;
        this._body = undefined;
        this._sent = false;
    }

    /**
     * Request method
     *
     * The HTTP request method to use: GET, POST, etc.
     *
     * @default GET
     * @param {String} requestMethod
     * @throws {(InvalidValueError|RequestSentError)}
     * @returns {Request} `this`
     */
    method(requestMethod) {
        if(this._sent)
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["RequestSentError"]("Request has been sent");

        if(!METHODS.includes(requestMethod))
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["InvalidValueError"](`Invalid method: ${requestMethod}`);

        this._method = requestMethod;
        return this;
    }

    /**
     * The type of the response body
     *
     * This will decide the type of the body the request will eventually resolve with.
     *
     * Possible values: `arrayBuffer`, `blob`, `formData`, `document`, `json`, `text`
     *
     * @default text
     * @param {String} responseType
     * @throws {(InvalidValueError|RequestSentError)}
     * @returns {Request} `this`
     */
    type(responseType) {
        if(this._sent)
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["RequestSentError"]("Request has been sent");

        if(!TYPES.includes(responseType))
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["InvalidValueError"](`Invalid type: ${responseType}`);

        this._type = responseType;
        return this;
    }

    /**
     * Data to send to the server
     *
     * @param {(Blob|BufferSource|FormData|URLSearchParams|String)} data
     * @throws {RequestSentError}
     * @returns {Request} `this`
     */
    body(data) {
        if(this._sent)
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["RequestSentError"]("Request has been sent");

        this._body = data;
        return this;
    }

    /**
     * Add a request header
     *
     * @param {String} name
     * @param {String} value
     * @throws {(RequestSentError|TypeError)}
     * @returns {Request} `this`
     */
    header(name, value) {
        if(this._sent)
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["RequestSentError"]("Request has been sent");

        this._headers.append(name, value);
        this._headersLength++;
        return this;
    }
}


/**
 * Class for sending and queueing requests
 *
 * @alias module:requests.Requester
 */
class Requester {
    /**
     * Send a request
     *
     * @param {Request} request
     * @throws {RequestSentError}
     * @returns {Promise} `resolve({body, response})`, `reject(error)`
     */
    static send(request) {
        const r = request;

        if(r._sent) throw new _errors__WEBPACK_IMPORTED_MODULE_2__["RequestSentError"]("Request has been sent");

        const data = {
            method: r._method,
            headers: r._headers,
            redirect: "follow"
        };

        if(r._body !== undefined)
            data["body"] = r._body;

        r._sent = true;

        return new Promise((resolve, reject) => {
            fetch(r._url, data).then(response => {
                if(!response.ok)
                    return reject(new Error(`Request failed with code: ${response.status}`));

                const method = r._type === "document" ? "text" : r._type;

                response[method].call(response).then(body => {
                    if(r._type === "document")
                        body = parseDocument(response, body);

                    resolve({body, response});
                }).catch(reject);
            }).catch(reject);
        });
    }

    /**
     * Load HTML from `url` and replace the contents of `element` with the result
     *
     * If `append` is true, the resulting children will be appended to the element,
     * instead of replacing old children.
     *
     * @param {(String|Url)} url
     * @param {(String|Element)} element Element/Selector
     * @param {Boolean} [append=false]
     */
    static load(url, element, append) {
        element = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["realElement"])(element);
        const req = new Request(url);

        return new Promise((resolve, reject) => {
            Requester.send(req).then(({body: html}) => {
                const temp = document.createElement("div");
                temp.innerHTML = html;

                if(!append) {
                    while(element.firstChild)
                        element.removeChild(element.firstChild);
                }

                _arrays__WEBPACK_IMPORTED_MODULE_0__["forEach"](temp.childNodes, node => {
                    element.appendChild(document.adoptNode(node));
                });

                resolve();
            }).catch(reject);
        });
    }

    /**
     * Create a new Requester instance
     */
    constructor() {
        this._queue = new RequestQueue(1);
    }

    /**
     * How many requests are handled in parallel
     *
     * @default 1
     * @param {Number} requests
     * @throws {(TypeError|RangeError)}
     * @returns {Requester} `this`
     */
    parallel(requests) {
        this._queue.maxRequests = _misc__WEBPACK_IMPORTED_MODULE_1__["Numbers"].between(requests, 1, 16);
        return this;
    }

    /**
     * Append a new request to the queue
     *
     * @param {Request} request
     * @throws {RequestSentError}
     * @returns {Promise} `resolve({body, response})`, `reject(error)`
     */
    append(request) {
        if(request._sent)
            throw new _errors__WEBPACK_IMPORTED_MODULE_2__["RequestSentError"]("Request has been sent");

        this._queue.append(request);
    }

    /**
     * Convenience method for creating and appending a request
     *
     * @param {String} url
     * @param {String} [type=text]
     * @param {String} [method=GET]
     * @throws {InvalidValueError}
     * @returns {Promise} `resolve({body, response})`, `reject(error)`
     */
    request(url, type, method) {
        type = type || "text";
        method = method || "GET";

        return this._queue.append((new Request(url)).type(type).method(method));
    }
}


/**
 * Simplified web requests
 *
 * @module requests
 */



/***/ }),

/***/ "./src/transitions.js":
/*!****************************!*\
  !*** ./src/transitions.js ***!
  \****************************/
/*! exports provided: TransitionChain, Transition */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransitionChain", function() { return TransitionChain; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Transition", function() { return Transition; });
/* harmony import */ var _css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./css */ "./src/css.js");
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data */ "./src/data.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./misc */ "./src/misc.js");
/* harmony import */ var _errors__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./errors */ "./src/errors.js");







const TRANSITION_PROPERTY = "transitionPromise";
const DISPLAY_PROPERTY = "originalDisplay";
const RECT_PROPERTY = "boundingRect";
const CLIP_PROPERTY = "clipPath";
const DIRECTIONS = ["up", "right", "down", "left"];
const SCALINGS = ["horizontal", "vertical", "both"];
const TIMINGS = ["linear", "ease", "ease-in", "ease-out", "ease-in-out"];


/**
 * @private
 */
class ElementDisplay {
    static initValue(element) {
        if(!!element.style.display) return;

        element.style.display = _css__WEBPACK_IMPORTED_MODULE_0__["computedStyle"](element).display;
    }

    static originalValue(element) {
        let originalDisplay = _data__WEBPACK_IMPORTED_MODULE_2__["get"](element, DISPLAY_PROPERTY);

        if(!originalDisplay) {
            originalDisplay = element.style.display;

            // If element has inline display set, and its none,
            // find what happens when its removed
            if(originalDisplay === "none") {
                const clone = element.cloneNode(false);
                clone.style.display = '';
                originalDisplay = _css__WEBPACK_IMPORTED_MODULE_0__["computedStyle"](clone).display;
            }
            // No inline style, compute the value of display
            else if(!originalDisplay) {
                originalDisplay = _css__WEBPACK_IMPORTED_MODULE_0__["computedStyle"](element).display;
            }

            // Display is set to none in a CSS rule,
            // find the default display for the tag
            if(originalDisplay === "none")
                originalDisplay = _css__WEBPACK_IMPORTED_MODULE_0__["computedStyle"](element.tagName).display;

            _data__WEBPACK_IMPORTED_MODULE_2__["set"](element, DISPLAY_PROPERTY, originalDisplay);
        }

        return originalDisplay;
    }

    static newValue(element, display, shown) {
        ElementDisplay.initValue(element);

        display = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["isNone"])(display) ? element.style.display : display;
        const original = ElementDisplay.originalValue(element);

        if(Object(_misc__WEBPACK_IMPORTED_MODULE_3__["isNone"])(shown))
            return display !== "none" ? "none" : original;
        else
            return !!shown ? original : "none";
    }
}


/**
 * @private
 */
class TransitionRunner {
    static getTransition(element) {
        return _data__WEBPACK_IMPORTED_MODULE_2__["get"](element, TRANSITION_PROPERTY);
    }

    constructor(element, transition, display) {
        this.element = element;
        this.transitions = transition._transitions;
        this.duration = transition._duration;
        this.easing = transition._easing;
        this.clip = transition._clip && !!this.element.parentElement;
        this.display = display;
        this.hiding = this.display === "none";
        this.overflow = null;
        this.parentClip = null;
        this.rect = null;
        this.parentRect = null;
        this.transform = null;

        return this.run();
    }

    /* <Transition methods> */
    move() {
        const d = this.get("move");

        let parentX = this.parentRect.x;
        let parentY = this.parentRect.y;
        let selfX = this.rect.x;
        let selfY = this.rect.y;

        if(d === "right")     parentX += this.parentRect.width;
        else if(d === "down") parentY += this.parentRect.height;
        else if(d === "left") selfX += this.rect.width;
        else if(d === "up")   selfY += this.rect.height;

        const xPos = d === "left" || d === "right" ? parentX - selfX : 0;
        const yPos = d === "up" || d === "down" ? parentY - selfY : 0;

        const x = this.hiding ? [0, xPos] : [xPos, 0];
        const y = this.hiding ? [0, yPos] : [yPos, 0];

        return ["transform", [`translate(${x[0]}px, ${y[0]}px)`, `translate(${x[1]}px, ${y[1]}px)`]];
    }

    rotate() {
        const degrees = this.get("rotate");
        const [from, to] = this.hiding ? [0, degrees] : [degrees, 0];

        return ["transform", [`rotate(${from}deg)`, `rotate(${to}deg)`]];
    }

    scale() {
        const scaleAxis = this.get("scale");
        const scale = this.hiding ? [1, 0] : [0, 1];
        const x = scaleAxis === "horizontal" || scaleAxis === "both" ? scale : [1, 1];
        const y = scaleAxis === "vertical" || scaleAxis === "both" ? scale : [1, 1];

        return ["transform", [`scale(${x[0]}, ${y[0]})`, `scale(${x[1]}, ${y[1]})`]];
    }

    width() {
        const w = this.rect.width;
        const [from, to] = this.hiding ? [w, 0] : [0, w];
        return ["width", [`${from}px`, `${to}px`]];
    }

    height() {
        const h = this.rect.height;
        const [from, to] = this.hiding ? [h, 0] : [0, h];
        return ["height", [`${from}px`, `${to}px`]];
    }

    opacity() {
        return ["opacity", this.hiding ? [1, 0] : [0, 1]];
    }
    /* </Transition methods> */

    get(transition) {
        return this.transitions[transition];
    }

    setup() {
        // Transform and other properties may not be
        // properly calculated when display is none
        if(!this.hiding) {
            this.element.style.visibility = "hidden";
            this.element.style.display = this.display;
        }

        let rect = this.element.getBoundingClientRect();
        let transform = _css__WEBPACK_IMPORTED_MODULE_0__["computedStyle"](this.element).transform;

        if(transform === "none")
            transform = '';

        if(!this.hiding) {
            this.element.style.display = "none";
            this.element.style.visibility = '';
        }

        this.overflow = this.element.style.overflow;
        this.rect = rect;
        this.transform = transform;

        if(this.clip) {
            const parent = this.element.parentElement;

            if(_data__WEBPACK_IMPORTED_MODULE_2__["has"](parent, CLIP_PROPERTY)) {
                this.parentClip = _data__WEBPACK_IMPORTED_MODULE_2__["get"](parent, CLIP_PROPERTY);
            }
            else {
                this.parentClip = parent.style.clipPath;
                _data__WEBPACK_IMPORTED_MODULE_2__["set"](parent, CLIP_PROPERTY, this.parentClip);
            }

            this.parentRect = parent.getBoundingClientRect();
        }
        else {
            this.parentRect = {
                x: 0, y: 0,
                width: window.innerWidth,
                height: window.innerHeight
            };
        }
    }

    keyframes() {
        const styles = {};

        const joiner = (first, second) => {
            return first.map((value, index) => {
                return [value, second[index]].join(' ');
            });
        };

        Object.keys(this.transitions).forEach(method => {
            const [property, value] = this[method].call(this);

            if(property === "transform" && !styles.hasOwnProperty("transform"))
                styles[property] = [this.transform, this.transform];

            if(styles.hasOwnProperty(property))
                styles[property] = joiner(styles[property], value);
            else
                styles[property] = value;
        });

        return styles;
    }

    run() {
        const createAnimation = () => {
            this.setup();

            const options = {duration: this.duration, easing: this.easing};
            const animation = this.element.animate(this.keyframes(), options);
            animation.pause();

            return animation;
        };

        const preAnimation = () => {
            if(this.clip)
                this.element.parentElement.style.clipPath = "inset(0 0)";

            this.element.style.overflow = "hidden";

            if(!this.hiding)
                this.element.style.display = this.display;
        };

        const postAnimation = () => {
            if(this.hiding)
                this.element.style.display = this.display;
            else
                _data__WEBPACK_IMPORTED_MODULE_2__["remove"](this.element, RECT_PROPERTY);

            this.element.style.overflow = this.overflow;

            if(this.clip)
                this.element.parentElement.style.clipPath = this.parentClip;

            _data__WEBPACK_IMPORTED_MODULE_2__["remove"](this.element, TRANSITION_PROPERTY);
        };

        const running = TransitionRunner.getTransition(this.element);
        const current = {
            animation: null,
            promise: Promise.resolve(),
            display: this.display,
        };

        if(!!running) {
            if(this.display === running.display)
                return Promise.resolve();

            current.promise = running.promise;
        }
        else if(this.element.style.display === this.display) {
            return Promise.resolve();
        }

        current.promise = current.promise.then(() => {
            _data__WEBPACK_IMPORTED_MODULE_2__["set"](this.element, TRANSITION_PROPERTY, current);
            current.animation = createAnimation();

            return new Promise(resolve => {
                let start = null;

                const animationRunner = timestamp => {
                    if(start === null) {
                        start = timestamp;
                        preAnimation();
                    }

                    const diff = timestamp - start;

                    if(diff >= this.duration) {
                        postAnimation();
                        current.animation.finish();
                        resolve();
                    }
                    else {
                        current.animation.currentTime = diff;
                        requestAnimationFrame(animationRunner);
                    }
                };

                requestAnimationFrame(animationRunner);
            });
        });

        return current.promise;
    }
}


/**
 * Class for chaining transitions
 *
 * @alias module:transitions.TransitionChain
 */
class TransitionChain {
    /**
     * Create a new TransitionChain instance
     */
    constructor() {
        this._chain = [];
        this._chain_reverse = [];
        this._alternate = false;
        this._promise = Promise.resolve();
        this._runs = 0;
    }

    /**
     * Alternate direction of the chain after running transitions
     *
     * Possibly useful if elements in the chain have a parent-child relation.
     *
     * @default false
     * @param {Boolean} [bool=true]
     * @returns {TransitionChain} `this`
     */
    alternate(bool) {
        this._alternate = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["boolDefault"])(bool, true);
        return this;
    }

    /**
     * Append an element and its transition to the chain
     *
     * @param {(Element|String)} element Element/Selector
     * @param {Transition} transition
     * @throws {(TypeError|InvalidValueError)}
     * @returns {TransitionChain} `this`
     */
    append(element, transition) {
        element = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["realElement"])(element);

        if(!Transition._valid(transition))
            throw new TypeError("Parameter wasn't a Transition");

        if(this._chain.some(([e, _]) => e === element))
            throw new _errors__WEBPACK_IMPORTED_MODULE_4__["InvalidValueError"]("Element already in the chain");

        this._chain.push([element, transition]);
        this._chain_reverse.unshift([element, transition]);
        return this;
    }

    /**
     * Toggle the visibility of all the elements in the chain
     *
     * @param {Boolean} [shown]
     * @returns {Promise}
     */
    toggle(shown) {
        const chain = this._runs++ % 2 !== 0 && this._alternate ? this._chain_reverse : this._chain;

        chain.forEach(([element, transition]) => {
            this._promise = this._promise.then(() => {
                return transition.toggle(element, shown);
            });
        });

        return this._promise;
    }

    /**
     * Hide all the elements in the chain
     *
     * @returns {Promise}
     */
    hide() {
        return this.toggle(false);
    }

    /**
     * Show all the elements in the chain
     *
     * @returns {Promise}
     */
    show() {
        return this.toggle(true);
    }
}


/**
 * Class for element transitions
 *
 * @alias module:transitions.Transition
 */
class Transition {
    /**
     * @private
     */
    static _valid(trans) {
        return trans instanceof Transition || trans === Transition;
    }

    /**
     * Toggle the visibility of element(s) synchronously
     *
     * If transition is already running on the element, this has to wait for
     * the transition to cancel, which is an asynchronous operation.
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @param {Boolean} [shown]
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    static toggle(element, shown) {
        const promises = [Promise.resolve()];

        _arrays__WEBPACK_IMPORTED_MODULE_1__["concat"](element).forEach(elem => {
            elem = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["realElement"])(elem);

            let display = elem.style.display || null;
            const running = TransitionRunner.getTransition(elem);

            if(!!running)
                display = running.display;

            const newDisplay = ElementDisplay.newValue(elem, display, shown);

            if(!!running) {
                running.promise = running.promise.then(() => {
                    elem.style.display = newDisplay;
                });

                promises.push(running.promise);
            }
            else {
                elem.style.display = newDisplay;
            }
        });

        return Promise.all(promises);
    }

    /**
     * Hide element(s) synchronously
     *
     * If transition is already running on the element, this has to wait for
     * the transition to cancel, which is an asynchronous operation.
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    static hide(element) {
        return Transition.toggle(element, false);
    }

    /**
     * Show element(s) synchronously
     *
     * If transition is already running on the element, this has to wait for
     * the transition to cancel, which is an asynchronous operation.
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    static show(element) {
        return Transition.toggle(element, true);
    }

    /**
     * Create a new Transition instance
     */
    constructor() {
        this._transitions = {};
        this._duration = 200;
        this._easing = "linear";
        this._clip = false;
    }

    /**
     * The length of the transition
     *
     * @default 200
     * @param {Number} milliseconds
     * @throws {(TypeError|RangeError)}
     * @returns {Transition} `this`
     */
    duration(milliseconds) {
        this._duration = _misc__WEBPACK_IMPORTED_MODULE_3__["Numbers"].between(milliseconds, 1, _misc__WEBPACK_IMPORTED_MODULE_3__["Numbers"].PlusInfinity);
        return this;
    }

    /**
     * Timing function to use for the transition
     *
     * Possible values: linear, ease, ease-in, ease-out, ease-in-out
     *
     * @default linear
     * @param {String} timing
     * @throws {InvalidValueError}
     * @returns {Transition} `this`
     */
    easing(timing) {
        if(!TIMINGS.includes(timing))
            throw new _errors__WEBPACK_IMPORTED_MODULE_4__["InvalidValueError"](`Invalid value for timing: ${timing}`);

        this._easing = timing;
        return this;
    }

    /**
     * Clip transitioning element to its parent
     *
     * Only useful when transition might overflow the parent element,
     * e.g. when rotating or moving an element.
     *
     * This will set the parent element `clip-path` to `inset(0 0)`, and also
     * slightly modifies the way `move` works.
     *
     * @example
     * const noClip = (new fw.Transition()).move("up");
     * const withClip = (new fw.Transition()).move("up").clipToParent();
     *
     * // This will move #elementOne completely off screen
     * noClip.hide("#elementOne");
     * // This will move #elementTwo just outside of the parent
     * withClip.hide("#elementTwo");
     *
     * @default false
     * @param {Boolean} [clip=true]
     * @returns {Transition} `this`
     */
    clipToParent(clip) {
        this._clip = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["boolDefault"])(clip, true);
        return this;
    }

    /**
     * Move the element off screen during transition
     *
     * Possible values for direction: up, right, down, left
     *
     * @param {String} direction
     * @throws {InvalidValueError}
     * @returns {Transition} `this`
    */
    move(direction) {
        if(!DIRECTIONS.includes(direction))
            throw new _errors__WEBPACK_IMPORTED_MODULE_4__["InvalidValueError"](`Invalid value for direction: ${direction}`);

        this._transitions["move"] = direction;
        return this;
    }

    /**
     * Rotate the element during transition
     *
     * Rotate an element by the specified degrees.
     * Negative degrees cause rotation to the left, positive to the right.
     *
     * @param {Number} degrees
     * @throws {TypeError}
     * @returns {Transition} `this`
     */
    rotate(degrees) {
        this._transitions["rotate"] = _misc__WEBPACK_IMPORTED_MODULE_3__["Numbers"].parse(degrees);
        return this;
    }

    /**
     * Scale the element during transition
     *
     * This option scales the element with `transform: scale()`.
     * To change the actual size of the element use `width` and/or `height`.
     *
     * The direction parameter specifies which direction to scale the element.
     * Possible values for direction: horizontal, vertical, both.
     *
     * @param {String} [direction=both]
     * @throws {InvalidValueError}
     * @returns {Transition} `this`
     */
    scale(direction) {
        direction = direction || "both";

        if(!SCALINGS.includes(direction))
            throw new _errors__WEBPACK_IMPORTED_MODULE_4__["InvalidValueError"](`Invalid value for direction: ${direction}`);

        this._transitions["scale"] = direction;
        return this;
    }

    /**
     * Change the width of the element during transition
     *
     * @returns {Transition} `this`
     */
    width() {
        this._transitions["width"] = true;
        return this;
    }

    /**
     * Change the height of the element during transition
     *
     * @returns {Transition} `this`
     */
    height() {
        this._transitions["height"] = true;
        return this;
    }

    /**
     * Fade the element in or out during transition
     *
     * @returns {Transition} `this`
     */
    opacity() {
        this._transitions["opacity"] = true;
        return this;
    }

    /**
     * Toggle the visibility of element(s)
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @param {Boolean} [shown]
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    toggle(element, shown) {
        const promises = [Promise.resolve()];

        _arrays__WEBPACK_IMPORTED_MODULE_1__["concat"](element).forEach(elem => {
            elem = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["realElement"])(elem);

            let display = elem.style.display || null;
            const running = TransitionRunner.getTransition(elem);

            if(!!running)
                display = running.display;

            const newDisplay = ElementDisplay.newValue(elem, display, shown);
            promises.push(new TransitionRunner(elem, this, newDisplay));
        });

        return Promise.all(promises);
    }

    /**
     * Hide element(s)
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    hide(element) {
        return this.toggle(element, false);
    }

    /**
     * Show element(s)
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    show(element) {
        return this.toggle(element, true);
    }
}


/**
 * Create transitions for toggling the visibility of elements
 *
 * @module transitions
 */



/***/ }),

/***/ "./src/ui.js":
/*!*******************!*\
  !*** ./src/ui.js ***!
  \*******************/
/*! exports provided: TabType, TabView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabType", function() { return TabType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabView", function() { return TabView; });
/* harmony import */ var _dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom */ "./src/dom.js");
/* harmony import */ var _arrays__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrays */ "./src/arrays.js");
/* harmony import */ var _transitions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./transitions */ "./src/transitions.js");
/* harmony import */ var _proxy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./proxy */ "./src/proxy.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./misc */ "./src/misc.js");







const TAB_ELEMENT = "div";
const TAB_CONTAINER = "fw-tab-container";
const TAB_VERTICAL = "fw-tab-vertical";
const TAB_TAB = "fw-tab";
const TAB_HEADER = "fw-tab-header";
const TAB_TITLE = "fw-tab-title";
const TAB_BODY = "fw-tab-body";
const TAB_ACTIVE_TITLE = "fw-tab-title-active";
const TAB_ACTIVE_BODY = "fw-tab-body-active";


/**
 * TabType enum
 *
 * @alias module:ui.TabType
 * @enum {Number}
 */
const TabType = {
    /**
     * Normal tab view, tab buttons at the top and content at bottom
     */
    Normal: 1,

    /**
     * Vertical tab view (aka accordion), tab buttons mixed in with content
     */
    Vertical: 2,

    /**
     * Hide tab buttons
     */
    HideButtons: 4
};


/**
 * Class for creating tab views
 *
 * @alias module:ui.TabView
 */
class TabView {
    /**
     * Create a new TabView instance
     *
     * This will transform the element into a tab view.
     *
     * The elements direct children will become the tabs, and links to change
     * the tab will be added to the top of the element. If vertical tabs were
     * requested, links will be added vertically between tabs.
     *
     * The elements don't have to be divs, but they should be block-level elements.
     *
     * Original tree:
     * ```html
     * <div id="my-tabview">
     *     <div id="my-tab-1" title="First tab title">Content for first tab</div>
     *     <div id="my-tab-2" title="Second tab title">Content for second tab</div>
     *     <div id="my-tab-3" title="Third tab title">Content for third tab</div>
     * </div>
     * ```
     *
     * default mode:
     * ```html
     * <div id="my-tabview" class="fw-tab-container">
     *     <div class="fw-tab-header">
     *         <a class="fw-tab-title fw-tab-title-active">First tab title</a>
     *         <a class="fw-tab-title">Second tab title</a>
     *         <a class="fw-tab-title">Third tab title</a>
     *     </div>
     *     <div class="fw-tab">
     *         <div id="my-tab-1" class="fw-tab-body fw-tab-body-active">Content for first tab</div>
     *         <div id="my-tab-2" class="fw-tab-body" style="display:none">Content for second tab</div>
     *         <div id="my-tab-3" class="fw-tab-body" style="display:none">Content for third tab</div>
     *     </div>
     * </div>
     * ```
     *
     * vertical mode:
     * ```html
     * <div id="my-tabview" class="fw-tab-container fw-tab-vertical">
     *     <div class="fw-tab">
     *         <div class="fw-tab-header">
     *             <a class="fw-tab-title fw-tab-title-active">First tab title</a>
     *         </div>
     *         <div id="my-tab-1" class="fw-tab-body fw-tab-body-active">Content for first tab</div>
     *     </div>
     *     <div class="fw-tab">
     *         <div class="fw-tab-header">
     *             <a class="fw-tab-title">Second tab title</a>
     *         </div>
     *         <div id="my-tab-2" class="fw-tab-body" style="display:none">Content for second tab</div>
     *     </div>
     *     <div class="fw-tab">
     *         <div class="fw-tab-header">
     *             <a class="fw-tab-title">Third tab title</a>
     *         </div>
     *         <div id="my-tab-3" class="fw-tab-body" style="display:none">Content for third tab</div>
     *     </div>
     * </div>
     * ```
     *
     * The tab view is 'live', i.e. it will update with a new tab
     * when a new child element is added to `#my-tabview`.
     *
     * @param {(String|Element)} element Element/Selector
     * @param {Number} [activeIndex=0] Index of tab to set active
     * @param {TabType} [type=TabType.Normal]
     * @throws {ElementError}
     */
    constructor(element, activeIndex, type) {
        this._element = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["realElement"])(element);
        this._type = type || 0;
        this._transition = _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"];
        this._changed = undefined;
        this._sections = [];
        this._active = activeIndex || 0;
        this._promise = Promise.resolve();
        this._header = _dom__WEBPACK_IMPORTED_MODULE_0__["element"](TAB_ELEMENT, {class: TAB_HEADER});
        this._body = _dom__WEBPACK_IMPORTED_MODULE_0__["element"](TAB_ELEMENT, {class: TAB_TAB});
        this._observer = this._setupObserver();

        // '===' has higher precedence than '&'
        if((this._type & TabType.Normal) === 0 && (this._type & TabType.Vertical) === 0)
            this._type |= TabType.Normal;

        if(this._active < 0)
            this._active = 0;
        else if(this._active >= this._element.childElementCount)
            this._active = Math.max(0, this._element.childElementCount - 1);

        this._transform();
    }

    _setupObserver() {
        return new MutationObserver(records => {
            records.forEach(record => {
                _arrays__WEBPACK_IMPORTED_MODULE_1__["filter"](record.addedNodes, r => r instanceof Element)
                      .forEach(node => this._createSection(node));
            });
        });
    }

    async _listener(current, event) {
        event.preventDefault();

        if(current.index === this._active) return;

        await this._promise;

        const old = this._sections[this._active];
        this._active = current.index;

        this._promise = new Promise(resolve => {
            let promise = null;

            current.link.classList.add(TAB_ACTIVE_TITLE);
            current.body.classList.add(TAB_ACTIVE_BODY);
            old.link.classList.remove(TAB_ACTIVE_TITLE);
            old.body.classList.remove(TAB_ACTIVE_BODY);

            if(this._type & TabType.Normal) {
                promise = this._transition.hide(old.body).then(() => {
                    return this._transition.show(current.body);
                });
            }
            else {
                this._transition.hide(old.body);
                promise = this._transition.show(current.body);
            }

            promise.then(() => {
                if(!Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isUndefined"])(this._changed))
                    this._changed(current.index, current.body);

                resolve();
            });
        });
    }

    _createLink(title, isActive) {
        const a = _dom__WEBPACK_IMPORTED_MODULE_0__["element"]('a', {href: 'javascript:;'});
        a.textContent = title;
        a.classList.add(TAB_TITLE);

        if(isActive)
            a.classList.add(TAB_ACTIVE_TITLE);

        return a;
    }

    _createSection(node, index) {
        index = Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isUndefined"])(index) ? this._sections.length : index;

        const isActive = index === this._active;
        const title = node.title || `Tab ${index + 1}`;
        const link = this._createLink(title, isActive);
        const tab = {
            index: index,
            link: null,
            body: null,
            listener: null
        };

        this._observer.disconnect(this._element);

        if(this._type & TabType.Normal) {
            tab.link = this._header.appendChild(link);
            tab.body = this._body.appendChild(node);
        }
        else {
            const tabWrapper = _dom__WEBPACK_IMPORTED_MODULE_0__["element"](TAB_ELEMENT, {class: TAB_TAB});
            const header = _dom__WEBPACK_IMPORTED_MODULE_0__["element"](TAB_ELEMENT, {class: TAB_HEADER});

            if(this._type & TabType.HideButtons)
                header.style.display = "none";

            this._element.insertBefore(tabWrapper, node);
            tabWrapper.appendChild(header);

            tab.link = header.appendChild(link);
            tab.body = tabWrapper.appendChild(node);
        }

        tab.listener = this._listener.bind(this, tab);
        tab.link.addEventListener("click", tab.listener);
        tab.body.removeAttribute("title");
        tab.body.classList.add(TAB_BODY);

        if(isActive) {
            tab.body.classList.add(TAB_ACTIVE_BODY);
            _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"].show(tab.body);
        }
        else {
            _transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"].hide(tab.body);
        }

        this._sections.push(tab);
        this._observer.observe(this._element, {childList: true});
    }

    _transform() {
        const firstChild = this._element.firstElementChild || null;
        const isNormal = (this._type & TabType.Normal) != 0;
        let child = undefined;
        let index = 0;

        this._element.classList.add(TAB_CONTAINER);

        if(isNormal) {
            if(this._type & TabType.HideButtons)
                this._header.style.display = "none";

            this._element.insertBefore(this._body, firstChild);
            this._element.insertBefore(this._header, this._body);
        }
        else {
            this._element.classList.add(TAB_VERTICAL);
        }

        while(!Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isUndefined"])(child = this._element.children[isNormal ? 2 : index]))
            this._createSection(child, index++);
    }

    /**
     * Get the active tab index
     *
     * @returns {Number}
     */
    get active() {
        return this._active;
    }

    /**
     * Set the active tab
     *
     * This changes the visible tab to be the one at `index`.
     *
     * @param {Number} index
     * @throws {(TypeError|RangeError)}
     */
    set active(index) {
        if(index === this._active) return;

        const sections = this._sections.length;

        if(sections === 0) return;

        index = _misc__WEBPACK_IMPORTED_MODULE_4__["Numbers"].between(index, 0, sections - 1);
        this._sections[index].link.dispatchEvent(new MouseEvent("click"));
    }

    /**
     * This callback gets called when user changes tabs
     *
     * @param {Function} callback `function(index, tabElement)`
     * @throws {TypeError}
     * @returns {TabView} `this`
     */
    changed(callback) {
        if(!Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isFunction"])(callback))
            throw new TypeError("Parameter was not a function");

        if(Object(_misc__WEBPACK_IMPORTED_MODULE_4__["isUndefined"])(this._changed))
            callback(this._active, this._sections[this._active].body);

        this._changed = callback;
        return this;
    }

    /**
     * Set the transition to use when user changes tabs
     *
     * @param {Transition} trans
     * @throws {TypeError}
     * @returns {TabView} `this`
     */
    transition(trans) {
        if(!_transitions__WEBPACK_IMPORTED_MODULE_2__["Transition"]._valid(trans))
            throw new TypeError("Parameter was not a Transition");

        this._transition = trans;
        return this;
    }

    /**
     * Destroy tab view and remove listeners
     *
     * After this the parent element will be empty.
     */
    remove() {
        this._observer.disconnect(this._element);
        this._element.classList.remove(TAB_CONTAINER);
        this._element.classList.remove(TAB_VERTICAL);

        this._sections.forEach(section => {
            section.link.removeEventListener("click", section.listener);
        });

        while(!!this._element.lastChild)
            this._element.removeChild(this._element.lastChild);

        Object(_proxy__WEBPACK_IMPORTED_MODULE_3__["invalidateCache"])(this._element, "tabView");
    }
}


/**
 * Common user interface components
 *
 * @module ui
 */



/***/ })

/******/ })["default"];
//# sourceMappingURL=framework.js.map