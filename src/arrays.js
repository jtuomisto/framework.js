import {isUndefined, isNone, isString, isFunction} from "./misc";


function base(proto) {
    return function(list, callback, ...args) {
        if(!isArrayLike(list))
            throw new TypeError("Parameter wasn't iterable");

        if(!isFunction(callback))
            throw new TypeError("Parameter wasn't a function");

        // Getting this far means list is some kind of iterable
        //
        // If list is not an array and doesn't have a length property,
        // it's propably an iterator, which means it should be made in to an array
        //
        // Array.prototypes can deal with some non-array lists directly eg.
        // HTMLCollection, NodeList
        // They all have the length property and will fail this test
        if(!Array.isArray(list) && isUndefined(list.length))
            list = [...list];

        return proto.call(list, callback, ...args);
    };
}


/**
 * filter(list, callback, [thisArg])
 * @private
 */
const filter = base(Array.prototype.filter);

/**
 * map(list, callback, [thisArg])
 * @private
 */
const map = base(Array.prototype.map);

/**
 * find(list, callback, [thisArg])
 * @private
 */
const find = base(Array.prototype.find);

/**
 * findIndex(list, callback, [thisArg])
 * @private
 */
const findIndex = base(Array.prototype.findIndex);

/**
 * forEach(list, callback, [thisArg])
 * @private
 */
const forEach = base(Array.prototype.forEach);

/**
 * reduce(list, callback, [initialValue])
 * @private
 */
const reduce = base(Array.prototype.reduce);

/**
 * reduceRight(list, callback, [initialValue])
 * @private
 */
const reduceRight = base(Array.prototype.reduceRight);

/**
 * every(list, callback, [thisArg])
 * @private
 */
const every = base(Array.prototype.every);

/**
 * some(list, callback, [thisArg])
 * @private
 */
const some = base(Array.prototype.some);

/**
 * flatMap(list, callback, [thisArg])
 * @private
 */
function flatMap(list, callback, ...args) {
    return map(list, callback, ...args).reduce((acc, a) => acc.concat(a), []);
}

/**
 * concat(...)
 * @private
 */
function concat(...args) {
    return args.map(a => !Array.isArray(a) && isArrayLike(a) ? [...a] : a)
               .reduce((acc, a) => acc.concat(a), []);
}


function createArray(length) {
    return [...(new Array(length)).keys()];
}


function isArrayLike(value) {
    return !isNone(value) && !isString(value) && !isUndefined(value[Symbol.iterator]);
}


/**
 * @module arrays
 * @private
 */
export {
    filter, map, find, findIndex, forEach, reduce, reduceRight,
    every, some, flatMap, concat,
    createArray, isArrayLike
};
