import * as arrays from "./arrays";
import {identifier, realElement, Numbers, isUndefined, isFunction, isString} from "./misc";
import {ElementError} from "./errors";


const DEFAULT_PROP = "textContent";

const bindings = new Map();


/**
 * Class for binding inputs
 *
 * @alias module:bindings
 */
class Binding {
    /**
     * @private
     */
    static _initialize() {
        arrays.filter(bindings.keys(), input => input.type !== "file")
              .forEach(input => Binding._dispatch(input));
    }

    /**
     * @private
     */
    static _dispatch(input) {
        const bind = bindings.get(input);
        const events = arrays.map(bind.instances.values(), instance => instance._event);
        const dispatched = {};

        events.forEach(event => {
            if(!dispatched[event]) {
                input.dispatchEvent(new Event(event));
                dispatched[event] = true;
            }
        });
    }

    /**
     * Create a new Binding instance
     *
     * @param {(Element|String)} inputElement Input element/Selector
     * @param {String} [event=change] Event type
     * @throws {ElementError}
     */
    constructor(inputElement, event) {
        this._input = realElement(inputElement);
        this._event = event || "change";
        this._delay = 0;
        this._timeout = null;
        // transform is an object, so that output callbacks have a reference to it
        // i.e. outputs are transformed correctly even if transform
        // is given *after* output callbacks have been created
        // e.g. fw(...).$binding().output(...).transform(...)
        this._transform = {func: (x) => x};
        this._output = [];
        this._once = [];
        this._checked = [];
        this._unchecked = [];

        this._eventListener = this._listener.bind(this);
        this._input.addEventListener(this._event, this._eventListener);

        let bind = bindings.get(this._input);

        if(!bind) {
            bind = {id: identifier(), instances: new Map()};
            bindings.set(this._input, bind);
        }

        this._id = bind.id();
        bind.instances.set(this._id, this);
    }

    _checkable(target) {
        target = target || this._input;
        return ["radio", "checkbox"].includes(target.type);
    }

    _handler(event) {
        const target = event.target;
        let value = target.value;

        if(this._checkable(target)) {
            value = target.checked;

            if(value)
                this._checked.forEach(check => check(target));
            else
                this._unchecked.forEach(uncheck => uncheck(target));
        }

        this._timeout = null;
        this._output.forEach(output => output(value, target));
        this._once.forEach(once => once(value, target));
    }

    _listener(event) {
        if(this._delay > 0) {
            if(this._timeout !== null)
                clearTimeout(this._timeout);

            this._timeout = setTimeout(this._handler.bind(this, event), this._delay);
        }
        else {
            this._handler(event);
        }
    }

    /**
     * Add a delay to the binding
     *
     * For example, this can be used to throttle a text input so that
     * the event listeners are not called (possibly) several times a second.
     *
     * @default 0
     * @param {Number} milliseconds
     * @throws {(TypeError|RangeError)}
     * @returns {Binding} `this`
     */
    delay(milliseconds) {
        this._delay = Numbers.between(milliseconds, 0, Numbers.PlusInfinity);
        return this;
    }

    /**
     * Add output to binding
     *
     * @param {(Element|String|Object)} output Element/Selector/Object
     * @param {String} [property=textContent] Property to change, eg. "style.fontSize"
     * @param {Function} [transform] Custom transform for this output
     * @throws {(ElementError|TypeError)}
     * @returns {Binding} `this`
     */
    output(output, property, transform) {
        let obj = arrays.concat(output);
        let outputs = obj.length;
        const properties = !!property ? property.split('.') : [DEFAULT_PROP];
        const propsLength = properties.length;
        const prop = properties[propsLength - 1];
        transform = !!transform ? {func: transform} : this._transform;

        if(!isFunction(transform.func))
            throw new TypeError("Parameter wasn't a function");

        if(isString(output)) {
            const elements = document.querySelectorAll(output);

            if(elements.length === 0)
                throw new ElementError(`No elements found for: ${output}`);

            obj = Array.from(elements);
            outputs = obj.length;
        }

        for(let i = 0; i < outputs; i++) {
            for(let j = 0; j < propsLength - 1; j++) {
                obj[i] = obj[i][properties[j]];
            }
        }

        const setter = function(value, target) {
            const [o, l, p, t] = this;
            const transformed = t.func(value, target);

            for(let i = 0; i < l; i++) {
                // CSS styles work in slightly weird way:
                // Property access through brackets wont work for custom properties (css variables)
                // Property access through setProperty wont work for camelCase properties
                // So use setProperty on all properties with dash
                if(o[i] instanceof CSSStyleDeclaration && p.includes('-'))
                    o[i].setProperty(p, transformed);
                else if(o[i] instanceof Element && isUndefined(o[i][p]))
                    o[i].setAttribute(p, transformed);
                else
                    o[i][p] = transformed;
            }
        };

        this._output.push(setter.bind([obj, outputs, prop, transform]));
        return this;
    }

    /**
     * Common transform function for all outputs
     *
     * @param {Function} transform `function(inputValue, eventTarget)`
     * @throws {TypeError}
     * @returns {Binding} `this`
     */
    transform(transform) {
        if(!isFunction(transform))
            throw new TypeError("Transform wasn't a function");

        this._transform.func = transform;
        return this;
    }

    /**
     * Run callback once per event (transforms are run per output)
     *
     * @param {Function} callback `function(inputValue, eventTarget)`
     * @throws {TypeError}
     * @returns {Binding} `this`
     */
    once(callback) {
        if(!isFunction(callback))
            throw new TypeError("Callback wasn't a function");

        this._once.push(callback);
        return this;
    }

    /**
     * Run callback when a checkable input (radio, checkbox) gets checked
     *
     * @param {Function} callback `function(eventTarget)`
     * @throws {(TypeError|Error)}
     * @returns {Binding} `this`
     */
    checked(callback) {
        if(!this._checkable())
            throw new Error("Input isn't a checkable (radio, checkbox)");

        if(!isFunction(callback))
            throw new TypeError("Callback wasn't a function");

        this._checked.push(callback);
        return this;
    }

    /**
     * Run callback when a checkable input (radio, checkbox) gets unchecked
     *
     * @param {Function} callback `function(eventTarget)`
     * @throws {(TypeError|Error)}
     * @returns {Binding} `this`
     */
    unchecked(callback) {
        if(!this._checkable())
            throw new Error("Input isn't a checkable (radio, checkbox)");

        if(!isFunction(callback))
            throw new TypeError("Callback wasn't a function");

        this._unchecked.push(callback);
        return this;
    }

    /**
     * Update input value
     *
     * Dispatches events to all the bindings associated with this input element.
     *
     * @param {(String|Number|Boolean)} [newValue] New value for input
     */
    update(newValue) {
        if(!isUndefined(newValue) && this._input.type !== "file") {
            if(this._checkable())
                this._input.checked = !!newValue;
            else
                this._input.value = newValue;
        }

        Binding._dispatch(this._input);
    }

    /**
     * Remove binding and the event listener
     *
     * This does *not* remove all bindings associated with this input element.
     */
    remove() {
        this._input.removeEventListener(this._event, this._eventListener);

        const bind = bindings.get(this._input);
        bind.instances.delete(this._id);

        if(bind.instances.size === 0)
            bindings.delete(this._input);
    }
}


/**
 * Bind input events to outputs; outputs being either Elements or Objects.
 *
 * @module bindings
 */
export default Binding;
