import namedColors from "./named_colors";
import {Numbers, isUndefined} from "./misc";
import {InvalidValueError} from "./errors";


/**
 * @private
 */
class RGBAColor {
    static from(color) {
        if(color instanceof RGBAColor)
            return new RGBAColor(color.r, color.g, color.b, color.a);
        else if(color instanceof HSLAColor)
            return RGBAColor.fromHSLA(color);

        throw new InvalidValueError("Unknown color type");
    }

    static fromHSLA(hslaColor) {
        const toByte = frac => {
            // Is it better to use floor(... * 256) than round(... * 255)?
            return Math.floor(frac * 256);
        };

        const [h, s, l] = [hslaColor.h, hslaColor.s, hslaColor.l];
        const hh = h / 60;
        const c = (1 - Math.abs(2 * l - 1)) * s;
        const x = c * (1 - Math.abs(hh % 2 - 1));
        const m = l - c / 2;
        let r, g, b, a = hslaColor.a;
        let rgb;

        if(hh <= 1)      rgb = [c, x, 0];
        else if(hh <= 2) rgb = [x, c, 0];
        else if(hh <= 3) rgb = [0, c, x];
        else if(hh <= 4) rgb = [0, x, c];
        else if(hh <= 5) rgb = [x, 0, c];
        else if(hh <= 6) rgb = [c, 0, x];

        [r, g, b] = rgb.map(x => toByte(x + m));

        return new RGBAColor(r, g, b, a);
    }

    /**
     * @param {Number} r [0-255]
     * @param {Number} g [0-255]
     * @param {Number} b [0-255]
     * @param {Number} a [0-1]
     */
    constructor(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = isUndefined(a) ? 1 : a;
    }

    get r() { return this._r; }
    get g() { return this._g; }
    get b() { return this._b; }
    get a() { return this._a; }
    set r(v) { this._r = Numbers.clamp(Math.round(v), 0, 255); }
    set g(v) { this._g = Numbers.clamp(Math.round(v), 0, 255); }
    set b(v) { this._b = Numbers.clamp(Math.round(v), 0, 255); }
    set a(v) { this._a = Numbers.clamp(v, 0, 1); }

    toString() {
        const a = Numbers.round(this._a, 2);

        // rgb and rgba are the same as of CSS Colors Level 4,
        // but that specification is still a working draft
        if(this._a === 1)
            return `rgb(${this._r}, ${this._g}, ${this._b})`;
        else
            return `rgba(${this._r}, ${this._g}, ${this._b}, ${a})`;
    }
}


/**
 * @private
 */
class HSLAColor {
    static from(color) {
        if(color instanceof HSLAColor)
            return new HSLAColor(color.h, color.s, color.l, color.a);
        else if(color instanceof RGBAColor)
            return HSLAColor.fromRGBA(color);

        throw new InvalidValueError("Unknown color type");
    }

    static fromRGBA(rgbaColor) {
        const [r, g, b] = [rgbaColor.r, rgbaColor.g, rgbaColor.b].map(x => x / 255);
        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);
        const c = max - min;
        let h, s, l = (max + min) / 2, a = rgbaColor.a;

        if(c === 0) {
            h = s = 0;
        }
        else {
            switch(max) {
                case r:
                    h = (g - b) / c % 6;
                    break;
                case g:
                    h = (b - r) / c + 2;
                    break;
                case b:
                    h = (r - g) / c + 4;
                    break;
            }

            h *= 60;
            s = l === 1 ? 0 : c / (1 - Math.abs(2 * l - 1));
        }

        return new HSLAColor(h, s, l, a);
    }

    /**
     * @param {Number} h [0-360]
     * @param {Number} s [0-1]
     * @param {Number} l [0-1]
     * @param {Number} a [0-1]
     */
    constructor(h, s, l, a) {
        this.h = h;
        this.s = s;
        this.l = l;
        this.a = isUndefined(a) ? 1 : a;
    }

    get h() { return this._h; }
    get s() { return this._s; }
    get l() { return this._l; }
    get a() { return this._a; }
    set h(v) { this._h = (v % 360) + (v < 0 ? 360 : 0); }
    set s(v) { this._s = Numbers.clamp(v, 0, 1); }
    set l(v) { this._l = Numbers.clamp(v, 0, 1); }
    set a(v) { this._a = Numbers.clamp(v, 0, 1); }

    toString() {
        const h = Numbers.round(this._h, 2);
        const s = Numbers.round(this._s * 100, 2);
        const l = Numbers.round(this._l * 100, 2);
        const a = Numbers.round(this._a, 2);

        // hsl and hsla are the same as of CSS Colors Level 4,
        // but that specification is still a working draft
        if(this._a === 1)
            return `hsl(${h}deg, ${s}%, ${l}%)`;
        else
            return `hsla(${h}deg, ${s}%, ${l}%, ${a})`;
    }
};


/**
 * @private
 */
class CSSColorNumber {
    constructor(str) {
        this._value = NaN;
        this._isPercentage = false;
        this._parse(str);
    }

    _parse(str) {
        const [parsed, unit] = Numbers.parseWithUnits(str, ['%']);
        let number = parsed;

        if(unit === '%') {
            number /= 100;

            if(number < 0 || number > 1)
                throw new RangeError(`Percentage out of bounds [0-100]: ${str}`);

            this._isPercentage = true;
        }

        if(number < 0 || number > 255)
            throw new RangeError(`Number out of bounds [0-255]: ${str}`);

        this._value = number;
    }

    get value() {
        return this._value;
    }

    get isPercentage() {
        return this._isPercentage;
    }
}


/**
 * @private
 */
class CSSColorAngle {
    constructor(str) {
        this._value = NaN;
        this._parse(str);
    }

    _parse(str) {
        const [parsed, unit] = Numbers.parseWithUnits(str, ["deg", "rad", "grad", "turn"]);
        let degrees = parsed;

        switch(unit) {
            case "rad":
                degrees *= 180 / Math.PI;
                break;
            case "grad":
                degrees *= 0.9;
                break;
            case "turn":
                degrees *= 360;
                break;
        }

        degrees = degrees % 360 + (degrees < 0 ? 360 : 0);
        this._value = degrees;
    }

    get degrees() {
        return this._value;
    }
}


/**
 * @private
 */
class CSSColorParser {
    static parseHex(color) {
        const bytes = color.substring(1);
        const length = bytes.length;
        const rgba = [NaN, NaN, NaN, 255];
        let colorCount = 3, colorLength = 1, hex = null;

        switch(length) {
            case 3: // #rgb
                break;
            case 4: // #rgba
                colorCount = 4;
                break;
            case 6: // #rrggbb
                colorLength = 2;
                break;
            case 8: // #rrggbbaa
                colorCount = 4;
                colorLength = 2;
                break;
            default:
                throw new Error("Wrong amount of bytes");
        }

        for(let i = 0; i < colorCount; i++) {
            hex = bytes.substring(colorLength * i, colorLength * (i + 1));
            // If color was given in short form (#rgb(a)), repeat hex
            rgba[i] = Numbers.parseHexadecimal(hex.repeat(3 - colorLength));
        }

        return new RGBAColor(rgba[0], rgba[1], rgba[2], rgba[3] / 255);
    }

    static parseRGBA(color) {
        const parser = new CSSColorParser(["rgb", "rgba"], [NaN, NaN, NaN, 1], ["number", "number", "number", "alpha"]);
        const result = parser.parseString(color);
        return new RGBAColor(result[0], result[1], result[2], result[3]);
    }

    static parseHSLA(color) {
        const parser = new CSSColorParser(["hsl", "hsla"], [NaN, NaN, NaN, 1], ["angle", "percent", "percent", "alpha"]);
        const result = parser.parseString(color);
        return new HSLAColor(result[0], result[1], result[2], result[3]);
    }

    static parse(str, colorClass) {
        const fixed = str.trim().toLowerCase();
        let color = null;

        try {
            if(namedColors.hasOwnProperty(fixed))
                color = CSSColorParser.parseHex(namedColors[fixed]);
            else if(fixed.startsWith('#'))
                color = CSSColorParser.parseHex(fixed);
            else if(fixed.startsWith("rgb"))
                color = CSSColorParser.parseRGBA(fixed);
            else if(fixed.startsWith("hsl"))
                color = CSSColorParser.parseHSLA(fixed);
        }
        catch(e) {
            throw new InvalidValueError(`Invalid color '${str}': ${e.message}`);
        }

        if(!!color)
            return colorClass.from(color);

        throw new InvalidValueError(`Unknown color type: ${str}`);
    }

    constructor(types, defaults, parts) {
        this._types = types;
        this._defaults = defaults;
        this._parts = parts;
    }

    alpha(str) {
        const number = new CSSColorNumber(str);

        if(!number.isPercentage && number.value > 1)
            throw new RangeError(`Alpha out of range [0-1]: ${str}`);

        return number.value;
    }

    angle(str) {
        const angle = new CSSColorAngle(str);
        return angle.degrees;
    }

    number(str) {
        const number = new CSSColorNumber(str);
        return number.value * (number.isPercentage ? 255 : 1);
    }

    percent(str) {
        const number = new CSSColorNumber(str);

        if(!number.isPercentage)
            throw new TypeError(`Number wasn't a percentage: ${str}`);

        return number.value;
    }

    parseString(str) {
        // This is not actually correct as it accepts
        // colors that a browser would not, e.g.:
        // rgb(r / g / b / a)
        // hsl(h, s / l a)
        const separators = [' ', ',', '/'];
        const length = str.length;
        const count = this._parts.length;
        const result = Array.from(this._defaults);
        let c = 0, l = '', acc = [], end = false;

        const parseAccumulated = () => {
            if(c >= count)
                throw new Error("Too many values");

            if(acc.length > 0) {
                result[c] = this[this._parts[c]].call(this, acc.join(''));
                acc = [];
                c++;
            }
        };

        for(let i = 0; i < length; i++) {
            l = str[i];

            if(l === '(') {
                const type = acc.join('');
                acc = [];

                if(!this._types.includes(type))
                    throw new Error(`Unknown type: ${type}`);
            }
            else if(l === ')') {
                end = true;
                parseAccumulated();
                break;
            }
            else if(separators.includes(l)) {
                parseAccumulated();
            }
            else {
                acc.push(l);
            }
        }

        if(!end)
            throw new Error("Parsing stopped before reaching end");

        if(result.some(x => Number.isNaN(x)))
            throw new Error("Missing values");

        return result;
    }
}


/**
 * Transform the color in to `rgb(...)`/`rgba(...)` form
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.rgb
 * @param {String} color
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function rgb(color) {
    return CSSColorParser.parse(color, RGBAColor).toString();
}


/**
 * Transform the color in to `hsl(...)`/`hsla(...)` form
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.hsl
 * @param {String} color
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function hsl(color) {
    return CSSColorParser.parse(color, HSLAColor).toString();
}


/**
 * Darken the color by some amount
 *
 * `amount` should be in range [0-1]
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.darken
 * @param {String} color
 * @param {Number} amount
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function darken(color, amount) {
    const rgba = CSSColorParser.parse(color, RGBAColor);
    const factor = Numbers.between(amount, 0, 1);

    rgba.r -= rgba.r * factor;
    rgba.g -= rgba.g * factor;
    rgba.b -= rgba.b * factor;

    return rgba.toString();
}


/**
 * Lighten the color by some amount
 *
 * `amount` should be in range [0-1]
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.lighten
 * @param {String} color
 * @param {Number} amount
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function lighten(color, amount) {
    const rgba = CSSColorParser.parse(color, RGBAColor);
    const factor = Numbers.between(amount, 0, 1);

    rgba.r += (255 - rgba.r) * factor;
    rgba.g += (255 - rgba.g) * factor;
    rgba.b += (255 - rgba.b) * factor;

    return rgba.toString();
}


/**
 * Saturate the color by some amount
 *
 * `amount` should be in range [0-1]
 *
 * `color` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.saturate
 * @param {String} color
 * @param {Number} amount
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function saturate(color, amount) {
    const hsla = CSSColorParser.parse(color, HSLAColor);
    const factor = Numbers.between(amount, 0, 1);

    if(hsla.s > 0) // Saturating gray doesn't make a lot of sense
        hsla.s += (1 - hsla.s) * factor;

    return RGBAColor.fromHSLA(hsla).toString();
}


/**
 * Get a readable text color for the specified background color
 *
 * `backgroundColor` should be a css color, e.g.: `seagreen`, `#fff`, `rgba(...)`, `hsl(...)`
 *
 * @alias module:colors.text
 * @param {String} backgroundColor
 * @throws {(InvalidValueError|RangeError|TypeError)}
 * @returns {String}
 */
function text(backgroundColor) {
    const rgba = CSSColorParser.parse(backgroundColor, RGBAColor);
    // Rec. 709
    const luma = 0.2126 * rgba.r + 0.7152 * rgba.g + 0.0722 * rgba.b;
    // 115 seemed to be slightly better than the more logical 127 (= max_luma / 2)
    const c = luma > 115 ? 0 : 255;
    return (new RGBAColor(c, c, c)).toString();
}


/**
 * Helper functions for modifying colors
 *
 * @module colors
 * @typicalname fw.color
 */
export {rgb, hsl, darken, lighten, saturate, text};
