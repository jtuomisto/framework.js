import * as dom from "./dom";
import * as arrays from "./arrays";
import {realElement} from "./misc";


const cache = {};


function cloneStyle(computed) {
    const values = {};

    // Firefox doesn't support Object.keys on CSSStyleDeclaration,
    // and only returns the property name indexes
    arrays.createArray(computed.length).forEach(i => {
        const p = computed[i], v = computed[p];

        if(p.indexOf('-') > 0) {
            const prop = p.replace(/\-([a-z])/g, (_, l) => l.toUpperCase());
            values[prop] = v;
        }

        values[p] = v;
    });

    return values;
}


function computeElement(element) {
    if(element.isConnected) return window.getComputedStyle(element);

    const container = dom.element("div");
    const cloned = element.cloneNode(false);

    container.style.display = "none";
    container.appendChild(cloned);
    document.body.appendChild(container);
    // The style must be copied as it is a 'live' reference to the style
    // i.e. it will disappear (at least in Chromium) when the element is removed
    const computed = cloneStyle(window.getComputedStyle(cloned));
    document.body.removeChild(container);

    return computed;
}


/**
 * @private
 * @param {(Element|String)} element
 * @returns {(Object|CSSStyleDeclaration)}
 */
function computedStyle(element) {
    if(element instanceof Element)
        return computeElement(realElement(element));

    element = element.toLowerCase();

    if(!cache[element])
        cache[element] = computeElement(dom.element(element));

    return cache[element];
}


/**
 * @module css
 * @private
 */
export {computedStyle};
