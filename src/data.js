const data = new Map();


function has(element, key) {
    const obj = data.get(element);
    return !!obj && obj.has(key);
}

function get(element, key) {
    const obj = data.get(element);
    return !!obj ? obj.get(key) : undefined;
}

function set(element, key, value) {
    const obj = data.get(element);

    if(!!obj)
        obj.set(key, value);
    else
        data.set(element, new Map([[key, value]]));
}

function remove(element, key) {
    const obj = data.get(element);

    if(!!obj) {
        obj.delete(key);

        if(obj.size === 0)
            data.delete(element);
    }
}

function purge(element, ) {
    data.delete(element);
}


/**
 * @module data
 * @private
 */
export {has, get, set, remove, purge};
