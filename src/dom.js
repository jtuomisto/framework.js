function element(tagName, attrs) {
    const newElement = document.createElement(tagName);

    if(!!attrs) {
        Object.entries(attrs).forEach(([key, value]) => {
            if(!key || !value) return;

            if(Array.isArray(value))
                value = value.join(' ');

            if(key.toLowerCase() === "class")
                String(value).split(' ').forEach((cls) => newElement.classList.add(cls));
            else
                newElement.setAttribute(key, value);
        });
    }

    return newElement;
}

function text(textContent) {
    return document.createTextNode(textContent);
}


/**
 * @module dom
 * @private
 */
export {element, text};
