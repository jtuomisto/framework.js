class FrameworkError extends Error {
    constructor(...args) {
        super(...args);
    }
}


/**
 * Error thrown when element is invalid
 *
 * @alias module:errors.ElementError
 */
class ElementError extends FrameworkError {}

/**
 * Error thrown when trying to define a template twice
 *
 * @alias module:errors.TemplateDefinedError
 */
class TemplateDefinedError extends FrameworkError {}

/**
 * Error thrown when appending data to model that has no template
 *
 * @alias module:errors.ModelHasNoTemplateError
 */
class ModelHasNoTemplateError extends FrameworkError {}

/**
 * Error thrown when template is empty
 *
 * @alias module:errors.EmptyTemplateError
 */
class EmptyTemplateError extends FrameworkError {}

/**
 * Error thrown when value is invalid
 *
 * @alias module:errors.InvalidValueError
 */
class InvalidValueError extends FrameworkError {}

/**
 * Error thrown when trying to send the same request twice
 *
 * @alias module:errors.RequestSentError
 */
class RequestSentError extends FrameworkError {}

/**
 * Error thrown when trying to call a framework proxy method
 * that doesn't operate on arrays with an array
 *
 * @alias module:errors.OperationNotSupportedError
 */
class OperationNotSupportedError extends FrameworkError {}


/**
 * framework.js errors
 *
 * @module errors
 * @typicalname fw.error
 */
export {
    ElementError,
    TemplateDefinedError,
    ModelHasNoTemplateError,
    EmptyTemplateError,
    InvalidValueError,
    RequestSentError,
    OperationNotSupportedError
};
