import * as dom from "./dom";
import * as errors from "./errors";
import * as colors from "./colors";
import * as ui from "./ui";
import Binding from "./bindings";
import Model from "./models";
import Message from "./messages";
import {TransitionChain, Transition} from "./transitions";
import {Request, Requester} from "./requests";
import {createFrameworkProxy} from "./proxy";
import {isFunction, isString} from "./misc";


/**
 * The access point for all the framework.js functionality
 *
 * @example <caption>fw is a global function</caption>
 * fw.load(function() {  // Run something on page load
 *     const fade = new fw.Transition();
 *     fade.opacity().duration(1000);
 *
 *     fw("#my-element").$hide(fade);
 *
 *     const model = fw("#model-container").$model();
 *
 *     try {
 *         model.template('');
 *     }
 *     catch(e) {
 *         e instanceof fw.error.EmptyTemplateError === true;
 *     }
 * });
 *
 * @param {(String|Element)} selector
 * @throws {TypeError}
 * @returns {(Element|Element[])}
 * @function
 * @global
 */
const fw = (function(selector) {
    if(selector instanceof Element) {
        if(selector.$__frameworkProxy)
            return selector;
        else
            return createFrameworkProxy(selector);
    }
    else if(!isString(selector)) {
        throw new TypeError("Paramater wasn't a string nor an Element");
    }

    const query = document.querySelectorAll(selector);

    if(query.length === 0)
        return undefined;
    else if(query.length > 1)
        return createFrameworkProxy(Array.from(query));
    else
        return createFrameworkProxy(query[0]);
}).bind({});


/**
 * Initialize framework.js
 *
 * Usually run once on page load.
 *
 * @alias fw.init
 */
fw.init = () => {
    Binding._initialize();
};


/**
 * Run a function when page and all it's resources have loaded
 *
 * For most cases `fw.ready` is a better choice.
 *
 * Can be called several times, functions will be called
 * (mostly) in the order they were given.
 *
 * @alias fw.load
 * @param {Function} func
 * @throws {TypeError}
 */
fw.load = func => {
    if(!isFunction(func))
        throw new TypeError("Parameter wasn't a function");

    // The load event *might* have not fired when readyState is complete
    // If this is called when readyState has changed, but load has not fired
    // func may be called before load
    // Unlikely(?) to cause anything serious
    if(document.readyState !== "complete")
        window.addEventListener("load", func);
    else
        func();
};


/**
 * Run a function when document parsing is finished
 *
 * This is faster than using `fw.load`.
 *
 * Can be called several times, functions will be called
 * (mostly) in the order they were given.
 *
 * @alias fw.ready
 * @param {Function} func
 * @throws {TypeError}
 */
fw.ready = func => {
    if(!isFunction(func))
        throw new TypeError("Parameter wasn't a function");

    if(document.readyState === "loading")
        document.addEventListener("DOMContentLoaded", func);
    else
        func();
};


/**
 * framework.js errors
 *
 * @alias fw.error
 * @see {@link module:errors}
 */
fw.error = errors;


/**
 * Helper functions for dealing with colors
 *
 * @example
 * fw.color.darken("#ffffff", 0.5) === "rgb(128, 128, 128)";
 * fw.color.darken("rgb(255, 255, 255)", 0.5) === "rgb(128, 128, 128)";
 *
 * // Get a readable text color for a background
 * fw.color.text("#ffeedd") === "rgb(0, 0, 0)";
 * fw.color.text("#402040") === "rgb(255, 255, 255)";
 *
 * @alias fw.color
 * @see {@link module:colors}
 */
fw.color = colors;


/**
 * User interface classes
 *
 * @alias fw.ui
 * @see {@link module:ui}
 */
fw.ui = ui;


/**
 * The Binding class
 *
 * @alias fw.Binding
 * @see {@link module:bindings}
 */
fw.Binding = Binding;


/**
 * The Model class
 *
 * @alias fw.Model
 * @see {@link module:models}
 */
fw.Model = Model;


/**
 * The Message class
 *
 * @alias fw.Message
 * @see {@link module:messages}
 */
fw.Message = Message;


/**
 * The TransitionChain class
 *
 * @example
 * const chain = new fw.TransitionChain();
 * const fade = (new fw.Transition()).opacity().duration(100);
 * const scale = (new fw.Transition()).scale().duration(200);
 *
 * chain.append("#parent", fade).append("#child", scale);
 *
 * fw.Transition.hide("#parent");
 * fw.Transition.hide("#child");
 *
 * // This will result in #parent element fading in
 * // and after that the #child element scaling in.
 * chain.show().then(() => {
 *     // if the alternate option was set (chain.alternate()),
 *     // this will run the chain in reverse
 *     chain.hide();
 * });
 *
 * @alias fw.TransitionChain
 * @see {@link module:transitions.TransitionChain}
 */
fw.TransitionChain = TransitionChain;


/**
 * The Transition class
 *
 * @example
 * fw.Transition.toggle(element);  // Synchronous toggling of element
 *
 * const transition = new fw.Transition();
 * transition.opacity().duration(500);
 *
 * transition.toggle(element);  // Async toggle that fades element in/out
 *                              // Returns a Promise
 *
 * @alias fw.Transition
 * @see {@link module:transitions.Transition}
 */
fw.Transition = Transition;


/**
 * The Request class
 *
 * @example
 * const request = new fw.Request("http://example.com");
 * request.type("document").method("POST");
 *
 * fw.Requester.send(request).then(({body, response}) => { ... });
 *
 * @alias fw.Request
 * @see {@link module:requests.Request}
 */
fw.Request = Request;


/**
 * The Requester class
 *
 * @example
 * const requester = new fw.Requester();
 * const request = new fw.Request("http://example.com/page2");
 *
 * requester.parallel(10); // Default is 1
 *
 * // A convenience method for creating and appending a simple request
 * requester.request("http://example.com").then(({body, response}) => { ... });
 *
 * // Append a request
 * requester.append(request).then(({body, response}) => { ... });
 *
 * @example
 * // Requester has a static method to simply send requests, without dealing with queues
 * fw.Requester.send(someRequest);
 *
 * @alias fw.Requester
 * @see {@link module:requests.Requester}
 */
fw.Requester = Requester


/**
 * Create a new element of type tagName, with optional attributes
 *
 * @alias fw.element
 * @param {String} tagName
 * @param {Object} [attrs] Attributes to set
 * @returns {Element}
 */
fw.element = (tagName, attrs) => dom.element(tagName, attrs);


/**
 * Create a new text node
 *
 * @alias fw.text
 * @param {String} textContent
 * @returns {Node}
 */
fw.text = (textContent) => dom.text(textContent);


/**
 * @module fw
 * @private
 */
export default fw;
