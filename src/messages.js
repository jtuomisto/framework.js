import * as dom from "./dom";
import * as arrays from "./arrays";
import {Transition} from "./transitions";
import {invalidateCache} from "./proxy";
import {realElement, identifier, Numbers, boolDefault, isNone} from "./misc";


const ELEMENT_TYPE = "p";
const DETACHED_TYPE = "aside";
const NORMAL_CLASS = "fw-message-normal";
const URGENT_CLASS = "fw-message-urgent";
const DETACHED_CLASS = "fw-message-detached";


/**
 * Class for showing messages
 *
 * @alias module:messages
 */
class Message {
    /**
     * Create a new Message instance
     *
     * @param {(Element|String)} parentElement Element/Selector
     * @throws {(ElementInUseError|ElementError)}
     */
    constructor(parentElement) {
        this._element = realElement(parentElement);
        this._timeout = 4000;
        this._overwrite = true;
        this._hide = true;
        this._messages = new Map();
        this._id = identifier();
        this._lastNormal = null;
        this._transition = Transition;
    }

    _createMessage(content, type, detached, id) {
        const msg = {
            element: this._createElement(type === "urgent", detached),
            id: id || null, timeout: null, messageID: this._id(), type, detached
        };

        msg.element.textContent = content;

        return msg;
    }

    _createElement(urgent, detached) {
        const cls = urgent ? detached ? [URGENT_CLASS, DETACHED_CLASS] : URGENT_CLASS : NORMAL_CLASS;
        const elem = urgent && detached ? DETACHED_TYPE : ELEMENT_TYPE;
        return dom.element(elem, {class: cls});
    }

    _setMessageTimeout(msg) {
        const messageTimeout = () => {
            if(this._lastNormal === msg.messageID)
                this._lastNormal = null;

            this._removeMessage(msg);
        };

        if(msg.timeout !== null)
            clearTimeout(msg.timeout);

        msg.timeout = setTimeout(messageTimeout, this._timeout);
    }

    _setMessageClickHandler(msg) {
        msg.element.addEventListener("click", this._removeMessage.bind(this, msg));
    }

    async _removeMessage(msg) {
        const parent = msg.detached ? document.body : this._element;

        this._messages.delete(msg.messageID);

        if(this._hide && !msg.detached && this._messages.size === 0)
            await this._transition.hide(this._element);
        else if(msg.detached)
            await this._transition.hide(msg.element);

        parent.removeChild(msg.element);
    }

    /**
     * How long to show normal messages for
     *
     * @default 4000
     * @param {Number} milliseconds
     * @throws {(TypeError|RangeError)}
     * @returns {Message} `this`
     */
    timeout(milliseconds) {
        this._timeout = Numbers.between(milliseconds, 0, Numbers.PlusInfinity);
        return this;
    }

    /**
     * Overwrite messages instead of appending them to the container
     *
     * @default true
     * @param {Boolean} [bool=true]
     * @returns {Message} `this`
     */
    overwrite(bool) {
        this._overwrite = boolDefault(bool, true);
        return this;
    }

    /**
     * Hide container when there are no messages
     *
     * @default true
     * @param {Boolean} [bool=true]
     * @returns {Message} `this`
     */
    hide(bool) {
        this._hide = boolDefault(bool, true);
        return this;
    }

    /**
     * Set the transition for message elements
     *
     * This is transition is used to show/hide the elements and the container.
     *
     * @param {Transition} trans
     * @return {Message} `this`
     */
    transition(trans) {
        if(!Transition._valid(trans))
            throw new TypeError("Parameter was not a Transition");

        this._transition = trans;
        return this;
    }

    /**
     * Show a normal message that goes away after a specified timeout
     *
     * Element appended to the message container:
     * ```html
     * <p class="fw-message-normal">content</p>
     * ```
     *
     * @param {String} content
     */
    normal(content) {
        if(this._overwrite && this._lastNormal !== null) {
            const msg = this._messages.get(this._lastNormal);
            msg.element.textContent = content;
            return this._setMessageTimeout(msg);
        }

        const msg = this._createMessage(content, "normal", false);
        this._lastNormal = msg.messageID;
        this._messages.set(msg.messageID, msg);

        this._element.appendChild(msg.element);
        this._transition.show(this._element);

        this._setMessageTimeout(msg);
    }

    /**
     * Show an urgent message that has to be clicked away
     *
     * Urgent messages are not affected by timeout or overwrite.
     *
     * The id parameter can be used to specify an identifier for this message.
     * If a message with the same id is already shown, the new message will be discarded.
     * This can be used to disable duplicate messages, so that the user
     * doesn't see several identical messages.
     *
     * detach == false; element appended to the message container:
     * ```html
     * <p class="fw-message-urgent">content</p>
     * ```
     *
     * detach == true; element appended to the document body:
     * ```html
     * <aside class="fw-message-urgent fw-message-detached">content</aside>
     * ```
     *
     * @param {String} content
     * @param {String} [id]
     * @param {Boolean} [detach=false]
     */
    urgent(content, id, detach) {
        const haveID = !isNone(id);
        const matcher = msg => msg.id === id;

        if(haveID && arrays.some(this._messages.values(), matcher))
            return;

        const msg = this._createMessage(content, "urgent", !!detach, id);
        this._messages.set(msg.messageID, msg);

        if(!msg.detached) {
            const reference = this._element.firstElementChild || null;
            this._element.insertBefore(msg.element, reference);
            this._transition.show(this._element);
        }
        else {
            Transition.hide(msg.element);
            document.body.appendChild(msg.element);
            this._transition.show(msg.element);
        }

        this._setMessageClickHandler(msg);
    }

    /**
     * Remove messages and destroy this message container
     */
    remove() {
        arrays.forEach(this._messages.values(), msg => {
            if(msg.type === "normal") {
                this._element.removeChild(msg.element);
                clearTimeout(msg.timeout);
            }
            else {
                document.body.removeChild(msg.element);
            }
        });

        this._messages = new Map();
        this._lastNormal = null;

        invalidateCache(this._element, "message");
    }
}


/**
 * Show messages, either timed or clickable
 *
 * @module messages
 */
export default Message;
