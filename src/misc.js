import {ElementError} from "./errors";


const Numbers = new class {
    _normalizeNumber(str) {
        return String(str).toLowerCase().replace(/\s/g, '');
    }

    get PlusInfinity() {
        return Number.POSITIVE_INFINITY;
    }

    get MinusInfinity() {
        return Number.NEGATIVE_INFINITY;
    }

    parse(value) {
        const parsed = Number.parseFloat(value);

        if(Number.isNaN(parsed))
            throw new TypeError(`Not a number: ${value}`);

        return parsed;
    }

    strictParse(value) {
        const str = this._normalizeNumber(value);

        if(/^(\-|\+)?\d*\.?\d+(e(\-|\+)?\d+)?$/.test(str))
            return this.parse(str);

        throw new TypeError(`Not a number: ${value}`);
    }

    parseHexadecimal(value) {
        const str = this._normalizeNumber(value);

        if(/^(0x)?[0-9a-f]+$/.test(str))
            return Number.parseInt(str, 16);

        throw new TypeError(`Not a hexadecimal number: ${value}`);
    }

    parseWithUnits(value, allowedUnits) {
        allowedUnits = allowedUnits || [];
        let str = this._normalizeNumber(value);
        const unit = allowedUnits.filter(x => str.endsWith(x))
                                 .reduce((p, c) => c.length > p.length ? c : p, '');

        if(!!unit)
            str = str.substring(0, str.length - unit.length);

        return [this.strictParse(str), unit];
    }

    between(value, min, max) {
        const parsed = this.parse(value);

        if(value > max || value < min)
            throw new RangeError(`Value out of bounds [${min}-${max}]: ${parsed}`);

        return parsed;
    }

    round(value, decimals) {
        const factor = Math.pow(10, decimals || 0);
        return Math.round(this.parse(value) * factor) / factor;
    }

    clamp(value, min, max) {
        return Math.max(min || 0, Math.min(this.parse(value), max || 1));
    }
};


function isString(value) {
    return typeof value === "string";
}


function isFunction(value) {
    return typeof value === "function";
}


function isUndefined(value) {
    return typeof value === "undefined";
}


function isNone(value) {
    return isUndefined(value) || value === null;
}


function identifier() {
    // Unlikely that anyone would need more than 9 007 199 254 740 991 ids
    const generator = (function*() {
        let id = 0;
        for(;;) yield id++;
    })();

    return () => generator.next().value;
}


function realElement(element) {
    let real = null;

    if(element instanceof Element) {
        if(element.$__frameworkProxy)
            real = element.$raw();
        else
            real = element;
    }
    else if(isString(element) && element.length > 0) {
        real = document.querySelector(element);
    }

    if(!real) throw new ElementError(`Invalid element: ${element}`);

    return real;
}


function boolDefault(bool, def) {
    return !isNone(bool) ? !!bool : def;
}


/**
 * @module common
 * @private
 */
export {
    Numbers, isString, isFunction, isUndefined, isNone,
    identifier, realElement, boolDefault
};
