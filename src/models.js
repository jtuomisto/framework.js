import * as dom from "./dom";
import * as arrays from "./arrays";
import {Transition} from "./transitions";
import {invalidateCache} from "./proxy";
import {realElement, Numbers, boolDefault, isUndefined, isNone, isFunction} from "./misc";
import {
    TemplateDefinedError,
    ModelHasNoTemplateError,
    EmptyTemplateError
} from "./errors";


/**
 * @private
 */
class Template {
    constructor() {
        this._template = null;
        this._model = {};
        this._original = [];
    }

    get _hasTemplate() {
        return this._template !== null;
    }

    get rootNodesLength() {
        if(!this._hasTemplate)
            throw new ModelHasNoTemplateError("Template not defined");

        return this._template.content.children.length;
    }

    fragment(datum) {
        if(!this._hasTemplate)
            throw new ModelHasNoTemplateError("Template not defined");

        Object.entries(datum).forEach(([key, value]) => {
            if(this._model.hasOwnProperty(key))
                this._model[key].forEach(f => f(value));
        });

        const imported = document.importNode(this._template.content, true);
        this._original.forEach(f => f());
        return imported;
    }

    setTemplate(tmpl) {
        if(this._template instanceof HTMLTemplateElement)
            throw new TemplateDefinedError("Template already defined");

        const createSetters = (id, obj, prop) => {
            const setter = ((i, o, p, value) => {
                o[p] = String(o[p]).replace(new RegExp(`{${i}}`, 'g'), value);
            }).bind(undefined, id, obj, prop);

            const originalSetter = ((o, p, v) => {
                o[p] = v;
            }).bind(undefined, obj, prop, obj[prop]);

            this._original.push(originalSetter);

            if(this._model.hasOwnProperty(id))
                this._model[id].push(setter);
            else
                this._model[id] = [setter];
        };

        const getIDMatches = value => {
            const regex = /{([^{} ]+)}/g;
            const matches = [];
            let match = null;

            while((match = regex.exec(value)) !== null)
                matches.push(match[1]);

            return matches;
        };

        const setupIdentifiers = element => {
            const attributes = element.attributes;
            const textNodes = arrays.filter(element.childNodes, c => c.nodeName === "#text");

            arrays.concat(attributes, textNodes).forEach(node => {
                getIDMatches(node.nodeValue).forEach(id => {
                    createSetters(id, node, "nodeValue");
                });
            });
        };

        if(tmpl instanceof HTMLTemplateElement) {
            this._template = tmpl.cloneNode(true);
        }
        else {
            this._template = dom.element("template");
            this._template.innerHTML = tmpl;
        }

        const elements = [...this._template.content.children];
        let current = undefined;

        if(elements.length === 0)
            throw new EmptyTemplateError("Empty template");

        try {
            while(!isUndefined(current = elements.shift())) {
                setupIdentifiers(current);

                if(current.children.length > 0)
                    elements.unshift(...current.children);
            }
        }
        catch(err) {
            this._template = null;
            this._model = {};
            throw err;
        }
    }
}


/**
 * Class for creating view models
 *
 * @alias module:models
 */
class Model {
    /**
     * Create a new Model instance
     *
     * @param {(Element|String)} parentElement Element/Selector
     * @throws {(ElementInUseError|ElementError)}
     */
    constructor(parentElement) {
        this._element = realElement(parentElement);
        this._data = [];
        this._lazy = false;
        this._chunk = 1;
        this._dataIndex = 0;
        this._visibleDOM = 0;
        this._filterMatch = undefined;
        this._filterCallback = undefined;
        this._transition = Transition;
        this._template = new Template();
        this._elementDataMap = new Map();
        this._visible = () => undefined;
        this._observer = this._setupObserver();
    }

    /**
     * @private
     */
    get _visibleElementsLength() {
        return this._template.rootNodesLength * this._dataIndex;
    }

    /**
     * @private
     */
    get _actualElementsLength() {
        return this._element.children.length;
    }

    /**
     * @private
     */
    _setupObserver() {
        const options = {/*root: this._element, */rootMargin: "100px"/*, threshold: 0*/};

        return new IntersectionObserver(entries => {
            entries.filter(entry => entry.isIntersecting).forEach(entry => {
                this._observer.unobserve(entry.target);
                this._visible(this._elementDataMap.get(entry.target), entry.target);
                this._elementDataMap.delete(entry.target);
                this._visibleDOM++;

                if(this._lazy) this._lazyDOM();
            });
        }, options);
    }

    /**
     * @private
     */
    _getIndexes(index) {
        const roots = this._template.rootNodesLength;
        return arrays.createArray(roots).map(i => i + index * roots);
    }

    /**
     * @private
     */
    _matchesFilter(datum, index) {
        index = index || -1;

        if(!this.isFiltered)
            return true;
        else
            return this._filterCallback(datum, index);
    }

    /**
     * @private
     */
    _createDOM(datum, index) {
        const fragment = this._template.fragment(datum);

        if(this.isFiltered && !this._filterMatch.has(index))
            Transition.hide(fragment.children);

        this._element.appendChild(fragment);

        this._getIndexes(index).forEach(i => {
            this._elementDataMap.set(this._element.children[i], datum);
            this._observer.observe(this._element.children[i]);
        });
    }

    /**
     * @private
     */
    _lazyDOM(largestFilterMatchIndex) {
        const dataLength = this._data.length;

        if(this._dataIndex === dataLength) return;

        const diff = this._visibleElementsLength - this._visibleDOM;
        largestFilterMatchIndex = largestFilterMatchIndex || -1;

        const update = count => {
            for(; count > 0 && this._dataIndex < dataLength; count--, this._dataIndex++)
                this._createDOM(this._data[this._dataIndex], this._dataIndex);
        };

        if(this._dataIndex === 0 || diff <= 0)
            update(this._chunk);

        if(largestFilterMatchIndex >= this._dataIndex) {
            const needed = largestFilterMatchIndex - this._dataIndex + 1;
            const chunks = this._chunk * Math.ceil(needed / this._chunk);
            update(chunks);
        }
    }

    /**
     * Is model empty (i.e. has no data)
     *
     * @returns {Boolean}
     */
    get isEmpty() {
        return this._data.length === 0;
    }

    /**
     * Is model filtered
     *
     * @returns {Boolean}
     */
    get isFiltered() {
        return isFunction(this._filterCallback);
    }

    /**
     * Lazy-load DOM elements
     *
     * @default false
     * @param {Boolean} [bool=true]
     * @returns {Model} `this`
     */
    lazy(bool) {
        this._lazy = boolDefault(bool, true);
        return this;
    }

    /**
     * How many DOM elements to load at a time
     *
     * This option does nothing when lazy-loading isn't used.
     *
     * @default 1
     * @param {Number} chunk
     * @throws {(TypeError|RangeError)}
     * @returns {Model} `this`
     */
    chunk(chunk) {
        this._chunk = Numbers.between(chunk, 1, Numbers.PlusInfinity);
        return this;
    }

    /**
     * Set the template of the model
     *
     * Template data that should be replaced by actual data is identified
     * by an id string enclosed in curly brackets, e.g. {some-id}.
     *
     * Strings that will have identifiers replaced by data:
     * ```
     * '{id}'
     * '{!#¤%}'
     * '{123A_sdASJH}'
     * '  {whitespace_before_and_after}   '
     * 'Full name: {fullName}'
     * '{fullName} - {occupation}'
     * 'This value will be: {{enclosed_in_braces}}'
     * ```
     *
     * Strings that will be left as is:
     * ```
     * '{ id}'
     * '{}'
     * '{no spaces}'
     * '{{}}'
     * 'Something {something{}}'
     * ```
     *
     * @example
     * --- HTML:
     * <template id="my-template">
     *     <p class="container">
     *         <span>{first}</span>
     *         <span>{second}</span>
     *         <a href="{url}">{url}</a>
     *         <p>My image: <img src="{image}"></p>
     *     </p>
     * </template>
     *
     * --- JavaScript:
     * const myModel = new fw.Model("#my-container");
     * myModel.template(fw.one("#my-template"));
     * myModel.append({
     *     first: "Yay!",
     *     second: "!yaY",
     *     url: "http://example.com",
     *     image: "images/hello.jpg"
     * });
     *
     * --- Result:
     * <your-model-container-element id="my-container">
     *     <p class="container">
     *         <span>Yay!</span>
     *         <span>!yaY</span>
     *         <a href="http://example.com">http://example.com</a>
     *         <p>My image: <img src="images/hello.jpg"></p>
     *     </p>
     * </your-model-container-element>
     *
     * @param {(Element|String)} tmpl HTML content or a `<template>` element
     * @throws {(TemplateDefinedError|EmptyTemplateError)}
     * @returns {Model} `this`
     */
    template(tmpl) {
        this._template.setTemplate(tmpl);
        return this;
    }

    /**
     * Specify a function to be called when elements become visible
     *
     * @example
     * const model = new fw.Model("#my-model");
     * model.template(...);
     * model.visible((data, element) => {
     *     console.log(data, element);
     * });
     *
     * model.append([{num: 1, name: "first"}, {num: 2, name: "second"}]);
     *
     * // The function would now be called twice, with these parameters
     * // (if the appended data caused visible elements)
     * // (if your template has more than one root element, func gets called on all of them)
     * func(modelData[0], firstElement);
     * func(modelData[1], secondElement);
     *
     * @param {Function} func `function(element)`
     * @throws {TypeError}
     * @returns {Model} `this`
     */
    visible(func) {
        if(!isFunction(func))
            throw new TypeError("Parameter wasn't a function");

        this._visible = func;
        return this;
    }

    /**
     * Set the transition for models child elements
     *
     * This is transition is used to show/hide the elements.
     *
     * @param {Transition} trans
     * @return {Model} `this`
     */
    transition(trans) {
        if(!Transition._valid(trans))
            throw new TypeError("Parameter was not a Transition");

        this._transition = trans;
        return this;
    }

    /**
     * Clear model data and remove all DOM elements
     */
    clear() {
        this._data = [];
        this._elementDataMap = new Map();
        this._dataIndex = 0;
        this._visibleDOM = 0;
        this._filterMatch = undefined;

        while(this._element.lastChild) {
            if(this._element.lastChild instanceof Element)
                this._observer.unobserve(this._element.lastChild);

            this._element.removeChild(this._element.lastChild);
        }
    }

    /**
     * Append data to the model
     *
     * @param {(Object|Object[])} data
     * @throws {ModelHasNoTemplateError}
     */
    async append(data) {
        let dataIndex = this._data.length;
        let largestIndex = undefined;

        if(this.isFiltered)
            this._filterMatch = this._filterMatch || new Map();

        arrays.concat(data).forEach(datum => {
            const thisIndex = dataIndex++;

            this._data.push(datum);

            if(this.isFiltered && this._matchesFilter(datum, thisIndex)) {
                this._filterMatch.set(thisIndex, true);
                largestIndex = thisIndex;
            }

            if(!this._lazy) this._createDOM(datum, thisIndex);
        });

        if(this._lazy) this._lazyDOM(largestIndex);
    }

    /**
     * Update model with data
     *
     * This removes the old data and DOM elements.
     *
     * @param {(Object|Object[])} data
     * @throws {ModelHasNoTemplateError}
     */
    async update(data) {
        this.clear();
        await this.append(data);
    }

    /**
     * Filter model
     *
     * Shows elements that match the filter, hides the rest.
     * Calling filter with no arguments removes filtering.
     *
     * @param {Function} [callback] `function(object, dataIndex)`
     * @throws {ModelHasNoTemplateError}
     */
    async filter(callback) {
        if(!isFunction(callback) && !isNone(callback))
            throw new TypeError("Callback wasn't a function");

        this._filterCallback = callback;

        const elements = indexes => {
            const c = this._element.children;
            return arrays.flatMap(indexes, di => this._getIndexes(di).map(i => c[i]));
        };

        const invert = (indexes, length) => {
            const inverted = [];
            let indexesIndex = 0;

            for(let i = 0; i < length; i++) {
                if(i === indexes[indexesIndex])
                    indexesIndex++;
                else
                    inverted.push(i);
            }

            return inverted;
        };

        const currentDataLength = () => this._lazy ? this._dataIndex : this._data.length;

        if(!this.isFiltered) {
            if(!!this._filterMatch) {
                const matches = [...this._filterMatch.keys()];
                const shown = elements(invert(matches, currentDataLength()));
                await this._transition.show(shown);
            }

            return this._filterMatch = undefined;
        }

        let matched = [];
        let unmatched = [];
        let largestIndex = undefined;
        this._filterMatch = new Map();

        this._data.forEach((datum, index) => {
            if(this._matchesFilter(datum, index)) {
                this._filterMatch.set(index, true);
                matched.push(index);
            }
        });

        if(matched.length > 0)
            largestIndex = matched[matched.length - 1];

        if(this._lazy) this._lazyDOM(largestIndex);

        if(matched.length !== currentDataLength())
            unmatched = invert(matched, currentDataLength());

        await this._transition.hide(elements(unmatched));
        await this._transition.show(elements(matched));
    }

    /**
     * Get the data indices that match the filter
     *
     * @returns {(Boolean|Number[])} false if model is not filtered
     */
    getMatches() {
        if(isUndefined(this._filterMatch))
            return false;
        else
            return [...this._filterMatch.keys()];
    }

    /**
     * Get data associated with this model at index
     *
     * @param {Number} index
     * @returns {Object}
     */
    getData(index) {
        return this._data[index];
    }

    /**
     * Transform a data index to a element index
     *
     * @param {Number} index Data index
     * @returns {(Number|Number[])} Array of indices, if your template has many direct children
     */
    getElementIndex(index) {
        const indexes = this._getIndexes(index);

        if(indexes.length > 1)
            return indexes;
        else
            return indexes[0];
    }

    /**
     * Remove this model
     *
     * Removes the model and all child elements.
     */
    remove() {
        this.clear();
        invalidateCache(this._element, "model");
    }
}


/**
 * Create view models for your data
 *
 * @module models
 */
export default Model;
