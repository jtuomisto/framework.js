import * as arrays from "./arrays";
import * as data from "./data";
import * as ui from "./ui";
import Binding from "./bindings";
import Model from "./models";
import Message from "./messages";
import {Transition} from "./transitions";
import {isUndefined, isFunction} from "./misc";
import {OperationNotSupportedError} from "./errors";


/**
 * framework.js proxy
 *
 * These are the methods available in the Proxy returned by {@link fw fw(selector)}.
 *
 * @example
 * // Some methods can work with several elements
 *
 * // All elements matching 'a.navigation' will be hidden
 * fw("a.navigation").$hide();
 * // All elements matching 'p' will now have the class my-paragraph
 * fw("p").$addClass("my-paragraph");
 *
 *
 * // Some methods can only work with single elements
 * const tabView = fw("#my-tab-view").$tabView();
 *
 * // This would result in error (if document has many divs)
 * fw("div").$tabView();
 *
 *
 * // If fw(...) results in a single element, all the elements members can be accessed normally
 * fw("#main-navigation").children;  // HTMLCollection
 * fw("#main-navigation").textContent = "No navigation for you";
 *
 *
 * // If fw(...) results in several elements, all of the Array.prototype methods work normally
 * fw("div").forEach(div => { div.textContent = "Noice." });
 *
 * // NOTE: the elements in the array are just basic unmodified Elements
 * // i.e. This wont work:
 * fw("div").forEach(div => div.$hide());  // Using a proxy method here will fail
 *
 * // The elements need to be wrapped up first:
 * fw("div").forEach(div => fw(div).$hide());  // This will work
 *
 * @typicalname fw(selector)
 * @global
 */
const frameworkProxy = {};


/**
 * @private
 */
const frameworkProxyHandler = {
    get(target, property, proxy) {
        if(property === "$__frameworkProxy")
            return true;

        if(isFunction(target[property]))
            return target[property].bind(target);
        else if(!isUndefined(target[property]))
            return target[property];

        const many = Array.isArray(target);

        if(frameworkProxy.hasOwnProperty(property))
            return frameworkProxy[property].bind({target, proxy, many});

        return undefined;
    },
    set(target, property, value, proxy) {
        if(property === "$__frameworkProxy")
            return false;

        target[property] = value;
        return true;
    }
};


/**
 * @private
 */
function createFrameworkProxy(target) {
    return new Proxy(target, frameworkProxyHandler);
}


/**
 * @private
 */
function invalidateCache(target, name) {
    const methodName = '$' + name;
    data.remove(target, methodName);
}


/**
 * func will have it's this binded to an object with these keys:
 * target: target element, or an array of elements
 * proxy:  the proxy object
 * many:   if true, target is an array of elements
 *
 * @private
 * @param {String} name
 * @param {Boolean} remember If true, remember the return value for this target
 * @param {Function} func
 */
function addProxyMethod(name, remember, func) {
    const methodName = '$' + name;

    if(remember) {
        frameworkProxy[methodName] = function(...args) {
            const funcArgs = func.bind(this, ...args);

            if(this.many)
                return funcArgs();

            if(data.has(this.target, methodName)) {
                const value = data.get(this.target, methodName);

                if(!isUndefined(value))
                    return value;
            }

            const ret = funcArgs();

            if(!isUndefined(ret)) {
                data.set(this.target, methodName, ret);
                return ret;
            }
            else {
                return this.proxy;
            }
        };
    }
    else {
        frameworkProxy[methodName] = function(...args) {
            const ret = func.call(this, ...args);
            return !isUndefined(ret) ? ret : this.proxy;
        };
    }
}


/**
 * Returns the raw, unproxied target
 *
 * @returns {(Element|Element[])}
 * @name $raw
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("raw", false, function() {
    return this.target;
});


/**
 * Support for forEach
 *
 * This is guaranteed to work even if `fw(selector)` matches only one element.
 * A normal forEach won't work in that case, as the proxy target will not be an array.
 *
 * @param {Function} callback The callback function
 * @param {*} [thisArg] The value of `this` in `callback`
 * @name $forEach
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("forEach", false, function(callback, thisArg) {
    arrays.concat(this.target).forEach(callback, thisArg);
});


/**
 * Remove all the children of the element(s)
 *
 * @name $removeChildren
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("removeChildren", false, function() {
    arrays.concat(this.target).forEach(e => {
        while(!!e.lastChild) e.removeChild(e.lastChild);
    });
});


/**
 * Toggle a class of the element(s)
 *
 * @param {String} cls
 * @name $toggleClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("toggleClass", false, function(cls) {
    arrays.concat(this.target).forEach(x => x.classList.toggle(cls));
});


/**
 * Add a class to the element(s)
 *
 * @param {String} cls
 * @name $addClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("addClass", false, function(cls) {
    arrays.concat(this.target).forEach(x => x.classList.add(cls));
});


/**
 * Remove a class from the element(s)
 *
 * @param {String} cls
 * @name $removeClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("removeClass", false, function(cls) {
    arrays.concat(this.target).forEach(x => x.classList.remove(cls));
});


/**
 * Replace a class of the element(s) with another class
 *
 * @param {String} cls
 * @param {String} replacement
 * @name $replaceClass
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("replaceClass", false, function(cls, replacement) {
    arrays.concat(this.target).forEach(x => x.classList.replace(cls, replacement));
});


/**
 * Create a binding for this input
 *
 * @example
 * const binding = fw("#my-input").$binding("input");
 *
 * // This would set the title of the document to
 * // whatever is the value of the input in uppercase
 * binding.output(document, "title").transform(function(value) {
 *     return value.toUpperCase();
 * });
 *
 * @param {String} [event=change] Event type
 * @throws {OperationNotSupportedError}
 * @returns {fw.Binding}
 * @see {@link module:bindings}
 * @name $binding
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("binding", false, function(event) {
    if(this.many)
        throw new OperationNotSupportedError("$binding doesn't operate on lists");

    return new Binding(this.target, event);
});


/**
 * Use the element as a container for a view model
 *
 * @example
 * const model = fw("#my-model-container").$model();
 *
 * model.template("<p><em>{name}</em>: {occupation}</p>");
 *
 * model.append({name: "Full Name", occupation: "Worker"});
 * model.append({name: "Firstname Lastname", occupation: "Work-doer"});
 *
 * #my-model-container:
 * <p><em>Full Name</em>: Worker</p>
 * <p><em>Firstname Lastname</em>: Work-doer</p>
 *
 * @throws {OperationNotSupportedError}
 * @returns {fw.Model}
 * @see {@link module:models}
 * @name $model
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("model", true, function() {
    if(this.many)
        throw new OperationNotSupportedError("$model doesn't operate on lists");

    return new Model(this.target);
});


/**
 * Use the element as a container for messages
 *
 * @example
 * const message = fw("#my-message-container").$message();
 *
 * // Disappears after some time
 * message.normal("This is a normal, timed message");
 *
 * // Disappears when clicked
 * message.urgent("This message has to be clicked away");
 *
 * // Disappears when clicked
 * message.urgent("This message has an id", "my-message");
 *
 * // Message with the same id is already shown
 * message.urgent("This message won't be shown at all", "my-message");
 *
 * @throws {OperationNotSupportedError}
 * @returns {fw.Message}
 * @see {@link module:messages}
 * @name $message
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("message", true, function() {
    if(this.many)
        throw new OperationNotSupportedError("$message doesn't operate on lists");

    return new Message(this.target);
});


/**
 * Toggle the visibility of element(s)
 *
 * @param {fw.Transition} [transition]
 * @param {Boolean} [shown]
 * @returns {Promise}
 * @see {@link module:transitions}
 * @name $toggle
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("toggle", false, function(transition, shown) {
    return (Transition._valid(transition) ? transition : Transition).toggle(this.target, shown);
});


/**
 * Hide the element(s)
 *
 * @param {fw.Transition} [transition]
 * @returns {Promise}
 * @see {@link module:transitions}
 * @name $hide
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("hide", false, function(transition) {
    return (Transition._valid(transition) ? transition : Transition).hide(this.target);
});


/**
 * Show the element(s)
 *
 * @param {fw.Transition} [transition]
 * @returns {Promise}
 * @see {@link module:transitions}
 * @name $show
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("show", false, function(transition) {
    return (Transition._valid(transition) ? transition : Transition).show(this.target);
});


/**
 * Transform the element into a tabbed view
 *
 * @param {Number} [activeIndex=0] Index of tab to set active
 * @param {fw.ui.TabType} [type=fw.ui.TabType.Normal]
 * @throws {OperationNotSupportedError}
 * @returns {fw.ui.TabView}
 * @see {@link module:ui}
 * @name $tabView
 * @memberof frameworkProxy
 * @function
 */
addProxyMethod("tabView", true, function(activeIndex, type) {
    if(this.many)
        throw new OperationNotSupportedError("$tabView doesn't operate on lists");

    return new ui.TabView(this.target, activeIndex, type);
});


/**
 * @module proxy
 * @private
 */
export {createFrameworkProxy, invalidateCache};
