import * as arrays from "./arrays";
import {identifier, Numbers, realElement} from "./misc";
import {InvalidValueError, RequestSentError} from "./errors";


const DOCUMENT_MIMES = ["application/xml", "image/svg+xml", "text/html", "text/xml"];
const TYPES = ["arrayBuffer", "blob", "formData", "document", "json", "text"];
const METHODS = ["GET", "HEAD", "POST", "PUT", "DELETE",
                 "CONNECT", "OPTIONS", "TRACE", "PATCH"];


function parseDocument(response, body) {
    const contentType = response.headers.get("content-type");
    let mime = "text/html";

    if(!!contentType) {
        mime = contentType.split(';')[0].toLowerCase().trim();
        mime = !DOCUMENT_MIMES.includes(mime) ? "text/html" : mime;
    }

    return (new DOMParser()).parseFromString(body, mime);
}


/**
 * @private
 */
class RequestQueue {
    constructor(maxRequests) {
        this.maxRequests = maxRequests || 1;
        this.requests = {};
        this.activeRequests = {};
        this.slot = 0;
        this.id = identifier();
    }

    nextSlot() {
        if(this.slot >= this.maxRequests) this.slot = 0;
        return this.slot++;
    }

    /**
     * @param {Request} request
     * @returns {Promise} resolve({body, response}), reject(error)
     */
    append(request) {
        const match = Object.values(this.requests).find(x => Request.is(x.request, request));

        if(!!match) return match.promise;

        const internal = {id: this.id(), promise: null, request};
        this.requests[internal.id] = internal;

        internal.promise = new Promise((resolve, reject) => {
            const index = this.nextSlot();
            let active = this.activeRequests[index];

            if(!active) {
                active = {index, promise: Promise.resolve(), length: 0};
                this.activeRequests[index] = active;
            }

            active.length++;

            active.promise = active.promise.finally(() => {
                Requester.send(internal.request).then(resolve, reject).finally(() => {
                    if(--active.length === 0)
                        delete this.activeRequests[active.index];

                    delete this.requests[internal.id];
                });
            });
        });

        return internal.promise;
    }
}


/**
 * Class for creating requests
 *
 * @alias module:requests.Request
 */
class Request {
    /**
     * Are requests the same
     *
     * @param {Request} first
     * @param {Request} second
     * @returns {Boolean}
     */
    static is(first, second) {
        const f = first, s = second;

        const basicEquals = f._url === s._url &&
                            f._method === s._method &&
                            f._type === s._type &&
                            f._headersLength === s._headersLength &&
                            f._body == s._body;

        const headerEquals = () => {
            return [...f._headers.entries()].every(([n, v]) => s._headers.get(n) === v);
        };

        return f === s || (basicEquals && headerEquals());
    }

    /**
     * Create a new Request instance
     *
     * @param {(String|URL)} url
     */
    constructor(url) {
        this._url = url instanceof URL ? url.href : url;
        this._method = "GET";
        this._type = "text";
        this._headers = new Headers();
        this._headersLength = 0;
        this._body = undefined;
        this._sent = false;
    }

    /**
     * Request method
     *
     * The HTTP request method to use: GET, POST, etc.
     *
     * @default GET
     * @param {String} requestMethod
     * @throws {(InvalidValueError|RequestSentError)}
     * @returns {Request} `this`
     */
    method(requestMethod) {
        if(this._sent)
            throw new RequestSentError("Request has been sent");

        if(!METHODS.includes(requestMethod))
            throw new InvalidValueError(`Invalid method: ${requestMethod}`);

        this._method = requestMethod;
        return this;
    }

    /**
     * The type of the response body
     *
     * This will decide the type of the body the request will eventually resolve with.
     *
     * Possible values: `arrayBuffer`, `blob`, `formData`, `document`, `json`, `text`
     *
     * @default text
     * @param {String} responseType
     * @throws {(InvalidValueError|RequestSentError)}
     * @returns {Request} `this`
     */
    type(responseType) {
        if(this._sent)
            throw new RequestSentError("Request has been sent");

        if(!TYPES.includes(responseType))
            throw new InvalidValueError(`Invalid type: ${responseType}`);

        this._type = responseType;
        return this;
    }

    /**
     * Data to send to the server
     *
     * @param {(Blob|BufferSource|FormData|URLSearchParams|String)} data
     * @throws {RequestSentError}
     * @returns {Request} `this`
     */
    body(data) {
        if(this._sent)
            throw new RequestSentError("Request has been sent");

        this._body = data;
        return this;
    }

    /**
     * Add a request header
     *
     * @param {String} name
     * @param {String} value
     * @throws {(RequestSentError|TypeError)}
     * @returns {Request} `this`
     */
    header(name, value) {
        if(this._sent)
            throw new RequestSentError("Request has been sent");

        this._headers.append(name, value);
        this._headersLength++;
        return this;
    }
}


/**
 * Class for sending and queueing requests
 *
 * @alias module:requests.Requester
 */
class Requester {
    /**
     * Send a request
     *
     * @param {Request} request
     * @throws {RequestSentError}
     * @returns {Promise} `resolve({body, response})`, `reject(error)`
     */
    static send(request) {
        const r = request;

        if(r._sent) throw new RequestSentError("Request has been sent");

        const data = {
            method: r._method,
            headers: r._headers,
            redirect: "follow"
        };

        if(r._body !== undefined)
            data["body"] = r._body;

        r._sent = true;

        return new Promise((resolve, reject) => {
            fetch(r._url, data).then(response => {
                if(!response.ok)
                    return reject(new Error(`Request failed with code: ${response.status}`));

                const method = r._type === "document" ? "text" : r._type;

                response[method].call(response).then(body => {
                    if(r._type === "document")
                        body = parseDocument(response, body);

                    resolve({body, response});
                }).catch(reject);
            }).catch(reject);
        });
    }

    /**
     * Load HTML from `url` and replace the contents of `element` with the result
     *
     * If `append` is true, the resulting children will be appended to the element,
     * instead of replacing old children.
     *
     * @param {(String|Url)} url
     * @param {(String|Element)} element Element/Selector
     * @param {Boolean} [append=false]
     */
    static load(url, element, append) {
        element = realElement(element);
        const req = new Request(url);

        return new Promise((resolve, reject) => {
            Requester.send(req).then(({body: html}) => {
                const temp = document.createElement("div");
                temp.innerHTML = html;

                if(!append) {
                    while(element.firstChild)
                        element.removeChild(element.firstChild);
                }

                arrays.forEach(temp.childNodes, node => {
                    element.appendChild(document.adoptNode(node));
                });

                resolve();
            }).catch(reject);
        });
    }

    /**
     * Create a new Requester instance
     */
    constructor() {
        this._queue = new RequestQueue(1);
    }

    /**
     * How many requests are handled in parallel
     *
     * @default 1
     * @param {Number} requests
     * @throws {(TypeError|RangeError)}
     * @returns {Requester} `this`
     */
    parallel(requests) {
        this._queue.maxRequests = Numbers.between(requests, 1, 16);
        return this;
    }

    /**
     * Append a new request to the queue
     *
     * @param {Request} request
     * @throws {RequestSentError}
     * @returns {Promise} `resolve({body, response})`, `reject(error)`
     */
    append(request) {
        if(request._sent)
            throw new RequestSentError("Request has been sent");

        this._queue.append(request);
    }

    /**
     * Convenience method for creating and appending a request
     *
     * @param {String} url
     * @param {String} [type=text]
     * @param {String} [method=GET]
     * @throws {InvalidValueError}
     * @returns {Promise} `resolve({body, response})`, `reject(error)`
     */
    request(url, type, method) {
        type = type || "text";
        method = method || "GET";

        return this._queue.append((new Request(url)).type(type).method(method));
    }
}


/**
 * Simplified web requests
 *
 * @module requests
 */
export {Request, Requester};
