import * as css from "./css";
import * as arrays from "./arrays";
import * as data from "./data";
import {realElement, Numbers, boolDefault, isNone} from "./misc";
import {InvalidValueError} from "./errors";


const TRANSITION_PROPERTY = "transitionPromise";
const DISPLAY_PROPERTY = "originalDisplay";
const RECT_PROPERTY = "boundingRect";
const CLIP_PROPERTY = "clipPath";
const DIRECTIONS = ["up", "right", "down", "left"];
const SCALINGS = ["horizontal", "vertical", "both"];
const TIMINGS = ["linear", "ease", "ease-in", "ease-out", "ease-in-out"];


/**
 * @private
 */
class ElementDisplay {
    static initValue(element) {
        if(!!element.style.display) return;

        element.style.display = css.computedStyle(element).display;
    }

    static originalValue(element) {
        let originalDisplay = data.get(element, DISPLAY_PROPERTY);

        if(!originalDisplay) {
            originalDisplay = element.style.display;

            // If element has inline display set, and its none,
            // find what happens when its removed
            if(originalDisplay === "none") {
                const clone = element.cloneNode(false);
                clone.style.display = '';
                originalDisplay = css.computedStyle(clone).display;
            }
            // No inline style, compute the value of display
            else if(!originalDisplay) {
                originalDisplay = css.computedStyle(element).display;
            }

            // Display is set to none in a CSS rule,
            // find the default display for the tag
            if(originalDisplay === "none")
                originalDisplay = css.computedStyle(element.tagName).display;

            data.set(element, DISPLAY_PROPERTY, originalDisplay);
        }

        return originalDisplay;
    }

    static newValue(element, display, shown) {
        ElementDisplay.initValue(element);

        display = isNone(display) ? element.style.display : display;
        const original = ElementDisplay.originalValue(element);

        if(isNone(shown))
            return display !== "none" ? "none" : original;
        else
            return !!shown ? original : "none";
    }
}


/**
 * @private
 */
class TransitionRunner {
    static getTransition(element) {
        return data.get(element, TRANSITION_PROPERTY);
    }

    constructor(element, transition, display) {
        this.element = element;
        this.transitions = transition._transitions;
        this.duration = transition._duration;
        this.easing = transition._easing;
        this.clip = transition._clip && !!this.element.parentElement;
        this.display = display;
        this.hiding = this.display === "none";
        this.overflow = null;
        this.parentClip = null;
        this.rect = null;
        this.parentRect = null;
        this.transform = null;

        return this.run();
    }

    /* <Transition methods> */
    move() {
        const d = this.get("move");

        let parentX = this.parentRect.x;
        let parentY = this.parentRect.y;
        let selfX = this.rect.x;
        let selfY = this.rect.y;

        if(d === "right")     parentX += this.parentRect.width;
        else if(d === "down") parentY += this.parentRect.height;
        else if(d === "left") selfX += this.rect.width;
        else if(d === "up")   selfY += this.rect.height;

        const xPos = d === "left" || d === "right" ? parentX - selfX : 0;
        const yPos = d === "up" || d === "down" ? parentY - selfY : 0;

        const x = this.hiding ? [0, xPos] : [xPos, 0];
        const y = this.hiding ? [0, yPos] : [yPos, 0];

        return ["transform", [`translate(${x[0]}px, ${y[0]}px)`, `translate(${x[1]}px, ${y[1]}px)`]];
    }

    rotate() {
        const degrees = this.get("rotate");
        const [from, to] = this.hiding ? [0, degrees] : [degrees, 0];

        return ["transform", [`rotate(${from}deg)`, `rotate(${to}deg)`]];
    }

    scale() {
        const scaleAxis = this.get("scale");
        const scale = this.hiding ? [1, 0] : [0, 1];
        const x = scaleAxis === "horizontal" || scaleAxis === "both" ? scale : [1, 1];
        const y = scaleAxis === "vertical" || scaleAxis === "both" ? scale : [1, 1];

        return ["transform", [`scale(${x[0]}, ${y[0]})`, `scale(${x[1]}, ${y[1]})`]];
    }

    width() {
        const w = this.rect.width;
        const [from, to] = this.hiding ? [w, 0] : [0, w];
        return ["width", [`${from}px`, `${to}px`]];
    }

    height() {
        const h = this.rect.height;
        const [from, to] = this.hiding ? [h, 0] : [0, h];
        return ["height", [`${from}px`, `${to}px`]];
    }

    opacity() {
        return ["opacity", this.hiding ? [1, 0] : [0, 1]];
    }
    /* </Transition methods> */

    get(transition) {
        return this.transitions[transition];
    }

    setup() {
        // Transform and other properties may not be
        // properly calculated when display is none
        if(!this.hiding) {
            this.element.style.visibility = "hidden";
            this.element.style.display = this.display;
        }

        let rect = this.element.getBoundingClientRect();
        let transform = css.computedStyle(this.element).transform;

        if(transform === "none")
            transform = '';

        if(!this.hiding) {
            this.element.style.display = "none";
            this.element.style.visibility = '';
        }

        this.overflow = this.element.style.overflow;
        this.rect = rect;
        this.transform = transform;

        if(this.clip) {
            const parent = this.element.parentElement;

            if(data.has(parent, CLIP_PROPERTY)) {
                this.parentClip = data.get(parent, CLIP_PROPERTY);
            }
            else {
                this.parentClip = parent.style.clipPath;
                data.set(parent, CLIP_PROPERTY, this.parentClip);
            }

            this.parentRect = parent.getBoundingClientRect();
        }
        else {
            this.parentRect = {
                x: 0, y: 0,
                width: window.innerWidth,
                height: window.innerHeight
            };
        }
    }

    keyframes() {
        const styles = {};

        const joiner = (first, second) => {
            return first.map((value, index) => {
                return [value, second[index]].join(' ');
            });
        };

        Object.keys(this.transitions).forEach(method => {
            const [property, value] = this[method].call(this);

            if(property === "transform" && !styles.hasOwnProperty("transform"))
                styles[property] = [this.transform, this.transform];

            if(styles.hasOwnProperty(property))
                styles[property] = joiner(styles[property], value);
            else
                styles[property] = value;
        });

        return styles;
    }

    run() {
        const createAnimation = () => {
            this.setup();

            const options = {duration: this.duration, easing: this.easing};
            const animation = this.element.animate(this.keyframes(), options);
            animation.pause();

            return animation;
        };

        const preAnimation = () => {
            if(this.clip)
                this.element.parentElement.style.clipPath = "inset(0 0)";

            this.element.style.overflow = "hidden";

            if(!this.hiding)
                this.element.style.display = this.display;
        };

        const postAnimation = () => {
            if(this.hiding)
                this.element.style.display = this.display;
            else
                data.remove(this.element, RECT_PROPERTY);

            this.element.style.overflow = this.overflow;

            if(this.clip)
                this.element.parentElement.style.clipPath = this.parentClip;

            data.remove(this.element, TRANSITION_PROPERTY);
        };

        const running = TransitionRunner.getTransition(this.element);
        const current = {
            animation: null,
            promise: Promise.resolve(),
            display: this.display,
        };

        if(!!running) {
            if(this.display === running.display)
                return Promise.resolve();

            current.promise = running.promise;
        }
        else if(this.element.style.display === this.display) {
            return Promise.resolve();
        }

        current.promise = current.promise.then(() => {
            data.set(this.element, TRANSITION_PROPERTY, current);
            current.animation = createAnimation();

            return new Promise(resolve => {
                let start = null;

                const animationRunner = timestamp => {
                    if(start === null) {
                        start = timestamp;
                        preAnimation();
                    }

                    const diff = timestamp - start;

                    if(diff >= this.duration) {
                        postAnimation();
                        current.animation.finish();
                        resolve();
                    }
                    else {
                        current.animation.currentTime = diff;
                        requestAnimationFrame(animationRunner);
                    }
                };

                requestAnimationFrame(animationRunner);
            });
        });

        return current.promise;
    }
}


/**
 * Class for chaining transitions
 *
 * @alias module:transitions.TransitionChain
 */
class TransitionChain {
    /**
     * Create a new TransitionChain instance
     */
    constructor() {
        this._chain = [];
        this._chain_reverse = [];
        this._alternate = false;
        this._promise = Promise.resolve();
        this._runs = 0;
    }

    /**
     * Alternate direction of the chain after running transitions
     *
     * Possibly useful if elements in the chain have a parent-child relation.
     *
     * @default false
     * @param {Boolean} [bool=true]
     * @returns {TransitionChain} `this`
     */
    alternate(bool) {
        this._alternate = boolDefault(bool, true);
        return this;
    }

    /**
     * Append an element and its transition to the chain
     *
     * @param {(Element|String)} element Element/Selector
     * @param {Transition} transition
     * @throws {(TypeError|InvalidValueError)}
     * @returns {TransitionChain} `this`
     */
    append(element, transition) {
        element = realElement(element);

        if(!Transition._valid(transition))
            throw new TypeError("Parameter wasn't a Transition");

        if(this._chain.some(([e, _]) => e === element))
            throw new InvalidValueError("Element already in the chain");

        this._chain.push([element, transition]);
        this._chain_reverse.unshift([element, transition]);
        return this;
    }

    /**
     * Toggle the visibility of all the elements in the chain
     *
     * @param {Boolean} [shown]
     * @returns {Promise}
     */
    toggle(shown) {
        const chain = this._runs++ % 2 !== 0 && this._alternate ? this._chain_reverse : this._chain;

        chain.forEach(([element, transition]) => {
            this._promise = this._promise.then(() => {
                return transition.toggle(element, shown);
            });
        });

        return this._promise;
    }

    /**
     * Hide all the elements in the chain
     *
     * @returns {Promise}
     */
    hide() {
        return this.toggle(false);
    }

    /**
     * Show all the elements in the chain
     *
     * @returns {Promise}
     */
    show() {
        return this.toggle(true);
    }
}


/**
 * Class for element transitions
 *
 * @alias module:transitions.Transition
 */
class Transition {
    /**
     * @private
     */
    static _valid(trans) {
        return trans instanceof Transition || trans === Transition;
    }

    /**
     * Toggle the visibility of element(s) synchronously
     *
     * If transition is already running on the element, this has to wait for
     * the transition to cancel, which is an asynchronous operation.
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @param {Boolean} [shown]
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    static toggle(element, shown) {
        const promises = [Promise.resolve()];

        arrays.concat(element).forEach(elem => {
            elem = realElement(elem);

            let display = elem.style.display || null;
            const running = TransitionRunner.getTransition(elem);

            if(!!running)
                display = running.display;

            const newDisplay = ElementDisplay.newValue(elem, display, shown);

            if(!!running) {
                running.promise = running.promise.then(() => {
                    elem.style.display = newDisplay;
                });

                promises.push(running.promise);
            }
            else {
                elem.style.display = newDisplay;
            }
        });

        return Promise.all(promises);
    }

    /**
     * Hide element(s) synchronously
     *
     * If transition is already running on the element, this has to wait for
     * the transition to cancel, which is an asynchronous operation.
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    static hide(element) {
        return Transition.toggle(element, false);
    }

    /**
     * Show element(s) synchronously
     *
     * If transition is already running on the element, this has to wait for
     * the transition to cancel, which is an asynchronous operation.
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    static show(element) {
        return Transition.toggle(element, true);
    }

    /**
     * Create a new Transition instance
     */
    constructor() {
        this._transitions = {};
        this._duration = 200;
        this._easing = "linear";
        this._clip = false;
    }

    /**
     * The length of the transition
     *
     * @default 200
     * @param {Number} milliseconds
     * @throws {(TypeError|RangeError)}
     * @returns {Transition} `this`
     */
    duration(milliseconds) {
        this._duration = Numbers.between(milliseconds, 1, Numbers.PlusInfinity);
        return this;
    }

    /**
     * Timing function to use for the transition
     *
     * Possible values: linear, ease, ease-in, ease-out, ease-in-out
     *
     * @default linear
     * @param {String} timing
     * @throws {InvalidValueError}
     * @returns {Transition} `this`
     */
    easing(timing) {
        if(!TIMINGS.includes(timing))
            throw new InvalidValueError(`Invalid value for timing: ${timing}`);

        this._easing = timing;
        return this;
    }

    /**
     * Clip transitioning element to its parent
     *
     * Only useful when transition might overflow the parent element,
     * e.g. when rotating or moving an element.
     *
     * This will set the parent element `clip-path` to `inset(0 0)`, and also
     * slightly modifies the way `move` works.
     *
     * @example
     * const noClip = (new fw.Transition()).move("up");
     * const withClip = (new fw.Transition()).move("up").clipToParent();
     *
     * // This will move #elementOne completely off screen
     * noClip.hide("#elementOne");
     * // This will move #elementTwo just outside of the parent
     * withClip.hide("#elementTwo");
     *
     * @default false
     * @param {Boolean} [clip=true]
     * @returns {Transition} `this`
     */
    clipToParent(clip) {
        this._clip = boolDefault(clip, true);
        return this;
    }

    /**
     * Move the element off screen during transition
     *
     * Possible values for direction: up, right, down, left
     *
     * @param {String} direction
     * @throws {InvalidValueError}
     * @returns {Transition} `this`
    */
    move(direction) {
        if(!DIRECTIONS.includes(direction))
            throw new InvalidValueError(`Invalid value for direction: ${direction}`);

        this._transitions["move"] = direction;
        return this;
    }

    /**
     * Rotate the element during transition
     *
     * Rotate an element by the specified degrees.
     * Negative degrees cause rotation to the left, positive to the right.
     *
     * @param {Number} degrees
     * @throws {TypeError}
     * @returns {Transition} `this`
     */
    rotate(degrees) {
        this._transitions["rotate"] = Numbers.parse(degrees);
        return this;
    }

    /**
     * Scale the element during transition
     *
     * This option scales the element with `transform: scale()`.
     * To change the actual size of the element use `width` and/or `height`.
     *
     * The direction parameter specifies which direction to scale the element.
     * Possible values for direction: horizontal, vertical, both.
     *
     * @param {String} [direction=both]
     * @throws {InvalidValueError}
     * @returns {Transition} `this`
     */
    scale(direction) {
        direction = direction || "both";

        if(!SCALINGS.includes(direction))
            throw new InvalidValueError(`Invalid value for direction: ${direction}`);

        this._transitions["scale"] = direction;
        return this;
    }

    /**
     * Change the width of the element during transition
     *
     * @returns {Transition} `this`
     */
    width() {
        this._transitions["width"] = true;
        return this;
    }

    /**
     * Change the height of the element during transition
     *
     * @returns {Transition} `this`
     */
    height() {
        this._transitions["height"] = true;
        return this;
    }

    /**
     * Fade the element in or out during transition
     *
     * @returns {Transition} `this`
     */
    opacity() {
        this._transitions["opacity"] = true;
        return this;
    }

    /**
     * Toggle the visibility of element(s)
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @param {Boolean} [shown]
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    toggle(element, shown) {
        const promises = [Promise.resolve()];

        arrays.concat(element).forEach(elem => {
            elem = realElement(elem);

            let display = elem.style.display || null;
            const running = TransitionRunner.getTransition(elem);

            if(!!running)
                display = running.display;

            const newDisplay = ElementDisplay.newValue(elem, display, shown);
            promises.push(new TransitionRunner(elem, this, newDisplay));
        });

        return Promise.all(promises);
    }

    /**
     * Hide element(s)
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    hide(element) {
        return this.toggle(element, false);
    }

    /**
     * Show element(s)
     *
     * @param {(Element|String|Element[]|String[])} element Element(s)/Selector(s)
     * @throws {ElementError}
     * @returns {Promise} Resolves when transition is finished
     */
    show(element) {
        return this.toggle(element, true);
    }
}


/**
 * Create transitions for toggling the visibility of elements
 *
 * @module transitions
 */
export {TransitionChain, Transition};
