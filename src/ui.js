import * as dom from "./dom";
import * as arrays from "./arrays";
import {Transition} from "./transitions";
import {invalidateCache} from "./proxy";
import {realElement, Numbers, isUndefined, isFunction} from "./misc";


const TAB_ELEMENT = "div";
const TAB_CONTAINER = "fw-tab-container";
const TAB_VERTICAL = "fw-tab-vertical";
const TAB_TAB = "fw-tab";
const TAB_HEADER = "fw-tab-header";
const TAB_TITLE = "fw-tab-title";
const TAB_BODY = "fw-tab-body";
const TAB_ACTIVE_TITLE = "fw-tab-title-active";
const TAB_ACTIVE_BODY = "fw-tab-body-active";


/**
 * TabType enum
 *
 * @alias module:ui.TabType
 * @enum {Number}
 */
const TabType = {
    /**
     * Normal tab view, tab buttons at the top and content at bottom
     */
    Normal: 1,

    /**
     * Vertical tab view (aka accordion), tab buttons mixed in with content
     */
    Vertical: 2,

    /**
     * Hide tab buttons
     */
    HideButtons: 4
};


/**
 * Class for creating tab views
 *
 * @alias module:ui.TabView
 */
class TabView {
    /**
     * Create a new TabView instance
     *
     * This will transform the element into a tab view.
     *
     * The elements direct children will become the tabs, and links to change
     * the tab will be added to the top of the element. If vertical tabs were
     * requested, links will be added vertically between tabs.
     *
     * The elements don't have to be divs, but they should be block-level elements.
     *
     * Original tree:
     * ```html
     * <div id="my-tabview">
     *     <div id="my-tab-1" title="First tab title">Content for first tab</div>
     *     <div id="my-tab-2" title="Second tab title">Content for second tab</div>
     *     <div id="my-tab-3" title="Third tab title">Content for third tab</div>
     * </div>
     * ```
     *
     * default mode:
     * ```html
     * <div id="my-tabview" class="fw-tab-container">
     *     <div class="fw-tab-header">
     *         <a class="fw-tab-title fw-tab-title-active">First tab title</a>
     *         <a class="fw-tab-title">Second tab title</a>
     *         <a class="fw-tab-title">Third tab title</a>
     *     </div>
     *     <div class="fw-tab">
     *         <div id="my-tab-1" class="fw-tab-body fw-tab-body-active">Content for first tab</div>
     *         <div id="my-tab-2" class="fw-tab-body" style="display:none">Content for second tab</div>
     *         <div id="my-tab-3" class="fw-tab-body" style="display:none">Content for third tab</div>
     *     </div>
     * </div>
     * ```
     *
     * vertical mode:
     * ```html
     * <div id="my-tabview" class="fw-tab-container fw-tab-vertical">
     *     <div class="fw-tab">
     *         <div class="fw-tab-header">
     *             <a class="fw-tab-title fw-tab-title-active">First tab title</a>
     *         </div>
     *         <div id="my-tab-1" class="fw-tab-body fw-tab-body-active">Content for first tab</div>
     *     </div>
     *     <div class="fw-tab">
     *         <div class="fw-tab-header">
     *             <a class="fw-tab-title">Second tab title</a>
     *         </div>
     *         <div id="my-tab-2" class="fw-tab-body" style="display:none">Content for second tab</div>
     *     </div>
     *     <div class="fw-tab">
     *         <div class="fw-tab-header">
     *             <a class="fw-tab-title">Third tab title</a>
     *         </div>
     *         <div id="my-tab-3" class="fw-tab-body" style="display:none">Content for third tab</div>
     *     </div>
     * </div>
     * ```
     *
     * The tab view is 'live', i.e. it will update with a new tab
     * when a new child element is added to `#my-tabview`.
     *
     * @param {(String|Element)} element Element/Selector
     * @param {Number} [activeIndex=0] Index of tab to set active
     * @param {TabType} [type=TabType.Normal]
     * @throws {ElementError}
     */
    constructor(element, activeIndex, type) {
        this._element = realElement(element);
        this._type = type || 0;
        this._transition = Transition;
        this._changed = undefined;
        this._sections = [];
        this._active = activeIndex || 0;
        this._promise = Promise.resolve();
        this._header = dom.element(TAB_ELEMENT, {class: TAB_HEADER});
        this._body = dom.element(TAB_ELEMENT, {class: TAB_TAB});
        this._observer = this._setupObserver();

        // '===' has higher precedence than '&'
        if((this._type & TabType.Normal) === 0 && (this._type & TabType.Vertical) === 0)
            this._type |= TabType.Normal;

        if(this._active < 0)
            this._active = 0;
        else if(this._active >= this._element.childElementCount)
            this._active = Math.max(0, this._element.childElementCount - 1);

        this._transform();
    }

    _setupObserver() {
        return new MutationObserver(records => {
            records.forEach(record => {
                arrays.filter(record.addedNodes, r => r instanceof Element)
                      .forEach(node => this._createSection(node));
            });
        });
    }

    async _listener(current, event) {
        event.preventDefault();

        if(current.index === this._active) return;

        await this._promise;

        const old = this._sections[this._active];
        this._active = current.index;

        this._promise = new Promise(resolve => {
            let promise = null;

            current.link.classList.add(TAB_ACTIVE_TITLE);
            current.body.classList.add(TAB_ACTIVE_BODY);
            old.link.classList.remove(TAB_ACTIVE_TITLE);
            old.body.classList.remove(TAB_ACTIVE_BODY);

            if(this._type & TabType.Normal) {
                promise = this._transition.hide(old.body).then(() => {
                    return this._transition.show(current.body);
                });
            }
            else {
                this._transition.hide(old.body);
                promise = this._transition.show(current.body);
            }

            promise.then(() => {
                if(!isUndefined(this._changed))
                    this._changed(current.index, current.body);

                resolve();
            });
        });
    }

    _createLink(title, isActive) {
        const a = dom.element('a', {href: 'javascript:;'});
        a.textContent = title;
        a.classList.add(TAB_TITLE);

        if(isActive)
            a.classList.add(TAB_ACTIVE_TITLE);

        return a;
    }

    _createSection(node, index) {
        index = isUndefined(index) ? this._sections.length : index;

        const isActive = index === this._active;
        const title = node.title || `Tab ${index + 1}`;
        const link = this._createLink(title, isActive);
        const tab = {
            index: index,
            link: null,
            body: null,
            listener: null
        };

        this._observer.disconnect(this._element);

        if(this._type & TabType.Normal) {
            tab.link = this._header.appendChild(link);
            tab.body = this._body.appendChild(node);
        }
        else {
            const tabWrapper = dom.element(TAB_ELEMENT, {class: TAB_TAB});
            const header = dom.element(TAB_ELEMENT, {class: TAB_HEADER});

            if(this._type & TabType.HideButtons)
                header.style.display = "none";

            this._element.insertBefore(tabWrapper, node);
            tabWrapper.appendChild(header);

            tab.link = header.appendChild(link);
            tab.body = tabWrapper.appendChild(node);
        }

        tab.listener = this._listener.bind(this, tab);
        tab.link.addEventListener("click", tab.listener);
        tab.body.removeAttribute("title");
        tab.body.classList.add(TAB_BODY);

        if(isActive) {
            tab.body.classList.add(TAB_ACTIVE_BODY);
            Transition.show(tab.body);
        }
        else {
            Transition.hide(tab.body);
        }

        this._sections.push(tab);
        this._observer.observe(this._element, {childList: true});
    }

    _transform() {
        const firstChild = this._element.firstElementChild || null;
        const isNormal = (this._type & TabType.Normal) != 0;
        let child = undefined;
        let index = 0;

        this._element.classList.add(TAB_CONTAINER);

        if(isNormal) {
            if(this._type & TabType.HideButtons)
                this._header.style.display = "none";

            this._element.insertBefore(this._body, firstChild);
            this._element.insertBefore(this._header, this._body);
        }
        else {
            this._element.classList.add(TAB_VERTICAL);
        }

        while(!isUndefined(child = this._element.children[isNormal ? 2 : index]))
            this._createSection(child, index++);
    }

    /**
     * Get the active tab index
     *
     * @returns {Number}
     */
    get active() {
        return this._active;
    }

    /**
     * Set the active tab
     *
     * This changes the visible tab to be the one at `index`.
     *
     * @param {Number} index
     * @throws {(TypeError|RangeError)}
     */
    set active(index) {
        if(index === this._active) return;

        const sections = this._sections.length;

        if(sections === 0) return;

        index = Numbers.between(index, 0, sections - 1);
        this._sections[index].link.dispatchEvent(new MouseEvent("click"));
    }

    /**
     * This callback gets called when user changes tabs
     *
     * @param {Function} callback `function(index, tabElement)`
     * @throws {TypeError}
     * @returns {TabView} `this`
     */
    changed(callback) {
        if(!isFunction(callback))
            throw new TypeError("Parameter was not a function");

        if(isUndefined(this._changed))
            callback(this._active, this._sections[this._active].body);

        this._changed = callback;
        return this;
    }

    /**
     * Set the transition to use when user changes tabs
     *
     * @param {Transition} trans
     * @throws {TypeError}
     * @returns {TabView} `this`
     */
    transition(trans) {
        if(!Transition._valid(trans))
            throw new TypeError("Parameter was not a Transition");

        this._transition = trans;
        return this;
    }

    /**
     * Destroy tab view and remove listeners
     *
     * After this the parent element will be empty.
     */
    remove() {
        this._observer.disconnect(this._element);
        this._element.classList.remove(TAB_CONTAINER);
        this._element.classList.remove(TAB_VERTICAL);

        this._sections.forEach(section => {
            section.link.removeEventListener("click", section.listener);
        });

        while(!!this._element.lastChild)
            this._element.removeChild(this._element.lastChild);

        invalidateCache(this._element, "tabView");
    }
}


/**
 * Common user interface components
 *
 * @module ui
 */
export {TabType, TabView};
