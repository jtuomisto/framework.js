const path = require('path');

module.exports = [
    {mode: "development", devtool: "source-map", name: "framework.js"},
    {mode: "production", devtool: undefined, name: "framework.min.js"}
].map(function(rel) {
    return {
        mode: rel.mode,
        devtool: rel.devtool,
        entry: "./src/fw.js",
        output: {
            library: "fw",
            libraryExport: "default",
            filename: rel.name,
            path: path.resolve(__dirname, "dist")
        }
    };
});
